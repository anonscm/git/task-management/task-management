package de.tarent.wfms.processmiddleware.aggregator;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tarent.wfms.processmiddleware.aggregator.AbstractCentralProcessEngineProvider;
import de.tarent.wfms.processmiddleware.config.Configuration;

public class UddiCentralProcessingEngineProvider
	extends	AbstractCentralProcessEngineProvider
{
	private static final String PARAMNAME_PUBLISH_URL = "publishURL";
	private static final String PARAMNAME_INQUIRY_URL = "inquiryURL";
	private static final String PARAMNAME_USERNAME = "username";
	private static final String PARAMNAME_PASSWORD = "password";
	private static final String PARAMNAME_T_MODEL_KEY = "tModelKey";
	private static final String PARAMNAME_SERVICE_KEY = "serviceKey";

	private static String SERVICE_KEY;
	private static String T_MODEL_KEY;
	private static String PASSWORD;
	private static String USERNAME;
	private static String INQUIRY_URL;
	private static String PUBLISH_URL;

	@Override
	public List<ProcessEngine> getProcessEngineList()
	{
		List< ProcessEngine > result = new LinkedList< ProcessEngine >();

		if ( Configuration.getInstance().getParameterAsBoolean("getProcessEnginesFromWS") )
		{
			// get process engines from web service
			RegistryClient client;
			List<URL> urlList = null;
			try
			{
				client = new RegistryClient( new URL( PUBLISH_URL ), new URL( INQUIRY_URL ), USERNAME, PASSWORD, T_MODEL_KEY, SERVICE_KEY );
				urlList = client.discoverServices();

				log.info("Services: " + urlList);
			}
			catch( MalformedURLException e )
			{
				log.warning("Unknown ProcessEngine URL: " + e.getMessage());
			}
			catch( UDDIException e )
			{
				log.warning( "UDDI Exception: " + e.getMessage() );
			}
			catch( TransportException e )
			{
				log.warning( "TransportException: " + e.getMessage() );
			}

			for ( URL url : urlList )
			{
				result.add( ProcessEngineFactory.getProcessEngine( url.toString() ) );
			}
		}

		return result;
	}

	@Override
	public void initialize( String resourceBundleName )
	{
		// FIXME ADD CONFIGPARAM
		// Map juddiParams = (Map)
		// oc.getConfigObject().getModuleConfig().getParamAsObject("juddi");
		Map juddiParams = null;
		if (Configuration.getInstance().getParameterAsBoolean("getProcessEnginesFromWS")) {
			SERVICE_KEY = (String) juddiParams.get(PARAMNAME_SERVICE_KEY);
			T_MODEL_KEY = (String) juddiParams.get(PARAMNAME_T_MODEL_KEY);
			PASSWORD = (String) juddiParams.get(PARAMNAME_PASSWORD);
			USERNAME = (String) juddiParams.get(PARAMNAME_USERNAME);
			INQUIRY_URL = (String) juddiParams.get(PARAMNAME_INQUIRY_URL);
			PUBLISH_URL = (String) juddiParams.get(PARAMNAME_PUBLISH_URL);
		}

		super.initialize( resourceBundleName );
	}
}
