/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

public class ListingProcessFilter implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	protected int listStart;

	protected int maxReturn;

	public ListingProcessFilter() {
	}

	public ListingProcessFilter(
			int listStart,
			int maxReturn) {
		this.listStart = listStart;
		this.maxReturn = maxReturn;
	}


	/**
	 * Gets the listStart value for this AeListingFilter.
	 * 
	 * @return listStart
	 */
	public int getListStart() {
		return listStart;
	}


	/**
	 * Sets the listStart value for this AeListingFilter.
	 * 
	 * @param listStart
	 */
	public void setListStart(int listStart) {
		this.listStart = listStart;
	}


	/**
	 * Gets the maxReturn value for this AeListingFilter.
	 * 
	 * @return maxReturn
	 */
	public int getMaxReturn() {
		return maxReturn;
	}


	/**
	 * Sets the maxReturn value for this AeListingFilter.
	 * 
	 * @param maxReturn
	 */
	public void setMaxReturn(int maxReturn) {
		this.maxReturn = maxReturn;
	}

	private java.lang.Object __equalsCalc = null;
	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof ListingProcessFilter)) return false;
		ListingProcessFilter other = (ListingProcessFilter) obj;
		if (obj == null) return false;
		if (this == obj) return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && 
		this.listStart == other.getListStart() &&
		this.maxReturn == other.getMaxReturn();
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;
	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		_hashCode += getListStart();
		_hashCode += getMaxReturn();
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc =
		new org.apache.axis.description.TypeDesc(ListingProcessFilter.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://list.impl.bpel.rt.activebpel.org", "AeListingFilter"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("listStart");
		elemField.setXmlName(new javax.xml.namespace.QName("", "listStart"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("maxReturn");
		elemField.setXmlName(new javax.xml.namespace.QName("", "maxReturn"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(
			java.lang.String mechType, 
			java.lang.Class _javaType,  
			javax.xml.namespace.QName _xmlType) {
		return 
		new  org.apache.axis.encoding.ser.BeanSerializer(
				_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(
			java.lang.String mechType, 
			java.lang.Class _javaType,  
			javax.xml.namespace.QName _xmlType) {
		return 
		new  org.apache.axis.encoding.ser.BeanDeserializer(
				_javaType, _xmlType, typeDesc);
	}

}


