/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.namespace.QName;

public class WorkerProcessFilter extends ProcessFilter {

	private static final long serialVersionUID = 943934828450850508L;

	private boolean responsible;
	
	public WorkerProcessFilter() {
    	this(null, null, null, null, true, false, null, false);
    }
    
    public boolean isResponsible() {
		return responsible;
	}

	public void setResponsible(boolean responsible) {
		this.responsible = responsible;
	}

	public WorkerProcessFilter(Date startDateFrom, Date startDateTo, Date endDateFrom, Date endDateTo, boolean statusOpen, boolean statusClosed, String processName, boolean responsible) {
    	setProcessCreateStart(getCalendar(startDateFrom));
    	setProcessCreateEnd(getCalendar(startDateTo));
    	setProcessCompleteStart(getCalendar(endDateFrom));
    	setProcessCompleteEnd(getCalendar(endDateTo));

        if (statusOpen == statusClosed)
    		throw new RuntimeException("illegal filter status");
    	
        if (statusOpen)
    		setProcessState(ProcessStates.STATE_RUNNING);
    	else
    		setProcessState(ProcessStates.STATE_COMPLETED_OR_FAULTED);
    	
        if (processName != null && processName.length() > 0)
    		setProcessName(new QName(processName));

        this.responsible = responsible;
	}
    
    /**
     * Wraps a {@link Date} inside a {@link Calendar}.
     * 
     * @param   date
     * @return
     */
    private static Calendar getCalendar(Date date) {
    	GregorianCalendar calendar = null;
    	if (date != null) {
    		calendar = new GregorianCalendar();
        	calendar.setTime(date);
    	}
    	return calendar;
    }
    
    public boolean isRunning() {
        return getProcessState() == ProcessStates.STATE_RUNNING;
    }

    public boolean isCompleted() {
        return getProcessState() == ProcessStates.STATE_COMPLETED_OR_FAULTED;
    }
}
