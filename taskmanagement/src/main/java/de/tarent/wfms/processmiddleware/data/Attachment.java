/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;


@NamedQueries({
		@NamedQuery(name = Attachment.STMT_SELECT_ONE_BY_ID, query = "select a from Attachment as a where a.pk = :id"),
		@NamedQuery(name = Attachment.STMT_SELECT_ALL, query = "select a from Attachment as a"),
		@NamedQuery(name = Attachment.STMT_SELECT_DEEP_BY_TASK_FK, query = "select a from Attachment as a where fkTask = :fk")
		}
		)
@Entity
@Table(name = "attachment")
public class Attachment {

	public static final String STMT_SELECT_ONE_BY_ID = "stmtSelectAttachmentByID"; 
	public static final String STMT_SELECT_DEEP_BY_TASK_FK = "stmtSelectAttachmentDeepByTaskFk";
	public static final String STMT_SELECT_ALL = "stmtSelectAllAttachments";
	
	private long pk;
	private Date creationDate;
	private Date updateDate;
	private Long fkParentProcessNode;
	private Long fkRootProcessNode;
	private Long fkTask;
	private Long fkAttachmentType; // not null
	private String stafferId; // not null
	private String pdmsId; // not null
	private String title; // not null
	private AttachmentType attachmentType;
	private boolean deleted;

	
	@Transient
	public Long getFkAttachmentType() {
		return fkAttachmentType;
	}
	
	public void setFkAttachmentType(Long fkAttachmentType) {
		this.fkAttachmentType = fkAttachmentType;
	}
	
	@Column(name = "fk_task")
	public Long getFkTask() {
		return fkTask;
	}
	
	public void setFkTask(Long fkTask) {
		this.fkTask = fkTask;
	}
	
	@Column(name = "pdms_id")
	public String getPdmsId() {
		return pdmsId;
	}
	
	public void setPdmsId(String pdmsId) {
		this.pdmsId = pdmsId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_attachment_type")
	public AttachmentType getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(AttachmentType attachmentType) {
		this.attachmentType = attachmentType;
	}

	@Column(name = "staffer_id")
	public String getStafferId() {
		return stafferId;
	}

	public void setStafferId(String stafferId) {
		this.stafferId = stafferId;
	}

	@Column(name = "fk_parent_process_node")
	public Long getFkParentProcessNode() {
		return fkParentProcessNode;
	}

	public void setFkParentProcessNode(Long fkParentProcessNode) {
		this.fkParentProcessNode = fkParentProcessNode;
	}

	@Column(name = "fk_root_process_node")
	public Long getFkRootProcessNode() {
		return fkRootProcessNode;
	}

	public void setFkRootProcessNode(Long fkRootProcessNode) {
		this.fkRootProcessNode = fkRootProcessNode;
	}

	@Column(name = "deleted")
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	public String toString(){
		String temp = "fkParentProcessNode:\t" + fkParentProcessNode + "\n" +
		"fkRootProcessNode:\t" + fkRootProcessNode + "\n" +
		"fkTask:\t" + fkTask + "\n" +
		"fkAttachmentType:\t" + fkAttachmentType + "\n" +
		"stafferId:\t" + stafferId + "\n" +
		"pdmsId:\t" + pdmsId + "\n" +
		"title:\t" + title + "\n" +
		"attachmentType:\t" + attachmentType + "\n" +
		"deleted:\t" + deleted + "\n";
		return temp;
	}

	@Id
	@GeneratedValue
	@Column(name = "pk_attachment", nullable = false)
	public long getPk() {
		return pk;
	}
	
	/**
	 * Sets the primary key of this object.
	 * 
	 * @param   pk
	 *          The primary key
	 */
	public void setPk(long pk) {
		this.pk = pk;
	}
	
	/**
	 * Returns the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  creationDate
	 *          The creation time
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   creationDate
	 *          The creation time
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Returns the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  updateDate
	 *          The update time
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	
	/**
	 * Sets the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   updateDate
	 *          The update time
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
