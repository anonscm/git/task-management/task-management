package de.tarent.wfms.processmiddleware.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/** This class gives access to the configuration file of
 * the process middleware.
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class Configuration {

	private static final String configFileName = "processmiddleware.properties";

	private static Logger log = Logger.getLogger(Configuration.class.getName());
	
	private static Configuration instance;
	
	
	private Properties properties;
	
	
	
	private Configuration() {
		
	}
	
	
	
	public static Configuration getInstance() {
		if (instance == null)
			instance = new Configuration();
		
		return instance;
	}
	
	
	/** returns the value of a given configuration parameter name
	 * 
	 * @param key the name of the parameter
	 * @return the value of the parameter
	 * @throws ConfigurationException
	 */
	public String getParameter(String key) {
		if (properties == null)
			loadProperties();
		
		String property = properties.getProperty(key);
		return property;
	}
	
	
	/** returns a parameter as a boolean value
	 * 
	 * @param key the name of the parameter
	 * @return the value of the parameter
	 * @throws ConfigurationException
	 */
	public boolean getParameterAsBoolean(String key) {
		String parameter = getParameter(key);
		
		if (parameter == null)
			return false;
		
		Boolean bool = Boolean.valueOf(parameter);
		
		return bool.booleanValue();
	}
	
	
	/** set a configuration parameter
	 * 
	 * @param key the name of the parameter
	 * @param value the value of the parameter
	 * @throws ConfigurationException
	 */
	public void setParameter(String key, String value) {
		if (properties == null)
			loadProperties();
		
		properties.put(key, value);
	}
	
	
	/** private method for opening properties file
	 * 
	 * @throws ConfigurationException
	 */
	private void loadProperties() {
		File file = new File(configFileName);
		
		properties = new Properties();
		try {
			properties.load(new FileInputStream(file));
		} catch (IOException e) {
			log.log(Level.WARNING, "can not load properties file! no configuration parameters can be accessed", e);
		}
		
	}
	
	
}
