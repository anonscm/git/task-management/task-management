/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import java.util.List;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.data.Comment;
import de.tarent.wfms.processmiddleware.util.JPAFilterHelper;

public class CommentDAO {

	private static CommentDAO instance = null;

	
    public static synchronized CommentDAO getInstance() {
        if (instance == null)
            instance = new CommentDAO();
        return instance;
    }
    
    
    public Comment getCommentByPk(Long pk, EntityManager em) {
    	return (Comment) em.createNamedQuery(Comment.STMT_SELECT_ONE_BY_ID).setParameter("id", pk).getSingleResult();
    }
    

	@SuppressWarnings("unchecked")
	public List<Comment> getAll(CommentFilter filter, EntityManager em) {
		return (List<Comment>) JPAFilterHelper.getQueryWithFilter(Comment.class, Comment.STMT_SELECT_ALL, filter, em).getResultList();
	}
    
}
