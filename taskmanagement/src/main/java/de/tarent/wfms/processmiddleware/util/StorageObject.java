package de.tarent.wfms.processmiddleware.util;

public class StorageObject {

    long expirationTime;
    Object cacheSubject;

    public StorageObject(Object cacheSubject, long validityms) {
        expirationTime = System.currentTimeMillis() + validityms;
        this.cacheSubject = cacheSubject;
    }

    public Object getCacheSubject() {
        return cacheSubject;
    }
   
    /**
     * Returns true, if the expiration date is not reachet, yet. False otherwise.
     */
    public boolean isValid() {
        return System.currentTimeMillis() < expirationTime;
    }
}