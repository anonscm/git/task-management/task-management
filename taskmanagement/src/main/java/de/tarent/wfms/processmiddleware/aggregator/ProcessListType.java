/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

/**
 * This class is necessary to map the xsd-types to proper java classes. 
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class ProcessListType implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private ProcessListResult response;

	public ProcessListResult getResponse() {
		return response;
	}

	public void setResponse(ProcessListResult response) {
		this.response = response;
	}
}
