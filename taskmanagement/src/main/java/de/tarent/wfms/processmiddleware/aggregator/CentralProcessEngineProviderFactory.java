package de.tarent.wfms.processmiddleware.aggregator;

public class CentralProcessEngineProviderFactory
{
	public static AbstractCentralProcessEngineProvider getCentralProcessEngineProvider( String classname, String resourceBundleName )
		throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		Class clazz = Class.forName( classname );

		AbstractCentralProcessEngineProvider result = AbstractCentralProcessEngineProvider.findProviderInstance( classname, resourceBundleName ); 
		if ( result == null )
		{
			result = ( AbstractCentralProcessEngineProvider ) clazz.newInstance();

			/* for now the implementation will see to it that it will initialize itself correctly
			 * including also the instance field in the class
			Method setInstance = clazz.getMethod( "setInstance", new Class[] { AbstractCentralProcessingEngineService.class } );
			setInstance.invoke( clazz, result );
			*/
			result.initialize( resourceBundleName );
		}

		return result;
	}
}
