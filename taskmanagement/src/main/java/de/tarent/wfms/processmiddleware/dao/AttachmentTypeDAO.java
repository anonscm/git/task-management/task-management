/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import de.tarent.wfms.processmiddleware.data.AttachmentType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

public class AttachmentTypeDAO {

	private static AttachmentTypeDAO instance = null;

	
    public static synchronized AttachmentTypeDAO getInstance() {
        if (instance == null)
            instance = new AttachmentTypeDAO();
        return instance;
    }
    
    
    /**
     * Retrieves the attachment type by the supplied name.
     * If this type does not exist yet, it will be created on-the-fly.
     */     
    public AttachmentType getOrCreateType(String name, EntityManager em) {
    	try {
    		return (AttachmentType) em.createNamedQuery(AttachmentType.STMT_SELECT_ONE_BY_NAME).setParameter("name", name).getSingleResult();
    	} catch (NoResultException e) {
    		AttachmentType type = new AttachmentType();
            type.setName(name);
            
            boolean commit = false;
            try {
	            if (!em.getTransaction().isActive()) {
	            	em.getTransaction().begin();
	            	commit = true;
	            }
            } catch (IllegalStateException ise) {
   				// If this library is used in an enterprise context, we may not be
   				// allowed to access the transaction. If so, we believe that the enterprise
   				// transaction will rollback if something happens and don't care about
   				// transaction handling.
            	commit = false;
            }
            
            em.persist(type);
            if (commit)
            	em.getTransaction().commit();
            
            return type;
    	}
    }
    
    
    
    @SuppressWarnings("unchecked")
	public List<AttachmentType> getAll(EntityManager em) {
    	return (List<AttachmentType>) em.createNamedQuery(AttachmentType.STMT_SELECT_ALL).getResultList();
    }
}
