/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ProcessList implements List<ProcessDetail> {
	
	private List<ProcessDetail> processList;
	private Boolean complete;
	private int count;
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public ProcessList(List<ProcessDetail> processList, Boolean complete) {
		super();
		this.processList = processList;
		this.complete = complete;
	}

	public ProcessList() {
		super();
	}

	public Boolean isComplete() {
		return complete;
	}

	public void setComplete(Boolean complete) {
		this.complete = complete;
	}

	public List<ProcessDetail> getProcessList() {
		return processList;
	}

	public void setProcessList(List<ProcessDetail> processList) {
		this.processList = processList;
	}

	public void add(int index, ProcessDetail element) {
		processList.add(index, element);
	}

	public boolean add(ProcessDetail o) {
		return processList.add(o);
	}

	public boolean addAll(Collection<? extends ProcessDetail> c) {
		return processList.addAll(c);
	}

	public boolean addAll(int index, Collection<? extends ProcessDetail> c) {
		return processList.addAll(index, c);
	}

	public void clear() {
		processList.clear();
	}

	public boolean contains(Object o) {
		return processList.contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return processList.containsAll(c);
	}

	public boolean equals(Object o) {
		return processList.equals(o);
	}

	public ProcessDetail get(int index) {
		return processList.get(index);
	}

	public int hashCode() {
		return processList.hashCode();
	}

	public int indexOf(Object o) {
		return processList.indexOf(o);
	}

	public boolean isEmpty() {
		return processList.isEmpty();
	}

	public Iterator<ProcessDetail> iterator() {
		return processList.iterator();
	}

	public int lastIndexOf(Object o) {
		return processList.lastIndexOf(o);
	}

	public ListIterator<ProcessDetail> listIterator() {
		return processList.listIterator();
	}

	public ListIterator<ProcessDetail> listIterator(int index) {
		return processList.listIterator(index);
	}

	public ProcessDetail remove(int index) {
		return processList.remove(index);
	}

	public boolean remove(Object o) {
		return processList.remove(o);
	}

	public boolean removeAll(Collection<?> c) {
		return processList.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		return processList.retainAll(c);
	}

	public ProcessDetail set(int index, ProcessDetail element) {
		return processList.set(index, element);
	}

	public int size() {
		return processList.size();
	}

	public List<ProcessDetail> subList(int fromIndex, int toIndex) {
		return processList.subList(fromIndex, toIndex);
	}

	public Object[] toArray() {
		return processList.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return processList.toArray(a);
	}
	
	
}
