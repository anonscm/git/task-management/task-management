package de.tarent.wfms.processmiddleware.util;

import groovy.lang.Binding;
import groovy.lang.Script;
import groovy.util.GroovyScriptEngine;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.dao.TaskTypeDAO;
import de.tarent.wfms.processmiddleware.data.ProcessNode;
import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.data.TaskType;
import de.tarent.wfms.processmiddleware.task.MaskConnector;
import de.tarent.wfms.processmiddleware.task.type.TaskTypeDescriptor;

/**
 * This singleton hold a small instance of {@link GroovyScriptEngine}.
 * 
 * @author Christoph Jerolimov, tarent GmbH
 * @author modified for use in Processmiddleware by Michael Kleinhenz, tarent GmbH
 */
public class GroovyScriptHelper {
	/** Logger instance */
	private static final Logger logger = Logger.getLogger(GroovyScriptHelper.class.getName());
	/** Singleton instance */
	private static final GroovyScriptHelper instance = new GroovyScriptHelper();

	/** Return the singleton instance of this class. */
	public static GroovyScriptHelper getInstance() {
		return instance;
	}

	/**
	 * GroovyScriptEngine reload itself dynamic groovy scripts and work like
	 * classloader and need a list of directories for loading.
	 */
	protected GroovyScriptEngine groovyScriptEngine;

	synchronized protected void initScriptEngine() throws IOException {
		if (groovyScriptEngine == null) {
			//File path = new File(Context.getActive().moduleRootPath(), "groovy");
			//FIXME: get groovy path from config
			File path =new File("");
			logger.info("Initialize GroovyScriptEngine with all files (recursively) of " + path);
			List<String> paths = new ArrayList<String>();
			appendPathRecursive(paths, path);
			logger.info("Initialize GroovyScriptEngine with this pathlist: " + paths);
			groovyScriptEngine = new GroovyScriptEngine((String[])
					paths.toArray(new String[paths.size()]));
		}
	}

	protected void appendPathRecursive(List<String> list, File path) {
		if (path.exists() && path.isDirectory()) {
			list.add(path.getAbsolutePath());
			File sub[] = path.listFiles();
			for (int i = 0; i < sub.length; i++) {
				appendPathRecursive(list, sub[i]);
			}
		}
	}

    protected Class getClass(String scriptname) throws Exception {
        if (scriptname.endsWith(".groovy"))
            scriptname = scriptname.substring(0, scriptname.length() - 7);
        return groovyScriptEngine.loadScriptByName(scriptname);
    }

    // Groovy Mask Connectors Code
    
    /** Returns a new instance of the given script. */
	public TaskTypeDescriptor newGroovyMaskConnectorInstance(String scriptname) 
	throws Exception {
		if (groovyScriptEngine == null)
			initScriptEngine();
		assert groovyScriptEngine != null;
		
		Class c = getClass(scriptname);
		if (c == null) {
			logger.severe("No script found for '" + scriptname + "'.");
			throw new RuntimeException("No script found for '" + scriptname + "'.");
		}
		
		Object o = c.newInstance();
		if (o instanceof Script) {
			return wrapTaskTypeDescriptorScript(scriptname, (Script)o);
		} else {
			return (TaskTypeDescriptor)o;
		}
	}

    protected TaskTypeDescriptor wrapTaskTypeDescriptorScript(
    		final String scriptname, final Script script) {
        if (logger.isLoggable(Level.FINE))
            logger.log(Level.FINE, "Will wrap groovy script '" + script + "' with MaskConnector.");
        
        return new TaskTypeDescriptor() {
            @Override
            public List<MaskConnector> getMaskConnectors(TaskType taskType, Task task, Map taskData) {
                Map<String,Object> variables = new LinkedHashMap<String,Object>();
                variables.put("scriptengine", groovyScriptEngine);
                variables.put("scriptname", scriptname);
                variables.put("script", script);
                variables.put("logger", Logger.getLogger(script.getClass().getName()));
                variables.put("taskType", taskType);
                variables.put("taskData", taskData);
                
                Binding binding = new Binding(variables);
                binding.setVariable("binding", binding);
                
                if (logger.isLoggable(Level.FINE))
                    logger.log(Level.FINE, "Run wrapped script '" + scriptname + "' with variables: " + variables);
                script.setBinding(binding);
                script.run();
                return (List<MaskConnector>)variables.get("result");
            }
        };
    }

    /**
     * Simple invocation of a groovy script.
     * 
     * @param scriptname Name of the groovy script, relative path, without ".groovy".
     * @param variables Map containing variables for the groovy script.
     * @return Content of the variable "result" after the script has terminated.
     * @throws Exception
     */
    public Object invokeGroovy(String scriptname, Map<String,Object> variables) {
        try
        {
            if (groovyScriptEngine == null)
                initScriptEngine();
            assert groovyScriptEngine != null;
            
            Class c = getClass(scriptname);
            if (c == null) {
                logger.severe("No " + scriptname + " found.");
                throw new RuntimeException("No " + scriptname + " found.");
            }
            
            final Object script = c.newInstance();
            if (script instanceof Script) {
                
                Binding binding = new Binding(variables);
                binding.setVariable("binding", binding);
                
                if (logger.isLoggable(Level.FINE))
                    logger.log(Level.FINE, "Run " + scriptname + " script with variables: " + variables);
                ((Script)script).setBinding(binding);
                ((Script)script).run();
                return variables.get("result");
            } else {
                throw new RuntimeException("Error executing " + scriptname + ".");
            }
        }
        catch (Exception e) {
            throw new RuntimeException("Error executing groovy script " + scriptname, e);
        }
    }

    /**
     * Formats the task data for the web-interface.
     * 
     * @param taskData
     * @return
     */
    public String formatTaskData(Map<String,Object> taskData, String typeURI) {

        Map<String,Object> variables = new LinkedHashMap<String,Object>();
        variables.put("scriptengine", groovyScriptEngine);
        variables.put("scriptname", "taskDataFormatter");
        variables.put("logger", Logger.getLogger(this.getClass().getName()));
        variables.put("taskData", taskData);
        variables.put("taskTypeURI", typeURI);

        return (String)invokeGroovy("taskDataFormatter", variables);
    }

    /**
     * Formats the task data for the web-interface.
     * 
     * @param taskData
     * @return
     */
    public String formatTaskDescription(Task task) {

        Map<String,Object> variables = new LinkedHashMap<String,Object>();
        variables.put("scriptengine", groovyScriptEngine);
        variables.put("scriptname", "taskDataFormatter");
        variables.put("logger", Logger.getLogger(this.getClass().getName()));
        variables.put("taskData", task.getData());
        variables.put("taskTypeURI", task.getType().getUri());
        variables.put("taskDescription", task.getDescription());

        return (String)invokeGroovy("taskDescriptionFormatter", variables);
    }

    /**
     * Calculates the staffer for a task.
     * 
     * @param emv
     * @param typeURI
     * @param taskData
     * @param intendedEndDate
     * @param intendedEndDateInterval
     * @param escalationDate1
     * @param escalationDate1Interval
     * @param escalationDate2
     * @param escalationDate2Interval
     * @return
     * @throws SQLException 
     */
    public String calculateStaffer(String typeURI, Map<String,Object> taskData, 
    		Calendar intendedEndDate, String intendedEndDateInterval, 
    		Calendar escalationDate1, String escalationDate1Interval, 
    		Calendar escalationDate2, String escalationDate2Interval,
    		EntityManager em) 
    throws SQLException{
        TaskType taskType = TaskTypeDAO.getInstance().getTypeByUri(typeURI, em);
        
        Map<String,Object> variables = new LinkedHashMap<String,Object>();
        variables.put("scriptengine", groovyScriptEngine);
        variables.put("scriptname", "calculateStaffer");
        variables.put("logger", Logger.getLogger(this.getClass().getName()));
        variables.put("taskTypeURI", typeURI);
        variables.put("taskData", taskData);            
        
        // TODO What does this groovy script do and what DBAccess does it need?
        //variables.put("emvDB", emv);
        variables.put("intendedEndDate", intendedEndDate);        
        variables.put("intendedEndDateInterval", intendedEndDateInterval);
        variables.put("escalationDate1", escalationDate1);        
        variables.put("escalationDate1Interval", escalationDate1Interval);
        variables.put("escalationDate2", escalationDate2);        
        variables.put("escalationDate2Interval", escalationDate2Interval);
        variables.put("taskType", taskType);

        return (String)invokeGroovy("calculateStaffer", variables);
    }

    public Map<String, String> calculateMail(String recipientCode, String typeURI, Map taskData, EntityManager em) throws SQLException
    {
        // FIXME: this requires that every mail sending is attached to a task type. This may not be the case on simple info mails!
        TaskType taskType = TaskTypeDAO.getInstance().getTypeByUri(typeURI, em);

        Map<String,Object> variables = new LinkedHashMap<String,Object>();
        variables.put("scriptengine", groovyScriptEngine);
        variables.put("scriptname", "calculateMail");
        variables.put("logger", Logger.getLogger(this.getClass().getName()));
        variables.put("recipientCode", recipientCode);            
        variables.put("taskData", taskData);            
        
        // TODO What does this groovy script do and what DBAccess does it need?
        //variables.put("emvDB", emv);
        variables.put("taskType", taskType);

        return (Map<String,String>)invokeGroovy("calculateMail", variables);
    }
    
    public Set<String> getTaskDataLoopupKeys(Task task) {

        Map<String,Object> variables = new LinkedHashMap<String,Object>();
        variables.put("scriptengine", groovyScriptEngine);
        variables.put("scriptname", "getTaskDataLoopupKeys");
        variables.put("logger", Logger.getLogger(this.getClass().getName()));
        variables.put("taskTypeURI", task.getType().getUri());

        return (Set<String>)invokeGroovy("getTaskDataLoopupKeys", variables);
    }
    
    public String formatProcessName(ProcessNode process) {

        Map<String,Object> variables = new LinkedHashMap<String,Object>();
        variables.put("scriptengine", groovyScriptEngine);
        variables.put("scriptname", "processNameFormatter");
        variables.put("logger", Logger.getLogger(this.getClass().getName()));
        variables.put("processName", process.getDetail().getName().getLocalPart().toString());

        return (String)invokeGroovy("processNameFormatter", variables);
    }
    
}
