/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.data;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import de.tarent.wfms.processmiddleware.data.Mask;
import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.task.action_checker.TaskActionChecker;
import de.tarent.wfms.processmiddleware.task.notifier.EscalationNotifier;
import de.tarent.wfms.processmiddleware.task.type.TaskTypeDescriptor;


/**
 * Specifies the type of a {@link Task}. The property
 * {@link Task#getData()} is parsed in dependency of the type with the
 * aid of a {@link TaskTypeDescriptor} which can be retrieved by
 * {@link #getTaskTypeDescriptor()}.
 * 
 * @author Hendrik Helwich
 * @author Christian Preilowski
 * @author Martin Pelzer, tarent GmbH
 */
@NamedQueries({
	@NamedQuery(name = TaskType.STMT_SELECT_ONE, query = "select x from TaskType as x where pk = :id"),
	@NamedQuery(name = TaskType.STMT_SELECT_BY_URI, query = "select x from TaskType as x where uri = :uri")
})
@Entity
@Table(name = "task_type")
public class TaskType {

	public static final String STMT_SELECT_ONE = "stmtSelectOneTaskType";
	public static final String STMT_SELECT_BY_URI = "stmtSelectTaskTypeByUri";
	
	private long pk;
	private Date creationDate;
	private Date updateDate;
	private String uri; // not null, unique
	private String classname; // not null
    private String notifierClassname; // not null
    private String taskActionCheckerClassname; // not null
    private String target; // may be null
    private List<Mask> masks = new ArrayList<Mask>();
	private String returnType;
    
    private static Map<String, TaskTypeDescriptor> descriptorMap;
    private static Map<String, EscalationNotifier> notifierMap;

    
	public String getClassname() {
		return classname;
	}
	
	public void setClassname(String classname) {
		this.classname = classname;
	}
	
	@Column(name = "notifier_classname")
    public String getNotifierClassname() {
        return notifierClassname;
    }
    
    public void setNotifierClassname(String notifierClassname) {
        this.notifierClassname = notifierClassname;
    }

    public String getTarget() {
        return target;
    }
    
    public void setTarget(String target) {
        this.target = target;
    }

    public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
    
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_task_type", updatable = false)
	public List<Mask> getMasks() {
        return masks;
    }
	
	
	public void setMasks(List<Mask> masks) {
		this.masks = masks;
	}

    public void addMask(Mask mask) {
        if (!this.masks.contains(mask))
            this.masks.add(mask);
    }

    @Column(name = "return_type")
    public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	/**
	 * Returns an instance of {@link TaskTypeDescriptor}. This instance is shared
	 * between all instances of {@link #TaskType()}.
	 * The property {@link #getClassname()} is used to determine the type of the
	 * returned value. 
	 * 
	 * @return  an instance of {@link TaskTypeDescriptor}
	 */
	@SuppressWarnings("unchecked")
	@Transient
	public synchronized TaskTypeDescriptor getTaskTypeDescriptor() {
		if (descriptorMap == null)
			descriptorMap = new HashMap<String, TaskTypeDescriptor>();
		String classname = getClassname();
		TaskTypeDescriptor descriptor = descriptorMap.get(classname);
		if (descriptor == null) {
			try {
				Class cls = Class.forName(classname);
				Object obj = cls.newInstance();
				if (!(obj instanceof TaskTypeDescriptor))
					throw new RuntimeException("class " + classname + " does have the wrong type");
				descriptor = (TaskTypeDescriptor) obj;
				if (descriptor != null) 
					descriptorMap.put(classname, descriptor);
			} catch (ClassNotFoundException e) {
                throw new RuntimeException("TaskTypeDescriptor class " + classname + " not found.", e);
			} catch (InstantiationException e) {
                throw new RuntimeException("TaskTypeDescriptor class " + classname + " could not instantiated.", e);
			} catch (IllegalAccessException e) {
                throw new RuntimeException("error creating instance of TaskTypeDescriptor class " + classname, e);
			}
		}
		return descriptor;
	}

    /**
     * Returns an instance of {@link EscalationNotifier}. This instance is shared
     * between all instances of {@link #TaskType()}.
     * The property {@link #getNotifierClassname()} is used to determine the type of the
     * returned value. 
     * 
     * @return  an instance of {@link EscalationNotifier}
     */
    @SuppressWarnings("unchecked")
    @Transient
    public synchronized EscalationNotifier getNotifier() {
        if (notifierMap == null)
            notifierMap = new HashMap<String, EscalationNotifier>();
        String classname = getNotifierClassname();
        EscalationNotifier notifier = notifierMap.get(classname);
        if (notifier == null) {
            try {
                Class cls = Class.forName(classname);
                Object obj = cls.newInstance();
                if (!(obj instanceof EscalationNotifier))
                    throw new RuntimeException("class " + classname + " does have the wrong type");
                notifier = (EscalationNotifier) obj;
                if (notifier != null) 
                    notifierMap.put(classname, notifier);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("EscalationNotifier class " + classname + " not found.", e);
            } catch (InstantiationException e) {
                throw new RuntimeException("EscalationNotifier class " + classname + " could not instantiated.", e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("error creating instance of EscalationNotifier class " + classname, e);
            }
        }
        return notifier;
    }
    
    @Transient
    public List<String> getReturnValues(){
    	ArrayList<String> list = new ArrayList<String>();
    	if(this.returnType != null){
    		String[] values = this.returnType.split(";");
    		for(int i = 0; i < values.length; i++){
    			list.add(values[i]);
    		}
    	} 
    	return list;
    }

    @Column(name = "task_action_checker_classname")
	public String getTaskActionCheckerClassname() {
		return taskActionCheckerClassname;
	}

	public void setTaskActionCheckerClassname(String taskActionCheckerClassname) {
		this.taskActionCheckerClassname = taskActionCheckerClassname;
	}
	
	/**
	 * Returns an instance of {@link TaskActionChecker}. 
	 * 
	 * The property {@link #getTaskActionCheckerClassname()} is used to determine the type of the
	 * returned value. 
	 * 
	 * @return  an instance of {@link TaskTypeDescriptor}
	 */
	@SuppressWarnings("unchecked")
	@Transient
	public TaskActionChecker getTaskActionChecker(Task task) {
		String classname = this.getTaskActionCheckerClassname();
		TaskActionChecker taskAC ;

			try {
				Class cls = Class.forName(classname);
				Object instance = null;
				try {
					Constructor con = cls.getDeclaredConstructor(new Class[] { Task.class });
					instance = con.newInstance(task);
				} catch (SecurityException e) {
					throw new RuntimeException("TaskActionChecker class " + classname + " security exception.", e);
				} catch (IllegalArgumentException e) {
					throw new RuntimeException("TaskActionChecker class " + classname + " illegal argument.", e);
				} catch (NoSuchMethodException e) {
					throw new RuntimeException("TaskActionChecker class " + classname + " no such method.", e);
				} catch (InvocationTargetException e) {
					throw new RuntimeException("TaskActionChecker class " + classname + " InvocationTargetException.", e);
				}
				if (!(instance instanceof TaskActionChecker))
					throw new RuntimeException("class " + classname + " does have the wrong type");
				taskAC = (TaskActionChecker) instance;
			} catch (ClassNotFoundException e) {
                throw new RuntimeException("TaskActionChecker class " + classname + " not found.", e);
			} catch (InstantiationException e) {
                throw new RuntimeException("TaskActionChecker class " + classname + " could not instantiated.", e);
			} catch (IllegalAccessException e) {
                throw new RuntimeException("error creating instance of TaskActionChecker class " + classname, e);
			}
		return taskAC;	
	}

	@Id
	@GeneratedValue
	@Column(name = "pk_task_type", nullable = false)
	public long getPk() {
		return pk;
	}
	
	/**
	 * Sets the primary key of this object.
	 * 
	 * @param   pk
	 *          The primary key
	 */
	public void setPk(long pk) {
		this.pk = pk;
	}
	
	/**
	 * Returns the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  creationDate
	 *          The creation time
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   creationDate
	 *          The creation time
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Returns the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  updateDate
	 *          The update time
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	
	/**
	 * Sets the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   updateDate
	 *          The update time
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
