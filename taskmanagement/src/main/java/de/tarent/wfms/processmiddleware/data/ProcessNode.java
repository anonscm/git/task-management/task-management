/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.data;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import de.tarent.wfms.processmiddleware.aggregator.ProcessDetail;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngine;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngineException;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngineFactory;
import de.tarent.wfms.processmiddleware.api.ProcessApi;


/**
 * This class represents a node in the process tree which is stored in the
 * processmiddleware database. The tree only holds processes which have a
 * task as child or which have a subprocess which has a task as child.
 * Every time a task is created all parent nodes are inserted in the database.
 * The related database table does only store the tree structure of the
 * processes and the unique process id which is represented by the processId
 * and the engineURL.
 * The properties of a process which can vary outside this middleware are
 * stored in the property <code>detail</code> which is of type 
 * {@link ProcessDetail}. This object  
 * 
 * @author Hendrik Helwich, Martin Pelzer
 *
 */
@NamedQueries({
	@NamedQuery(name = ProcessNode.STMT_SELECT_ONE_BY_ID, query = "select x from ProcessNode as x where processId = :id"),
	@NamedQuery(name = ProcessNode.STMT_SELECT_ONE_BY_EXTERNAL_ID, query = "select x from ProcessNode as x where processId = :processId and engineUrl = :engineUrl"),
	@NamedQuery(name = ProcessNode.STMT_SELECT_ONE_BY_EXTERNAL_ID_WITH_ACTUAL_TASK, query = "select process from ProcessNode as process left outer join process.tasks as task left outer join task.stafferList as staffer where task.endDate = null and process.processId = :processId and process.engineUrl = :engineUrl order by task.intendedEndDate asc order by staffer.startDate desc limit 1"),
	@NamedQuery(name = ProcessNode.STMT_SELECT_PARENT_PROCESS_BY_TASK, query = "select process from ProcessNode as process join process.tasks as task where task.pk = :pk"),
	@NamedQuery(name = ProcessNode.STMT_SELECT_BY_FK_PARENT_PROCESS_NODE, query = "select x.processId from ProcessNode as x where fkParentProcessNode = :fk")
})
@Entity
@Table(name = "process_node")
public class ProcessNode {
	
	public static final String STMT_SELECT_ONE_BY_ID = "stmtSelectProcessNodeById";
	public static final String STMT_SELECT_ONE_BY_EXTERNAL_ID = "stmtSelectProcessNodeByExternalId";
	public static final String STMT_SELECT_ONE_BY_EXTERNAL_ID_WITH_ACTUAL_TASK = "stmtSelectProcessNodeByExternalIdWithActualTask";
	public static final String STMT_SELECT_PARENT_PROCESS_BY_TASK = "stmtSelectParentProcessByTask";
	public static final String STMT_SELECT_BY_FK_PARENT_PROCESS_NODE = "stmtSelectByFkParentProcessNode";
	//public static final String STMT_COUNT = "processStmtCount";
	
	//private long pk;	the primary key is now the process id
	private Date creationDate;
	private Date updateDate;
	private Long fkParentProcessNode;
	private Long fkRootProcessNode; // not null
	private ProcessNode parentProcessNode;
	private long processId; // not null, unique, primary key
	private String engineUrl; // not null
	private ProcessDetail detail;
	private List<Task> tasks;
	private List<Comment> commentList = new LinkedList<Comment>();
	private List<Attachment> attachmentList = new LinkedList<Attachment>();
	private Date deleteDate;
	

	/** This method returns the actual task. At the moment there is only
	 * one task associated with a process node. So we return only the first one.
	 * In other words: at the moment the task list will ever consist of only one task.
	 * 
	 * @return the actual task or null if no tasks are associated to this process node
	 */
	@Transient
	public Task getActualTask() {
		if (this.tasks != null)
			return this.tasks.get(0);
		else
			return null;
	}


	public void setActualTask(Task actualTask) {
		if (this.tasks == null)
			this.tasks = new LinkedList<Task>();
		
		// We insert the actualTask at the first position in the list so that
		// no other task will be returned when calling getActualTask
		this.tasks.add(0, actualTask);
	}

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkparentprocessnode")
	public List<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public String getEngineUrl() {
		return engineUrl;
	}
	
	public void setEngineUrl(String engineUrl) {
		this.engineUrl = engineUrl;
	}
	
	public Long getFkParentProcessNode() {
		return fkParentProcessNode;
	}
	
	public void setFkParentProcessNode(Long fkParentProcessNode) {
		this.fkParentProcessNode = fkParentProcessNode;
	}
	
	//@Column(unique = true)
	@Id
	@Column(name = "pk_process_node", nullable = false)
	public long getProcessId() {
		return processId;
	}
	
	public void setProcessId(long processId) {
		this.processId = processId;
	}

	@Transient
	public ProcessDetail getDetail() {
		ProcessDetail processDetail = null;
		if (this.detail==null){
			try {
				ProcessEngine engine = ProcessEngineFactory
						.getProcessEngine(this.getEngineUrl());
				processDetail = engine.getProcessDetail(this.getProcessId());
			} catch (ProcessEngineException e) {
				// TODO: handle exception
			}
			this.setDetail(processDetail);
			}
		return this.detail;
	}

	/**
	 * This property will be set by a subclass of {@link ProcessTreeAccess}.
	 * 
	 * @param   detail
	 */
	public void setDetail(ProcessDetail detail) {
		this.detail = detail;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "fkparentprocessnode", name = "pk_process_node")
	public ProcessNode getParentProcessNode() {
		return parentProcessNode;
	}

	public void setParentProcessNode(ProcessNode parentProcessNode) {
		this.parentProcessNode = parentProcessNode;
	}

	public Long getFkRootProcessNode() {
		return fkRootProcessNode;
	}

	public void setFkRootProcessNode(Long fkRootProcessNode) {
		this.fkRootProcessNode = fkRootProcessNode;
	}
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_parent_process_node")
	public List<Attachment> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<Attachment> attachmentList) {
		this.attachmentList = attachmentList;
	}

	public void addAttachment(Attachment attachment) {
		if (! attachmentList.contains(attachment))
			attachmentList.add(attachment);
	}

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_root_process_node", updatable = false)
	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public void addComment(Comment comment) {
		if (! commentList.contains(comment))
			commentList.add(comment);
	}

	/*@Id
	@GeneratedValue
	@Column(name = "pk_process_node", nullable = false)
	public long getPk() {
		return pk;
	}*/
	
	/**
	 * Sets the primary key of this object.
	 * 
	 * @param   pk
	 *          The primary key
	 */
	/*public void setPk(long pk) {
		this.pk = pk;
	}*/
	
	/**
	 * Returns the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  creationDate
	 *          The creation time
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   creationDate
	 *          The creation time
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Returns the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  updateDate
	 *          The update time
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	
	/**
	 * Sets the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   updateDate
	 *          The update time
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	@Transient
	public String getGuid() {
		return this.getProcessId() + ":" + this.getEngineUrl();
	}
	
	@Transient
	public ProcessNode getRootProcessNode(){
		Long rootProcessPk = this.getFkRootProcessNode();
		return new ProcessApi().getProcessByPk(rootProcessPk);
	}


	public Date getDeleteDate() {
		return deleteDate;
	}


	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

}
