package de.tarent.wfms.processmiddleware.aggregator;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** bean class for the engine configuration file
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "engines")
public class Engines {

	@XmlElement(required = true, name = "engine")
	private List<String> identifiers;

	
	
	public List<String> getIdentifiers() {
		return identifiers;
	}

	public void setIdentifiers(List<String> identifiers) {
		this.identifiers = identifiers;
	}

}
