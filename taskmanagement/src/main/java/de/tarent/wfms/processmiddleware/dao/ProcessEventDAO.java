package de.tarent.wfms.processmiddleware.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import de.tarent.wfms.processmiddleware.data.ProcessEvent;

/** DAO for the bean ProcessEvent
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class ProcessEventDAO {

	private static ProcessEventDAO instance = null;

	
    public static synchronized ProcessEventDAO getInstance() {
        if (instance == null)
            instance = new ProcessEventDAO();
        return instance;
    }
    
    
    /** returns all process events for a process identified by its id
     * 
     * @param processId
     * @return
     */
    public List<ProcessEvent> getAllForProcess(long processId, EntityManager em) {
    	Query query = em.createNamedQuery(ProcessEvent.STMT_SELECT_ALL_BY_PROCESS);
    	query.setParameter("processId", processId);
    	return (List<ProcessEvent>) query.getResultList();
    }


    /** adds a process event
     * 
     * @param typeTaskCompleted
     * @param string
     * @param staffer
     */
	public void log(int type, long processId, String description, String staffer, EntityManager em) {
		ProcessEvent event = new ProcessEvent();
		event.setDate(new Date());
		event.setDescription(description);
		event.setType(type);
		event.setProcessId(processId);
		event.setUserId(staffer);
		
		boolean commit = false;
		try {
			if (!em.getTransaction().isActive()) {
				em.getTransaction().begin();
				commit = true;
			}
		} catch (IllegalStateException e) {
			// If this library is used in an enterprise context, we may not be
			// allowed to access the transaction. If so, we believe that the enterprise
			// transaction will rollback if something happens and don't care about
			// transaction handling.
			commit = false;
		}
		
		em.persist(event);
		if (commit)
			em.getTransaction().commit();
	}
	
}
