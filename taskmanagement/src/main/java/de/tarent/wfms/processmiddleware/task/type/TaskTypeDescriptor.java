/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.task.type;

import java.util.List;
import java.util.Map;

import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.data.TaskType;
import de.tarent.wfms.processmiddleware.task.MaskConnector;

/**
 * Describes a wrapper factory used for generation of mask lists.
 * <p>
 * Every task living inside the middleware has one or more external
 * masks associated that link to an external application.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class TaskTypeDescriptor
{        
    /**
     * Creates a new MaskConnector instance for the given maskConnectorClass.
     * 
     * @param maskConnectorClass
     * @param taskType
     * @param task
     * @param taskData
     * @return
     */
    protected MaskConnector createMaskConnector(Class maskConnectorClass, TaskType taskType, Task task, Map<String,Object> taskData)
    {
        MaskConnector connector = null;
        try
        {
            connector = (MaskConnector) maskConnectorClass.newInstance();
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException("Error instantiating mask connector " + maskConnectorClass.getCanonicalName(), e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException("Error instantiating mask connector " + maskConnectorClass.getCanonicalName(), e);
        }

        connector.setTaskData(taskData);
        connector.setTask(task);
        connector.setTaskType(taskType);
        
        return connector;
    }

    /**
     * Creates a new MaskConnector instance for the given maskConnectorClass.
     * 
     * @param maskConnectorClass
     * @param taskType
     * @param taskData
     * @return
     */
    protected MaskConnector createMaskConnector(String maskConnectorClassName, TaskType taskType, Task task, Map<String,Object> taskData)
    {
        try
        {
            return createMaskConnector(Class.forName(maskConnectorClassName), taskType, task, taskData);
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException("Error instantiating mask connector " + maskConnectorClassName, e);
        }
    }

    /**
     * Returns the list of MaskConnector instances each describing
     * a single external mask.
     */
	public abstract List<MaskConnector> getMaskConnectors(TaskType taskType, Task task, Map<String,Object> taskData);
}
