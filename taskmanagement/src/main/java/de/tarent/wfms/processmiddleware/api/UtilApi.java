/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */


package de.tarent.wfms.processmiddleware.api;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.config.Configuration;
import de.tarent.wfms.processmiddleware.dao.TaskTypeDAO;
import de.tarent.wfms.processmiddleware.data.CommentType;
import de.tarent.wfms.processmiddleware.data.ProcessEvent;
import de.tarent.wfms.processmiddleware.data.TaskType;
import de.tarent.wfms.processmiddleware.db.DBAccess;
import de.tarent.wfms.processmiddleware.util.GroovyScriptHelper;

public class UtilApi{     
    
	/** returns information about the version of this software
	 * 
	 * @return
	 */
    public String getBuildInfo(){
        return Configuration.getInstance().getParameter("buildInfo");
    }     

    
    public GroovyScriptHelper getGroovyScriptHelper(){    
        return GroovyScriptHelper.getInstance();
    }
    
    /**
     * Adds or substracts an amount to/from a given date. Format as follows:
     *      [+|-]n[h|d|w|m|y]
     * Examples:
     *      -5d     Substracts 5 days
     *      4m      adds 4 month
     *      12345   adds 12345 seconds
     * if given date is nil or not parseable, current date is used.
     * 
     * In the day scope, only weekdays are counted against toBeAdded.
     * 
     * @param oc
     * @param dateTime
     * @param toBeAdded
     * @return
     * @throws OctopusSecurityException
     */
    public GregorianCalendar addToDateTime(GregorianCalendar dateTime,String toBeAdded){

        if (dateTime==null)
            dateTime = new GregorianCalendar();
        
        GregorianCalendar origStartTime = (GregorianCalendar)dateTime.clone();
        
        int unit = Calendar.SECOND;
        int value = 0;

        if (toBeAdded.startsWith("+"))
            toBeAdded = toBeAdded.substring(1);
        
        if (Character.isLetter(toBeAdded.charAt(toBeAdded.length()-1)))
        {
            switch (toBeAdded.charAt(toBeAdded.length()-1))
            {
                case 'h': unit = Calendar.HOUR_OF_DAY; break;
                case 'd': unit = Calendar.DAY_OF_MONTH; break;
                case 'w': unit = Calendar.WEEK_OF_YEAR; break;
                case 'm': unit = Calendar.MONTH; break;
                case 'y': unit = Calendar.YEAR; break;
            }
            value = Integer.parseInt(toBeAdded.substring(0, toBeAdded.length()-1));            
        }
        else
            value = Integer.parseInt(toBeAdded); 
            
        if (unit != Calendar.DAY_OF_MONTH)
            dateTime.add(unit, value);
        else
        {
            int signum = Integer.signum(value);
            value = Math.abs(value);
            while (value>0)
            {
                if ((signum==1 && dateTime.get(Calendar.DAY_OF_WEEK)!=Calendar.FRIDAY && dateTime.get(Calendar.DAY_OF_WEEK)!=Calendar.SATURDAY) 
                        || (signum==-1 && dateTime.get(Calendar.DAY_OF_WEEK)!=Calendar.SATURDAY && dateTime.get(Calendar.DAY_OF_WEEK)!=Calendar.SUNDAY))
                    value--;

                dateTime.add(Calendar.DAY_OF_MONTH, signum);
            }
        }

        // if in debugMode, the original time is always returned
        if (Configuration.getInstance().getParameterAsBoolean("debugMode"))
            return origStartTime;

        return dateTime;
    }
    
    
    public void addExternalLogEvent(ProcessEvent event) {
    	this.addExternalLogEvent(event, DBAccess.getEntityManager());
    }
    
    /** adds an event to the internal process log
     * 
     */
    public void addExternalLogEvent(ProcessEvent event, EntityManager em) {
		boolean transactionStarted = true;
		try {
			em.getTransaction().begin();
		} catch (IllegalStateException e) {
			// If this library is used in an enterprise context, we may not be
			// allowed to access the transaction. If so, we believe that the enterprise
			// transaction will rollback if something happens and don't care about
			// transaction handling.
			transactionStarted = false;
		}
		
    	em.persist(event);
    	
    	if (transactionStarted)
    		em.getTransaction().commit();
    }
    
    
    public void addTaskType(TaskType type) {
    	this.addTaskType(type, DBAccess.getEntityManager());
    }
    
    
    /** inserts a new task type (if it is really new)
     * 
     */
    public void addTaskType(TaskType type, EntityManager em) {    	
    	boolean transactionStarted = true;
		try {
			em.getTransaction().begin();
		} catch (IllegalStateException e) {
			// If this library is used in an enterprise context, we may not be
			// allowed to access the transaction. If so, we believe that the enterprise
			// transaction will rollback if something happens and don't care about
			// transaction handling.
			transactionStarted = false;
		}
		
		if (TaskTypeDAO.getInstance().getTypeByUri(type.getUri(), em) == null) {
			// does not exist yet
    		em.persist(type);
		}
    		
    	if (transactionStarted) {
    		em.getTransaction().commit();
    	}
    }
    

    public void addCommentType(CommentType commentType) {
    	this.addCommentType(commentType, DBAccess.getEntityManager());
    }
    
    /** inserts a new comment type
     * 
     * @param commentType
     */
	public void addCommentType(CommentType commentType, EntityManager em) {
		boolean transactionStarted = true;
		try {
			em.getTransaction().begin();
		} catch (IllegalStateException e) {
			// If this library is used in an enterprise context, we may not be
			// allowed to access the transaction. If so, we believe that the enterprise
			// transaction will rollback if something happens and don't care about
			// transaction handling.
			transactionStarted = false;
		}
		
		em.persist(commentType);
		
		if (transactionStarted)
			em.getTransaction().commit();
	}
    
}
