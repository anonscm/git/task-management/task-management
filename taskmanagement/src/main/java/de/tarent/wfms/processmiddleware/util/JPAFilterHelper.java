package de.tarent.wfms.processmiddleware.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;

import de.tarent.commons.datahandling.ListFilter;
import de.tarent.commons.datahandling.ListFilterOperator;

/** This class provides methods for adding filters to JPA queries.
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class JPAFilterHelper {
	
	/** adds a filter to a query defined by its name and the bean where it is stored and
	 * returns a Query object that can be executed
	 * 
	 * @param bean
	 * @param queryName
	 * @param filter
	 * @return
	 */
	public static Query getQueryWithFilter(Class bean, String queryName, ListFilter filter, EntityManager em) {
		// the query
		String queryString = null;
		
		// find correct query in bean
		NamedQuery[] queries = ((NamedQueries) bean.getAnnotation(NamedQueries.class)).value();
		for (int i = 0; i< queries.length; i++) {
			if (queries[i].name().equals(queryName)) {
				// we found the correct query. store the query so that we can extend it
				queryString = queries[i].query();
				break;
			}
		}
		
		String filterQueryPart = getWhereRPNList(filter.getFilterList());
		
		// add filter part to query
		if (queryString.contains("where"))
			queryString += " and " + filterQueryPart;
		else
			queryString += " where " + filterQueryPart;

		// sorting
		if (filter.getSortField() != null && !filter.getSortField().equals("")) {
			queryString += " order by " + filter.getSortField();
		
			if (filter.getSortDirection() != null)
				queryString += " " + filter.getSortDirection();
		}
			
		// TODO This is a hack because class ListFilterOperator
		// in tarent-commons was written for SQL, not for HQL
		queryString = queryString.replace('[', '(');
		queryString = queryString.replace(']', ')');
		
		Query query = em.createQuery(queryString);
		
		if (filter.useLimit())
			query.setMaxResults(filter.getLimit());
		
		query.setFirstResult(filter.getStart());
		
		return query;
	}
	
	
    private static String getWhereRPNList(List filterList) {
		if (filterList.size() == 0)
			return null;

		int paramCounter = 0;
		LinkedList stack = new LinkedList();
		boolean valueExpected = false;
		String lastColumnName = null;

		// we iterate over all tokens
		for (Iterator iter = filterList.iterator(); iter.hasNext();) {
			Object tokenObject = iter.next();

			if (tokenObject instanceof ListFilterOperator) {
				// the token is an Operator
				// get the consuming args from the stack, 
				// concatenate them and push it back to the stack

				valueExpected = false;
				ListFilterOperator op = (ListFilterOperator) tokenObject;

				if (!op.isConnectionOperator()) {
					String columnName = "";
					String value = "";
					if (2 == op.getConsumingArsg()) {
						value = (String) stack.removeLast();
						columnName = (String) stack.removeLast();
					} else
						columnName = (String) stack.removeLast();
					if (ListFilterOperator.IN.equals(op)) {
						//stack.add(columnName + " in ?");
						stack.add(columnName + " in " + value);
					} else {
						if (value.equals(""))
							stack.add(columnName + " " + op.toString());
						else if (value.equals("null"))
							stack.add(columnName + " " + op.toString()
									+ " " + value + " ");	
						else	
							stack.add(columnName + " " + op.toString()
								+ " '" + value + "' ");
					}
				} else {
					String left = null;
					String right = null;
					if (2 == op.getConsumingArsg()) {
						right = (String) stack.removeLast();
						left = (String) stack.removeLast();
					} else {
						right = (String) stack.removeLast();
					}
					stack.add(left + " " + op.toString() + " "
							+ right);
				}

			} else {
				// the token is an operand
				
				if (valueExpected) {
					// value
					// make the preparedStatementKey unique, but readable
					//String preparedStatementKey = lastColumnName + "-"
					//		+ (paramCounter++);
					//stack.add(preparedStatementKey);
					if (tokenObject instanceof Date) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String asString = format.format((Date) tokenObject);
						stack.add(asString);
					} else {
						if (tokenObject != null)
							stack.add(tokenObject.toString());
						else
							stack.add("null");
					}
					
					valueExpected = false;
				} else {
					// column
					stack.add((String) tokenObject.toString());
					valueExpected = true; // we added a column; the next one has to be a value
					lastColumnName = tokenObject.toString();
				}
			}
		}

		return " " + (String) stack.removeLast();
	}
	
}
