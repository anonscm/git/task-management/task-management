/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on Jul 18, 2007
 */

package de.tarent.wfms.processmiddleware.task;

import java.util.Map;

import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.data.TaskType;

/**
 * Abstract superclass, storing some task information.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class TaskDataStorer
{
    private TaskType taskType = null;
    private Map<String, Object> taskData = null;
    private Task task = null;
    
    public void setTaskData(Map<String, Object> taskData)
    {
        this.taskData = taskData;
    }

    public void setTaskType(TaskType taskType)
    {
        this.taskType = taskType;
    }

    public Map<String, Object> getTaskData()
    {
        return taskData;
    }

    public TaskType getTaskType()
    {
        return taskType;
    }

    public Task getTask()
    {
        return task;
    }

    public void setTask(Task task)
    {
        this.task = task;
    }
}
