/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.data;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The Class CommentType.
 * 
 *  @author Christian Preilowski (c.preilowski@tarent.de), Martin Pelzer
 */
@NamedQueries({
	@NamedQuery(name = CommentType.STMT_SELECT_ONE_BY_PK, query = "select x from CommentType as x where pk = :id"),
	@NamedQuery(name = CommentType.STMT_SELECT_ONE_BY_NAME, query = "select x from CommentType as x where name = :name"),
	@NamedQuery(name = CommentType.STMT_SELECT_ALL, query = "select x from CommentType as x")
})
@Entity
@Table(name = "comment_type")
public class CommentType {

	public static final String STMT_SELECT_ONE_BY_PK = "stmtSelectCommentTypeByPk";
	public static final String STMT_SELECT_ONE_BY_NAME = "stmtSelectCommentTypeByName";
	public static final String STMT_SELECT_ALL = "stmtSelectAllCommentTypes";
	
	
	private long pk;
	private Date creationDate;
	private Date updateDate;
	private List<Comment> comments;
	
	/** The name. */
	private String name; 
	
	/** The shortcut. */
	private String shortcut; 

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the shortcut.
	 * 
	 * @return the shortcut
	 */
	public String getShortcut() {
		return shortcut;
	}

	/**
	 * Sets the shortcut.
	 * 
	 * @param shortcut the new shortcut
	 */
	public void setShortcut(String shortcut) {
		this.shortcut = shortcut;
	}

	@Id
	@GeneratedValue
	@Column(name = "pk_comment_type", nullable = false)
	public long getPk() {
		return pk;
	}
	
	/**
	 * Sets the primary key of this object.
	 * 
	 * @param   pk
	 *          The primary key
	 */
	public void setPk(long pk) {
		this.pk = pk;
	}
	
	/**
	 * Returns the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  creationDate
	 *          The creation time
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   creationDate
	 *          The creation time
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Returns the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  updateDate
	 *          The update time
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	
	/**
	 * Sets the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   updateDate
	 *          The update time
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	
	@OneToMany(mappedBy = "commentType")
	public List<Comment> getComments() {
		return comments;
	}

	
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
}
