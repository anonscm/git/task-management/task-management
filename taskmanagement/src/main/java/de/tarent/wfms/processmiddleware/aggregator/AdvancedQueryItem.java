package de.tarent.wfms.processmiddleware.aggregator;

import java.io.Serializable;

/** Item of the advanced query part of the process filter.
 * One item contains a column name, an operation and a value.
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class AdvancedQueryItem implements Serializable {
	
	private static final long serialVersionUID = 1395289187865464327L;
	
	
	private static final String OPERATION_EQUALS = "=";
	private static final String OPERATION_ST = "<";
	private static final String OPERATION_GT = ">";
	private static final String OPERATION_STE = "<=";
	private static final String OPERATION_GTE = ">=";
	
	
	private String columnName;
	private String operation;
	private Object value;
	
	
	public AdvancedQueryItem() { }
	
	
	public AdvancedQueryItem(String columnName, String operation, Object value) {
		this.columnName = columnName;
		this.operation = operation;
		this.value = value;
	}
	
	
	public String getColumnName() {
		return columnName;
	}
	
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	public String getOperation() {
		return operation;
	}
	
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
}
