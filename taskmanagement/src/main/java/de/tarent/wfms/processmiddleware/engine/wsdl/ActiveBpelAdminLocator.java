/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 *
 * ActiveBpelAdminLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package de.tarent.wfms.processmiddleware.engine.wsdl;

public class ActiveBpelAdminLocator extends org.apache.axis.client.Service implements ActiveBpelAdmin {

	private static final long serialVersionUID = 1L;

	public ActiveBpelAdminLocator() {
    }


    public ActiveBpelAdminLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ActiveBpelAdminLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ActiveBpelAdminPort
    private java.lang.String ActiveBpelAdminPort_address = "http://workflow-server.example.com:8080/active-bpel/services/ActiveBpelAdmin";

    public java.lang.String getActiveBpelAdminPortAddress() {
        return ActiveBpelAdminPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ActiveBpelAdminPortWSDDServiceName = "ActiveBpelAdminPort";

    public java.lang.String getActiveBpelAdminPortWSDDServiceName() {
        return ActiveBpelAdminPortWSDDServiceName;
    }

    public void setActiveBpelAdminPortWSDDServiceName(java.lang.String name) {
        ActiveBpelAdminPortWSDDServiceName = name;
    }

    public IAeActiveBpelAdmin getActiveBpelAdminPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ActiveBpelAdminPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getActiveBpelAdminPort(endpoint);
    }

    public IAeActiveBpelAdmin getActiveBpelAdminPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ActiveBpelAdminPortSoapBindingStub _stub = new ActiveBpelAdminPortSoapBindingStub(portAddress, this);
            _stub.setPortName(getActiveBpelAdminPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setActiveBpelAdminPortEndpointAddress(java.lang.String address) {
        ActiveBpelAdminPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (IAeActiveBpelAdmin.class.isAssignableFrom(serviceEndpointInterface)) {
                ActiveBpelAdminPortSoapBindingStub _stub = new ActiveBpelAdminPortSoapBindingStub(new java.net.URL(ActiveBpelAdminPort_address), this);
                _stub.setPortName(getActiveBpelAdminPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ActiveBpelAdminPort".equals(inputPortName)) {
            return getActiveBpelAdminPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://docs.active-endpoints/wsdl/activebpeladmin/2007/01/activebpeladmin.wsdl", "ActiveBpelAdmin");
    }

    private java.util.HashSet ports = null;

    @SuppressWarnings("unchecked")
	public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://docs.active-endpoints/wsdl/activebpeladmin/2007/01/activebpeladmin.wsdl", "ActiveBpelAdminPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ActiveBpelAdminPort".equals(portName)) {
            setActiveBpelAdminPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
