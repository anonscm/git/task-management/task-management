/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.dao.ProcessNodeDAO;
import de.tarent.wfms.processmiddleware.data.ProcessNode;

public class DirectProcessTreeAccess extends ProcessTreeAccess {

	private static Logger log = Logger.getLogger(DirectProcessTreeAccess.class.getName());
	
	
	protected DirectProcessTreeAccess() {}

	@Override
	public ProcessNode getProcess(Long processPk, EntityManager em) {
		ProcessNodeDAO dao = ProcessNodeDAO.getInstance();
		ProcessNode node = dao.getProcessNodeByPk(processPk, em);
		ProcessEngine engine = ProcessEngineFactory.getProcessEngine(node.getEngineUrl());
		ProcessDetail processDetail;
		try {
			processDetail = engine.getProcessDetail(node.getProcessId());
			node.setDetail(processDetail);
		} catch (ProcessEngineException e) {
			log.log(Level.WARNING, "problems while communicating with process engine", e);
		}
    	return node;
	}

	@Override
	public ProcessNode getProcess(ProcessDetail processDetail, EntityManager em) {
		ProcessNodeDAO dao = ProcessNodeDAO.getInstance();
		ProcessNode node = dao.getProcessNodeByExternalProcessId(processDetail.getProcessId(), processDetail.getEngineIdentifier(), em);
		if (node != null)
			node.setDetail(processDetail);
		return node;
	}

	@Override
	public ProcessNode addProcess(ProcessDetail processDetail, EntityManager em) {
		ProcessNodeDAO dao = ProcessNodeDAO.getInstance();
		ProcessNode node = getProcess(processDetail, em);
		if (node == null) {
			//node = dao.insertProcessNode(processDetail.getProcessId(), processDetail.getEngineIdentifier(), null);
			return null;
		}
		node.setDetail(processDetail);
		return node;
	}

	@Override
	public List<ProcessNode> addAllProcesses(ProcessList processDetailList, EntityManager em) {
		List<ProcessNode> list = new LinkedList<ProcessNode>();
		for (ProcessDetail detail : processDetailList) {
			ProcessNode pn = addProcess(detail, em);
			if (pn != null)
				list.add(pn);
		}
		return list;
	}
}
