package de.tarent.wfms.processmiddleware.data;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/** represents a logged event for one process
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
@NamedQueries({
		@NamedQuery(name = ProcessEvent.STMT_SELECT_ALL_BY_PROCESS, query = "select event from ProcessEvent as event where processId = :processId order by date asc")
	})
@Table(name = "process_event")
@Entity
public class ProcessEvent {

	public static final String STMT_SELECT_ALL_BY_PROCESS = "stmtSelectAllByProcess";
	
	
	public static final int TYPE_PROCESS_STARTED = 1;
	public static final int TYPE_PROCESS_CLOSED = 2;
	public static final int TYPE_PROCESS_FORWARDED = 3;
	public static final int TYPE_TASK_COMPLETED = 4;
	public static final int TYPE_PROCESS_DELETED = 5;
	
	
	private long pk;				// primary key
	private long processId;			// id of the process this event belongs to
	private Date date;				// when did this event happen
	private String description;		// description of this event
	private int type;				// type of the event
	private String userId;			// if of the user who initiated this event
	
	
	public ProcessEvent() {
		this.date = new Date();
	}

	
	public ProcessEvent(int type) {
		this();
		this.type = type;
	}
	
	
	@Id
	@GeneratedValue
	public long getPk() {
		return pk;
	}


	public void setPk(long pk) {
		this.pk = pk;
	}


	public long getProcessId() {
		return processId;
	}


	public void setProcessId(long processId) {
		this.processId = processId;
	}


	public Date getDate() {
		return date;
	}
	
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public int getType() {
		return type;
	}
	
	
	public void setType(int type) {
		this.type = type;
	}
	
	
	public String getUserId() {
		return userId;
	}

	
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
