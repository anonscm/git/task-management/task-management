/**
 * AesProcessListResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package de.tarent.wfms.processmiddleware.engine.xsd;

public class AesProcessListResult  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int totalRowCount;
    private boolean completeRowCount;
    private AesProcessInstanceDetail[] rowDetails;
    private boolean empty;

    public AesProcessListResult() {
    }

    public AesProcessListResult(
           int totalRowCount,
           boolean completeRowCount,
           AesProcessInstanceDetail[] rowDetails,
           boolean empty) {
           this.totalRowCount = totalRowCount;
           this.completeRowCount = completeRowCount;
           this.rowDetails = rowDetails;
           this.empty = empty;
    }


    /**
     * Gets the totalRowCount value for this AesProcessListResult.
     * 
     * @return totalRowCount
     */
    public int getTotalRowCount() {
        return totalRowCount;
    }


    /**
     * Sets the totalRowCount value for this AesProcessListResult.
     * 
     * @param totalRowCount
     */
    public void setTotalRowCount(int totalRowCount) {
        this.totalRowCount = totalRowCount;
    }


    /**
     * Gets the completeRowCount value for this AesProcessListResult.
     * 
     * @return completeRowCount
     */
    public boolean isCompleteRowCount() {
        return completeRowCount;
    }


    /**
     * Sets the completeRowCount value for this AesProcessListResult.
     * 
     * @param completeRowCount
     */
    public void setCompleteRowCount(boolean completeRowCount) {
        this.completeRowCount = completeRowCount;
    }


    /**
     * Gets the rowDetails value for this AesProcessListResult.
     * 
     * @return rowDetails
     */
    public AesProcessInstanceDetail[] getRowDetails() {
        return rowDetails;
    }


    /**
     * Sets the rowDetails value for this AesProcessListResult.
     * 
     * @param rowDetails
     */
    public void setRowDetails(AesProcessInstanceDetail[] rowDetails) {
        this.rowDetails = rowDetails;
    }


    /**
     * Gets the empty value for this AesProcessListResult.
     * 
     * @return empty
     */
    public boolean isEmpty() {
        return empty;
    }


    /**
     * Sets the empty value for this AesProcessListResult.
     * 
     * @param empty
     */
    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AesProcessListResult)) return false;
        AesProcessListResult other = (AesProcessListResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.totalRowCount == other.getTotalRowCount() &&
            this.completeRowCount == other.isCompleteRowCount() &&
            ((this.rowDetails==null && other.getRowDetails()==null) || 
             (this.rowDetails!=null &&
              java.util.Arrays.equals(this.rowDetails, other.getRowDetails()))) &&
            this.empty == other.isEmpty();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getTotalRowCount();
        _hashCode += (isCompleteRowCount() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getRowDetails() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRowDetails());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRowDetails(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += (isEmpty() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AesProcessListResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "AesProcessListResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalRowCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "totalRowCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("completeRowCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "completeRowCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rowDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "rowDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "AesProcessInstanceDetail"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "empty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
