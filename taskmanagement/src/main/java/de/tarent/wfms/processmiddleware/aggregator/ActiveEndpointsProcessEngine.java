/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;
import org.apache.axis.EngineConfiguration;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.configuration.FileProvider;

import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.engine.wsdl.ActiveBpelAdminPortSoapBindingStub;
import de.tarent.wfms.processmiddleware.engine.wsdl.IAeActiveBpelAdmin;
import de.tarent.wfms.processmiddleware.engine.xsd.AdminFault;
import de.tarent.wfms.processmiddleware.engine.xsd.AesGetVariableDataType;
import de.tarent.wfms.processmiddleware.engine.xsd.AesProcessFilter;
import de.tarent.wfms.processmiddleware.engine.xsd.AesProcessFilterType;
import de.tarent.wfms.processmiddleware.engine.xsd.AesProcessInstanceDetail;
import de.tarent.wfms.processmiddleware.engine.xsd.AesProcessListType;
import de.tarent.wfms.processmiddleware.engine.xsd.AesProcessType;
import de.tarent.wfms.processmiddleware.engine.xsd.AesSetVariableDataType;
import de.tarent.wfms.processmiddleware.engine.xsd.AesStringResponseType;
import de.tarent.wfms.processmiddleware.util.Helper;
import de.tarent.wfms.processmiddleware.util.StorageObject;
import de.tarent.wfms.processmiddleware.util.TimedObjectCache;

public class ActiveEndpointsProcessEngine implements ProcessEngine {

	private static final Logger logger = Logger.getLogger(ActiveEndpointsProcessEngine.class.getName());

	private IAeActiveBpelAdmin engine;
	private String identifier;
	private Service service;


	public ActiveEndpointsProcessEngine(String identifier) {
		try {
			this.identifier = identifier;
			EngineConfiguration engineConfig = new FileProvider(Helper.getAxisConfigFileString());
			service = new Service(engineConfig);
			engine= new ActiveBpelAdminPortSoapBindingStub(new URL(identifier),service);
		} catch (AxisFault e) {
			logger.warning("AxisFault beim Erstellen einer ProcessEngine Instanz mit der url "+identifier+". Message: "+e.getMessage());
		} catch (MalformedURLException e) {
			logger.warning("ungueltige URL beim Erstellen einer ProcessEngine-Instanz mit der url "+identifier+". Message: "+e.getMessage());
		}

	}

	public ProcessDetail getProcessDetail(long processId) throws ProcessEngineException {
        String detailID = ProcessDetail.class.getName() +"#"+processId;
    	StorageObject requestedDetailSO = TimedObjectCache.getGlobalInstance().retrieveStorageObject(detailID);
    	ProcessDetail requestedDetail = null;
    	try {
	    	if(requestedDetailSO != null){
	    		requestedDetail = (ProcessDetail) (requestedDetailSO.getCacheSubject());
	    	} else {
	    		requestedDetail = new ProcessDetail((engine.getProcessDetail(new AesProcessType(processId))).getResponse());
	    		TimedObjectCache.getGlobalInstance().store(detailID, requestedDetail);
	    	}
    	} catch (RemoteException e) {
    		throw new ProcessEngineException(e);
    	}
		return requestedDetail;
	}

	public List<ProcessDetail> getProcessList(ProcessFilter filter) throws ProcessEngineException {
		List<ProcessDetail> list =new LinkedList<ProcessDetail>();
		try {
			list = makeProcessList(engine.getProcessList(new AesProcessFilterType(ProcessFiltertoAesProcessFilter(filter))));
		} catch (RemoteException e) {
			throw new ProcessEngineException(e);
		}
		return list;
	}
	
	public int getProcessCount(ProcessFilter filter) throws ProcessEngineException {
		try {
			return engine.getProcessCount(new AesProcessFilterType(ProcessFiltertoAesProcessFilter(filter)));
		} catch (RemoteException e) {
			throw new ProcessEngineException(e);
		}
	}
	
    public List<Long> getParentList(long processId) throws ProcessEngineException {
		try {
			return engine.getParentList(processId);
		} catch (RemoteException e) {
			throw new ProcessEngineException(e);
		}
	}

	public int getProcessState(long processId) throws ProcessEngineException {
		AesProcessType process = new AesProcessType(processId);
		AesStringResponseType response;
		try {
			response = engine.getProcessState(process);
		} catch (RemoteException e) {
			throw new ProcessEngineException(e);
		}
		return Integer.valueOf(response.getResponse()); // TODO evtl. stimmt das nicht und da muss noch ein Mapping rein
	}

	public String getIdentifier() {
		return this.identifier;
	}

    /**
     * TODO: Hack: The Process engine returns an error (RemoteException), if a variable is not 
     * present, to work with this, we return <code>null</code> in this case. See https://issues.tarent.de/show_bug.cgi?id=544
     * for details and progress on this issue.
     */
	public String getVariable(long processId, String xpath)	{
        String cacheKey = ActiveEndpointsProcessEngine.class.getName()+"#getVariable()"+processId+":"+xpath;
        StorageObject responseStringSO = TimedObjectCache.getGlobalInstance().retrieveStorageObject(cacheKey);
      
        if (responseStringSO != null)
            return (String)(responseStringSO.getCacheSubject());
     
        try {
        	String responseString;
            AesGetVariableDataType variable = new AesGetVariableDataType(processId, xpath);
            AesStringResponseType response = engine.getVariable(variable);
            responseString = response.getResponse();
            TimedObjectCache.getGlobalInstance().store(cacheKey, responseString);
            return responseString;
        } catch (RemoteException re) {
			logger.severe("getVariable() of the process Engine returned an error. We use 'null' as result. See https://issues.tarent.de/show_bug.cgi?id=544 for further details.");
            return null;
        }
	}

	public void resumeProcess(long processId) throws ProcessEngineException {
		AesProcessType process = new AesProcessType(processId);
		try {
			engine.resumeProcess(process);
		} catch (AdminFault e) {
			throw new ProcessEngineException(e);
		} catch (RemoteException e) {
			throw new ProcessEngineException(e);
		}
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public void setVariable(long processId, String xpath, String variableData) throws ProcessEngineException {
		AesSetVariableDataType variable = new AesSetVariableDataType(processId, xpath,variableData);
		try {
			engine.setVariable(variable);
		} catch (RemoteException e) {
			throw new ProcessEngineException(e);
		}
	}

	public void suspendProcess(long processId) throws ProcessEngineException {
		AesProcessType process = new AesProcessType(processId);
		try {
			engine.suspendProcess(process);
		} catch (AdminFault e) {
			throw new ProcessEngineException(e);
		} catch (RemoteException e) {
			throw new ProcessEngineException(e);
		}
	}

	public void terminateProcess(long processId) throws ProcessEngineException {
		AesProcessType process = new AesProcessType(processId);
		try {
			engine.terminateProcess(process);
		} catch (AdminFault e) {
			throw new ProcessEngineException(e);
		} catch (RemoteException e) {
			throw new ProcessEngineException(e);
		}
	}



	//*********** UTILITY METHODS *******************

	public AesProcessFilter ProcessFiltertoAesProcessFilter(ProcessFilter pFilter){
		AesProcessFilter filter = new AesProcessFilter();
		
		// FIXME filter.setAdvancedQuery(pFilter.getAdvancedQuery()); commented out because of changig advanced query to support different operators
		filter.setListStart(pFilter.getListStart());
		filter.setMaxReturn(pFilter.getMaxReturn());
		filter.setProcessCompleteEnd(pFilter.getProcessCompleteEnd());
		filter.setProcessCompleteStart(pFilter.getProcessCompleteStart());
		filter.setProcessCreateEnd(pFilter.getProcessCreateEnd());
		filter.setProcessCreateStart(pFilter.getProcessCreateStart());
		filter.setProcessName(pFilter.getProcessName());
		filter.setProcessState(pFilter.getProcessState());
		return filter;
	}

	private List<ProcessDetail> makeProcessList(AesProcessListType list){
		List<ProcessDetail> processList= new LinkedList<ProcessDetail>();
		if (list!=null){
			ProcessDetail detail;
			AesProcessInstanceDetail[] array = list.getResponse().getRowDetails();
			for (int i=0;i<array.length;i++){
				detail=AesProcessInstanceDetailToProcessDetail(array[i]);
				processList.add(detail);
			}
		}
		return processList;
	}

	public ProcessDetail AesProcessInstanceDetailToProcessDetail(AesProcessInstanceDetail input){
		ProcessDetail detail = new ProcessDetail();
		detail.setEnded(input.getEnded());
		detail.setName(input.getName());
		detail.setProcessId(input.getProcessId());
		detail.setStarted(input.getStarted());
		detail.setState(input.getState());
		detail.setStateReason(input.getStateReason());
		return detail;
	}

	public void doCallback(Task task, Map<String, Object> returnData) throws ProcessEngineException {
		try {
			EngineConfiguration engineConfig = new FileProvider(Helper
					.getAxisConfigFileString());
			Service service = new Service(engineConfig);
	
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(task.getReplyToUrl());
	
			call.setOperationName(new QName("http://schemas.tarent.de/taskmgmt/",
					"addTaskCallback"));
			call.setEncodingStyle("http://schemas.xmlsoap.org/soap/encoding/");
	
			call
					.addParameter("taskId", new QName(
							"http://www.w3.org/2001/XMLSchema", "string"),
							ParameterMode.IN);
			call.addParameter("returnData", new QName(
					"http://xml.apache.org/xml-soap", "Map"), ParameterMode.IN);
			List<Object> params = new ArrayList<Object>();
	
			params.add(task.getTaskId());
			params.add(returnData);
	
			call.invoke(params.toArray());
		} catch (RemoteException e) {
			throw new ProcessEngineException("could not invoke callback for task " + task.getTaskId(), e);
		} catch (ServiceException e) {
			throw new ProcessEngineException("could not create call on service for callback on task " + task.getTaskId(), e);
		}
	}


}
