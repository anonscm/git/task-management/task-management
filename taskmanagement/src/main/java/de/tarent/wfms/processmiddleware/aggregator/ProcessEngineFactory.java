package de.tarent.wfms.processmiddleware.aggregator;

/** decides whether to instantiate a ActiveEndpointsProcessEngine
 * or a JBPMProcessEngine
 *  
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class ProcessEngineFactory {

	/** TODO for now we decide depending on the protocol of the url
	 * 
	 * @param url
	 * @return
	 */
	public static ProcessEngine getProcessEngine(String identifier) {
		if (identifier.startsWith("jbpm")) {
			return new JbpmProcessEngine(identifier);
		} else {
			return new ActiveEndpointsProcessEngine(identifier);
		}
	}
	
}
