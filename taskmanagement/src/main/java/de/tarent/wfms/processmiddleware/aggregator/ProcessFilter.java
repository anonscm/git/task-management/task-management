/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

import java.util.LinkedList;
import java.util.List;

/**
 * This class implements the ProcessFilter that is used to configure 
 * the query for processes. 
 * The method getProcessList implemented by ProcessEngine expects an instance of class ProcessFilter. 
 * If no filter is needed create an emnpty filter and pass it to the method.
 *
 * @author Steffi Tinder, tarentGmbH
 *
 */
public class ProcessFilter extends ListingProcessFilter implements java.io.Serializable {
    
	private static final long serialVersionUID = 5841071519015698440L;
	
	private List<AdvancedQueryItem> advancedQuery;

    private java.util.Calendar processCompleteEnd;

    //disbled due to new AdminAPI
    //private java.util.Calendar processCompleteEndNextDay;

    private java.util.Calendar processCompleteStart;

    private java.util.Calendar processCreateEnd;
    
    //disbled due to new AdminAPI
    //private java.util.Calendar processCreateEndNextDay;

    private java.util.Calendar processCreateStart;

    private javax.xml.namespace.QName processName;
    
    private int processState;
    
    private List<String> processOwner;
    
    private String sortColumn;
    
    private String sortDir;

    /**
     * The process must not be in the defined end state, as is specified by the states name.
     */
    private String notInDefinedEndState;

    public ProcessFilter() {
    }

    public ProcessFilter(
           int listStart,
           int maxReturn,
           List<AdvancedQueryItem> advancedQuery,
           java.util.Calendar processCompleteEnd,
           //java.util.Calendar processCompleteEndNextDay,
           java.util.Calendar processCompleteStart,
           java.util.Calendar processCreateEnd,
           //java.util.Calendar processCreateEndNextDay,
           java.util.Calendar processCreateStart,
           javax.xml.namespace.QName processName,
           int processState) {
        super(
            listStart,
            maxReturn);
        this.advancedQuery = advancedQuery;
        this.processCompleteEnd = processCompleteEnd;
        //this.processCompleteEndNextDay = processCompleteEndNextDay;
        this.processCompleteStart = processCompleteStart;
        this.processCreateEnd = processCreateEnd;
        //this.processCreateEndNextDay = processCreateEndNextDay;
        this.processCreateStart = processCreateStart;
        this.processName = processName;
        this.processState = processState;
    }

    public ProcessFilter(
            int listStart,
            int maxReturn,
            List<AdvancedQueryItem> advancedQuery,
            java.util.Calendar processCompleteEnd,
            //java.util.Calendar processCompleteEndNextDay,
            java.util.Calendar processCompleteStart,
            java.util.Calendar processCreateEnd,
            //java.util.Calendar processCreateEndNextDay,
            java.util.Calendar processCreateStart,
            javax.xml.namespace.QName processName,
            int processState, String notInDefinedEndState) {
         this( listStart, maxReturn, advancedQuery, processCompleteEnd, processCompleteStart, processCreateEnd, processCreateStart, processName, processState );
         this.notInDefinedEndState = notInDefinedEndState;
     }

    /**
     * Gets the advancedQuery value for this AeProcessFilter.
     * 
     * @return advancedQuery
     */
    public List<AdvancedQueryItem> getAdvancedQuery() {
        return advancedQuery;
    }


    /**
     * Sets the advancedQuery value for this AeProcessFilter.
     * 
     * @param advancedQuery
     */
    public void setAdvancedQuery(List<AdvancedQueryItem> advancedQuery) {
        this.advancedQuery = advancedQuery;
    }


    /**
     * Gets the processCompleteEnd value for this AeProcessFilter.
     * 
     * @return processCompleteEnd
     */
    public java.util.Calendar getProcessCompleteEnd() {
        return processCompleteEnd;
    }


    /**
     * Sets the processCompleteEnd value for this AeProcessFilter.
     * 
     * @param processCompleteEnd
     */
    public void setProcessCompleteEnd(java.util.Calendar processCompleteEnd) {
        this.processCompleteEnd = processCompleteEnd;
    }


    /*
    public java.util.Calendar getProcessCompleteEndNextDay() {
        return processCompleteEndNextDay;
    }*/


   /* 
    public void setProcessCompleteEndNextDay(java.util.Calendar processCompleteEndNextDay) {
        this.processCompleteEndNextDay = processCompleteEndNextDay;
    }*/


    /**
     * Gets the processCompleteStart value for this AeProcessFilter.
     * 
     * @return processCompleteStart
     */
    public java.util.Calendar getProcessCompleteStart() {
        return processCompleteStart;
    }


    /**
     * Sets the processCompleteStart value for this AeProcessFilter.
     * 
     * @param processCompleteStart
     */
    public void setProcessCompleteStart(java.util.Calendar processCompleteStart) {
        this.processCompleteStart = processCompleteStart;
    }


    /**
     * Gets the processCreateEnd value for this AeProcessFilter.
     * 
     * @return processCreateEnd
     */
    public java.util.Calendar getProcessCreateEnd() {
        return processCreateEnd;
    }


    /**
     * Sets the processCreateEnd value for this AeProcessFilter.
     * 
     * @param processCreateEnd
     */
    public void setProcessCreateEnd(java.util.Calendar processCreateEnd) {
        this.processCreateEnd = processCreateEnd;
    }


//   public java.util.Calendar getProcessCreateEndNextDay() {
//        return processCreateEndNextDay;
//    }


//  public void setProcessCreateEndNextDay(java.util.Calendar processCreateEndNextDay) {
//        this.processCreateEndNextDay = processCreateEndNextDay;
//    }


    /**
     * Gets the processCreateStart value for this AeProcessFilter.
     * 
     * @return processCreateStart
     */
    public java.util.Calendar getProcessCreateStart() {
        return processCreateStart;
    }


    /**
     * Sets the processCreateStart value for this AeProcessFilter.
     * 
     * @param processCreateStart
     */
    public void setProcessCreateStart(java.util.Calendar processCreateStart) {
        this.processCreateStart = processCreateStart;
    }


    /**
     * Gets the processName value for this AeProcessFilter.
     * 
     * @return processName
     */
    public javax.xml.namespace.QName getProcessName() {
        return processName;
    }


    /**
     * Sets the processName value for this AeProcessFilter.
     * 
     * @param processName
     */
    public void setProcessName(javax.xml.namespace.QName processName) {
        this.processName = processName;
    }


    /**
     * Gets the processState value for this AeProcessFilter.
     * 
     * @return  processState
     *          one of the following values:
     *          {@link ProcessStates#STATE_ANY}
     *          {@link ProcessStates#STATE_COMPENSATABLE}
     *          {@link ProcessStates#STATE_COMPLETED}
     *          {@link ProcessStates#STATE_COMPLETED_OR_FAULTED}
     *          {@link ProcessStates#STATE_FAULTED}
     *          {@link ProcessStates#STATE_RUNNING}
     *          {@link ProcessStates#STATE_SUSPENDED}
     *          {@link ProcessStates#STATE_SUSPENDED_FAULTING}
     *          {@link ProcessStates#STATE_SUSPENDED_MANUAL}
     *          {@link ProcessStates#STATE_SUSPENDED_PROGRAMMATIC}
     */
    public int getProcessState() {
        return processState;
    }


    /**
     * Sets the processState value for this AeProcessFilter.
     * 
     * @param   processState
     *          one of the following values:
     *          {@link ProcessStates#STATE_ANY}
     *          {@link ProcessStates#STATE_COMPENSATABLE}
     *          {@link ProcessStates#STATE_COMPLETED}
     *          {@link ProcessStates#STATE_COMPLETED_OR_FAULTED}
     *          {@link ProcessStates#STATE_FAULTED}
     *          {@link ProcessStates#STATE_RUNNING}
     *          {@link ProcessStates#STATE_SUSPENDED}
     *          {@link ProcessStates#STATE_SUSPENDED_FAULTING}
     *          {@link ProcessStates#STATE_SUSPENDED_MANUAL}
     *          {@link ProcessStates#STATE_SUSPENDED_PROGRAMMATIC}
     */
    public void setProcessState(int processState) {
        this.processState = processState;
    }

    /** returns the list of all process owners
     * 
     * @return
     */
	public List<String> getProcessOwner() {
		return processOwner;
	}

	/** sets the list of process owners
	 * 
	 * @param processOwner
	 */
	public void setProcessOwner(List<String> processOwner) {
		this.processOwner = processOwner;
	}
    
	/** adds a name to the list of process owners
	 * 
	 * @param processOwner
	 */
	public void addProcessOwner(String processOwner) {
		if (this.processOwner == null)
			this.processOwner = new LinkedList<String>();
		this.processOwner.add(processOwner);
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public String getSortDir() {
		return sortDir;
	}

	public void setSortDir(String sortDir) {
		this.sortDir = sortDir;
	}

	public String getNotInDefinedEndState()
	{
		return this.notInDefinedEndState;
	}
 
	public void setNotInDefinedEndState( String notInDefinedEndState )
	{
		this.notInDefinedEndState = notInDefinedEndState;
	}

	public boolean definesNotInDefinedEndStateConstraint()
	{
		return ( this.notInDefinedEndState != null );
	}
 }
