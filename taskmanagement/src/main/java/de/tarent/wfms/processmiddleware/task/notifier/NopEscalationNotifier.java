package de.tarent.wfms.processmiddleware.task.notifier;

import de.tarent.wfms.processmiddleware.data.Task;

public class NopEscalationNotifier implements EscalationNotifier 
{
	public void notify(Task arg0) 
	{
		return;
	}
}
