package de.tarent.wfms.processmiddleware.aggregator;

/** exeption that is thrown by ProcessEngine connectors when
 * something wents wrong while communicating with the engine
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class ProcessEngineException extends Exception {

	
	
	public ProcessEngineException() {
		super();
	}
	
	
	public ProcessEngineException(Throwable cause) {
		super(cause);
	}
	
	
	public ProcessEngineException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
