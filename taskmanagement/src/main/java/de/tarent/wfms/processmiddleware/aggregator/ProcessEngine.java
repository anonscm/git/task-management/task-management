/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import de.tarent.wfms.processmiddleware.data.Task;

public interface ProcessEngine {

	public void suspendProcess(long processId) throws ProcessEngineException;

	public void resumeProcess(long processId) throws ProcessEngineException;

	public void terminateProcess(long processId) throws ProcessEngineException;

	public List<ProcessDetail> getProcessList(ProcessFilter filter)
			throws ProcessEngineException;
	
	public int getProcessCount(ProcessFilter filter)
			throws ProcessEngineException;
	
	public List<Long> getParentList(long processId)
			throws ProcessEngineException;

	public ProcessDetail getProcessDetail(long processId)
			throws ProcessEngineException;

	public int getProcessState(long processId) throws ProcessEngineException;

	/**
	 * @param processId the id of the process the wanted variable belongs to
	 * @param xpath the variable path, for example <code> /process/variables/variable[@name='input']</code>
	 * @return an xml-fragment that represents the variable specified by <code> xpath </code> 
	 * @throws RemoteException
	 */
	public String getVariable(long processId, String xpath)
			throws ProcessEngineException;

	public void setVariable(long processId, String xpath, String value)
			throws ProcessEngineException;

	public String getIdentifier();

	public void setIdentifier(String identifier);
	
	public void doCallback(Task task, Map<String, Object> returnData) throws ProcessEngineException;

}