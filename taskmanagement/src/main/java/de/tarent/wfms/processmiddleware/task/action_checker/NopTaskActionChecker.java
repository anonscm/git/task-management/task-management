
package de.tarent.wfms.processmiddleware.task.action_checker;

import de.tarent.wfms.processmiddleware.data.Task;



/**
 * default Class for task action checks. All checks return true and no messages
 * are provided.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 */
public class NopTaskActionChecker extends TaskActionCheckerImpl {
	
	/**
	 * Instantiates a new default task action checker.
	 * 
	 * @param task the task
	 */
	public NopTaskActionChecker(Task task){
		super(task);
	}

	/* (non-Javadoc)
	 * @see de.tarent.wfms.processmiddleware.util.TaskActionChecker#getCloseMessage()
	 */
	public String getCloseMessage() {
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tarent.wfms.processmiddleware.util.TaskActionChecker#getForwardMessage()
	 */
	public String getForwardMessage() {
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tarent.wfms.processmiddleware.util.TaskActionChecker#getReopenMessage()
	 */
	public String getReopenMessage() {
		return null;
	}


	/* (non-Javadoc)
	 * @see de.tarent.wfms.processmiddleware.util.TaskActionChecker#isCloseable()
	 */
	public boolean isCloseable() {
		return true;
	}

	/* (non-Javadoc)
	 * @see de.tarent.wfms.processmiddleware.util.TaskActionChecker#isForwardable()
	 */
	public boolean isForwardable() {
		return true;
	}

	/* (non-Javadoc)
	 * @see de.tarent.wfms.processmiddleware.util.TaskActionChecker#isReopenable()
	 */
	public boolean isReopenable() {
		return true;
	}

	/* (non-Javadoc)
	 * @see de.tarent.wfms.processmiddleware.util.TaskActionChecker#isReopenable()
	 */
	public String getResubmitMessage() {
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tarent.wfms.processmiddleware.util.TaskActionChecker#isReopenable()
	 */
	public boolean isResubmitable() {
		return true;
	}

}
