/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

import java.util.Calendar;
import javax.xml.namespace.QName;

import de.tarent.wfms.processmiddleware.engine.xsd.AesProcessInstanceDetail;



public class ProcessDetail {

	private long processId;

	private String type;

	private QName name;

	private String responsibleStafferId;

	private Calendar started;

	private Calendar ended;

	private String engineIdentifier;

	private int state;

	private int stateReason;

	public ProcessDetail(){

	}
	public ProcessDetail(AesProcessInstanceDetail detail){
		this.ended=detail.getEnded();
		this.name=detail.getName();
		this.processId=detail.getProcessId();
		this.started=detail.getStarted();
		this.state=detail.getState();
		this.stateReason=detail.getStateReason();
	}

	public String getEngineIdentifier() {
		return engineIdentifier;
	}

	public void setEngineIdentifier(String engineIdentifier) {
		this.engineIdentifier = engineIdentifier;
	}

	public QName getName() {
		return name;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public String getResponsibleStafferId() {
		return responsibleStafferId;
	}

	public void setResponsibleStafferId(String responsibleStafferId) {
		this.responsibleStafferId = responsibleStafferId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Calendar getEnded() {
		return ended;
	}

	public void setEnded(Calendar ended) {
		this.ended = ended;
	}

	public long getProcessId() {
		return processId;
	}

	public void setProcessId(long processId) {
		this.processId = processId;
	}

	public Calendar getStarted() {
		return started;
	}

	public void setStarted(Calendar started) {
		this.started = started;
	}

	/**
	 * Returns the process state.
	 * 
	 * @return  the process state.
	 *          Must be one of the following values:
	 *          {@link ProcessStates#PROCESS_COMPENSATABLE}
	 *          {@link ProcessStates#PROCESS_COMPLETE}
	 *          {@link ProcessStates#PROCESS_FAULTED}
	 *          {@link ProcessStates#PROCESS_LOADED}
	 *          {@link ProcessStates#PROCESS_REASON_NONE}
	 *          {@link ProcessStates#PROCESS_RUNNING}
	 *          {@link ProcessStates#PROCESS_SUSPENDED}
	 */
	public int getState() {
		return state;
	}

	/**
	 * Sets the process state.
	 * 
	 * @param   state
	 *          the process state.
	 *          Must be one of the following values:
	 *          {@link ProcessStates#PROCESS_COMPENSATABLE}
	 *          {@link ProcessStates#PROCESS_COMPLETE}
	 *          {@link ProcessStates#PROCESS_FAULTED}
	 *          {@link ProcessStates#PROCESS_LOADED}
	 *          {@link ProcessStates#PROCESS_REASON_NONE}
	 *          {@link ProcessStates#PROCESS_RUNNING}
	 *          {@link ProcessStates#PROCESS_SUSPENDED}
	 */
	public void setState(int state) {
		this.state = state;
	}

	public int getStateReason() {
		return stateReason;
	}

	public void setStateReason(int stateReason) {
		this.stateReason = stateReason;
	}

	public String toString(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("{");
		buffer.append("processId: "+processId +" , ");
		if (engineIdentifier!=null)buffer.append("engineURL: "+engineIdentifier +" , ");
		if (name!=null)buffer.append("name: "+ name +" , ");
		if (started!=null && started.getTime()!=null)buffer.append("started: "+started.getTime() +" , ");
		if (ended!=null && ended.getTime()!=null)buffer.append("ended: "+ended.getTime()+" , ");
		buffer.append("state: "+state +" , ");
		buffer.append("stateReason: "+stateReason +"}");
		return buffer.toString();
	}

}