/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import de.tarent.commons.datahandling.ListFilterOperator;
import de.tarent.wfms.processmiddleware.util.JPAListFilter;

import java.util.List;

public class AttachmentFilter extends JPAListFilter {
	
	public AttachmentFilter() {
        addFilter("fkParentProcessNode", ListFilterOperator.IN);
        addFilter("deleted", ListFilterOperator.EQ);
        addFilter("pdmsId", ListFilterOperator.EQ);
        setUseLimit(false);
	}

	public AttachmentFilter setPdmsId (String pdmsId) {
		setFilterValue("pdmsId", pdmsId);
		return this;
	}
	
	public AttachmentFilter setDeleted (boolean deleted) {
		setFilterValue("deleted", deleted);
		return this;
	}
	
    public AttachmentFilter setParenProcessPkList(List<Long> parentProcessPKs) {
        setFilterValue("fkParentProcessNode", parentProcessPKs);
        return this;
    }
    
}
