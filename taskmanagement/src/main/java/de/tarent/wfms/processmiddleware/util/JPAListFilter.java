/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.wfms.processmiddleware.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import de.tarent.commons.datahandling.ListFilterImpl;
import de.tarent.commons.datahandling.ListFilterOperator;


/**
 * Default implementation for the list filter.
 *
 * At the moment this implementation only supports simple filters concatenated by and.
 *
 */
public class JPAListFilter extends ListFilterImpl {

    int count;
    int start = 0;
    int limit = 20;
    String sortField;
    String sortDirection;
    boolean useLimit = false;

    List possibleAndFilterParams;
    /** supplied filter list from the request */       
    List requestFilterList = null;
    

    /**
     * Initializes the values of the filter
     * @return Returns this.
     */
    public JPAListFilter init(String prefix) {
        setUseLimit(true);
            
        if (possibleAndFilterParams != null) {
            for (Iterator iter = possibleAndFilterParams.iterator(); iter.hasNext();) {
                FilterParam param = (FilterParam)iter.next();
                String key = prefix+"."+param.columnName;
                Object filterValue = param.value;
                
                if (filterValue instanceof String && ((String)filterValue).trim().length() == 0)
                    param.value = null;
                else
                    if(filterValue != null)
                        param.value = filterValue;
                    
            }
        }

        return this;
    }
    
    /**
     * Returns the column for sorting of the list
     */
    public String getSortField() {
        return sortField;
    }

    /**
     * Sets the column or colums the SQL should use for ORDER BY
     * You can use one Popertyname that will be mapped to a database column
     * or simply the columnname as a String. Even multiple colums as a 
     * concatenated String like "table1.column1, table2.column1" can be used 
     * @param sortField
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    /**
     * Returns the sort direction. This may be one of the constants DIRECTION_ASC, DIRECTION_DESC.
     */
    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        if (DIRECTION_DESC.equalsIgnoreCase(sortDirection))
            this.sortDirection = DIRECTION_DESC;
        else
            this.sortDirection = DIRECTION_ASC;
    }
    
    /**
     * Returns the total count of records in the list.
     */
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Returns the start position. (Beginning at 0)
     */
    public int getStart() {
    	/**
    	 * this solution "return Math.min(start, getLastPageStart());" considered the special case
    	 * of a web application that needs full pages of data. So instead of the given offset the
    	 * offset of the current page was used.
    	 * This should be considered when setting the offset instead of cerrecting it in the database
    	 * layer. 
    	 */
    	return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    /**
     * Returns the count of records to return, beginning at position start.
     */
    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * Returns the count of pages for the total list, based on the values for limit and count.
     */
    public int getPages() {
        return  count / limit + (count % limit == 0 ? 0 : 1);        
    }

    /**
     * Returns the current page number.
     */
    public int getPage() {
        return  start / limit + 1;        
    }

    /**
     * Returns the start position of the previous page, based on the values for start, limit and count.
     */
    public int getPreviousPageStart() {
		int prev = start - limit;
        if (prev < 0)
            return 0;
        return prev;
    }
    
    /**
     * Returns the start position of the next page, based on the values for start, limit and count.
     */
    public int getNextPageStart() {
        int next = start + limit;
        if (next > getLastPageStart())
            return getLastPageStart();
        return next;
    }

    /**
     * Returns the start position of the last page, based on the values for start, limit and count.
     */
    public int getLastPageStart() {
        return count - (count % limit) - (count != 0 && count % limit  == 0 ? limit : 0);
    }

    public boolean useLimit() {
        return useLimit;
    }

    public void setUseLimit(boolean newUseLimit) {
        this.useLimit = newUseLimit;
    }
   
    public void addFilter(String columnName, ListFilterOperator relation) {
        // TODO: It would be more clean to store the ListFilterOperator directly
        addFilterParam(new FilterParam(columnName, relation.toString()));
    }


    public void addFilter(String columnName, String relation) {
        addFilterParam(new FilterParam(columnName, relation));
    }

    public void addFilter(String columnName, ListFilterOperator relation, Object defaultValue) {
        addFilterParam(new FilterParam(columnName, relation.toString(), defaultValue));
    }

    public void addFilter(String columnName, String relation, Object defaultValue) {
        addFilterParam(new FilterParam(columnName, relation, defaultValue));
    }

    public void setFilterValue(String columnName, Object value) {
        if (possibleAndFilterParams == null)
            return;

        for (Iterator iter = possibleAndFilterParams.iterator(); iter.hasNext();) {
            FilterParam param = (FilterParam)iter.next();
            if (param.columnName.equals(columnName))
                param.value = value;
        }        
    }

    public Object getFilterValue(String columnName) {
        if (possibleAndFilterParams == null)
            return null;

        for (Iterator iter = possibleAndFilterParams.iterator(); iter.hasNext();) {
            FilterParam param = (FilterParam)iter.next();
            if (param.columnName.equals(columnName))
                return param.value;
        }        
        return null;
    }
    
    public void clearAllFilterValues(String columnName, Object value) {
        if (possibleAndFilterParams == null)
            return;

        for (Iterator iter = possibleAndFilterParams.iterator(); iter.hasNext();) {
            FilterParam param = (FilterParam)iter.next();
            param.value = null;
        }        
    }

    public void addFilterParam(FilterParam param) {
        if (possibleAndFilterParams == null)
            possibleAndFilterParams = new ArrayList();
        possibleAndFilterParams.add(param);        
    }

    /**
     * Checks wether all not optional paramters are set
     */
    public boolean isComplete() {
        for (Iterator iter = possibleAndFilterParams.iterator(); iter.hasNext();) {
            FilterParam param = (FilterParam)iter.next();
            if ( ! (param.optional || param.isSet()) )
                 return false;
        }
        return true;
    }

    /**
     * Returns the filter tree as list in Reverse Polish Notation (Umgekehrte Polnische Notation UPN)     
     * The first (left) operand is allways the String column name, the right operand may be of any type.
     * The operators are ListFilterOperator objects.
     */
    public List getFilterList() {
        int stackCounter = 0;
        LinkedList filterList = new LinkedList();
        if (requestFilterList != null)
            filterList.addAll(requestFilterList);
        if (possibleAndFilterParams != null) {
            for (Iterator iter = possibleAndFilterParams.iterator(); iter.hasNext();) {
                FilterParam param = (FilterParam)iter.next();
                if (param.isSet()) {
                    filterList.add(param.columnName);
                    filterList.add(param.value);
                    filterList.add(new ListFilterOperator(param.relation));
                    stackCounter++;
                }
            }
            while (--stackCounter > 0) {
                filterList.add(ListFilterOperator.AND);
            }
        }
        if (requestFilterList != null && requestFilterList.size() > 0 
            && possibleAndFilterParams != null && possibleAndFilterParams.size() > 0)
            filterList.add(ListFilterOperator.AND);

        return filterList;
    }
    

    public class FilterParam {
        boolean optional = true;
        String columnName;
        String relation;
        Object value;

        public FilterParam(String columnName, String relation) {
            this.columnName = columnName;
            this.relation = relation;
        }
        public FilterParam(String columnName, String relation, Object value) {
            this.columnName = columnName;
            this.relation = relation;
            this.value = value;
        }

        public FilterParam(String columnName, String relation, boolean optional) {
            this.columnName = columnName;
            this.relation = relation;
            this.optional = optional;
        }
        
        public boolean isSet() {
            if (value instanceof String)
                return ((String)value).trim().length() > 0;
            return value != null;
        }
    }

	public int getStartForPage(int page) {
		if(page<=1)
			return 0;
		if((page-1) * limit >=getLastPageStart())
			return getLastPageStart();
		else		
			return (page-1) * limit;
	}
	
	public String getFilterName() {
		return toString();
	}
}
