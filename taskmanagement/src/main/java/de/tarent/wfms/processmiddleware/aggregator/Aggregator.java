/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

/**
 * This class fetches the process-lists of the different process-engines that are registered at the ZPD (zentraler Processdienst)
 * to aggregate the processes into a single process-list. 
 * The process-business-objects are provided with the engine id of its original engine. 
 * A process ID can only be unique in combination with its engine ID.
 *
 * @author Steffi Tinder, tarentGmbH
 */
public class Aggregator {

	private static final Logger logger = Logger.getLogger(Aggregator.class.getName());

	private static List<ProcessEngine> processEngineList;
	private static Aggregator instance;

	private Aggregator() {
	    super();
    }
    
	public static Aggregator getInstance() {
		if (instance==null) instance = new Aggregator();
		return instance;
	}

	public ProcessList getProcessList(ProcessFilter filter) {
		return buildAggregatedProcessList(filter);
	}

	private ProcessList buildAggregatedProcessList(ProcessFilter filter) {
		int count = 0;
		updateEngineList();
		ProcessList processList = new ProcessList(new LinkedList<ProcessDetail>(), true);
		for (ProcessEngine engine : processEngineList){
			List<ProcessDetail> currentList;
			try {
				currentList = engine.getProcessList(filter);
				if (!(currentList==null)){
					insertEngineID(currentList, engine.getIdentifier());
					processList.addAll(currentList);
				}
				count += engine.getProcessCount(filter);
			} catch (ProcessEngineException e) {
				//if any of the engines is unreachable, log and set isComplete to false
				logger.log(Level.INFO, "engine unreachable: "+engine.getIdentifier(), e);
				processList.setComplete(false);				
			}
		}
		processList.setCount(count);
		return processList;
	}

	private void insertEngineID(List<ProcessDetail> processList, String engineID)
	{
		for (ProcessDetail process : processList)
		{
			process.setEngineIdentifier(engineID);
		}
	}

	public void updateEngineList()
	{
		// TODO provide an xml configuration file for configuring the providers
		// available and the resource bundles used with each provider
		//
		// i.e.
		//
		// <engineproviders>
		//   <engineprovider>
		//     <fqClassname>de.tarent.wfms.processmiddleware.aggregator.LocalCentralProcessingEngineProvider</fqClassname>
		//     <fqResourceBundleName>localEngineProvider</fqResourceBundleName>
		//   </engineprovider>
		// </engineproviders>
		try
		{
			processEngineList = CentralProcessEngineProviderFactory.getCentralProcessEngineProvider( "de.tarent.wfms.processmiddleware.aggregator.LocalCentralProcessEngineProvider", "localEngineProvider" ).getProcessEngineList();
		}
		catch( ClassNotFoundException e)
		{
			logger.log( Level.SEVERE, "The requested provider cannot be found.", e );
			e.printStackTrace();
		}
		catch( InstantiationException e )
		{
			logger.log( Level.SEVERE, "The requested provider cannot be instantiated.", e );
			e.printStackTrace();
		}
		catch( IllegalAccessException e )
		{
			logger.log( Level.SEVERE, "The requested provider cannot be instantiated due to security restrictions.", e );
			e.printStackTrace();
		}
	}
}
