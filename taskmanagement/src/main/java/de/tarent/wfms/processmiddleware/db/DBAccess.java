/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.db;

//import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/**
 * @author Alex Maier
 * @author Sebastian Mancke, tarent GmbH
 * @author Martin Pelzer, tarent GmbH
 */
public class DBAccess {

	//private static Logger logger = Logger.getLogger(DBAccess.class.getName());
    
    private static EntityManager entityManager = null;
    
    /** returns the EntityManager object for accessing the
     * database via JPA
     * 
     * @return
     */
    public static EntityManager getEntityManager() {
    	if (entityManager == null) {
    		EntityManagerFactory factory = Persistence.createEntityManagerFactory("processmiddleware");
    		entityManager = factory.createEntityManager();
    	}
    	
    	return entityManager;
    }
    
    
    public static void injectEntityManager(EntityManager manager) {
    	DBAccess.entityManager = manager;
    }
    
}
