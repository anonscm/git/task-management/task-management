package de.tarent.wfms.processmiddleware.aggregator;

import java.util.ArrayList;
import java.util.List;

/**
 * The abstract class AbstractCentralProcessEngineProvider represents
 * the root of a hierarchy of derived concepts.
 * 
 * All implementations of this abstract provider interface must be
 * implemented as singletons.
 * 
 * @author Carsten Klein
 * @date 2008-09-10
 */
public abstract class AbstractCentralProcessEngineProvider
{
	
	/**
	 * The internally managed list of registered process engine providers. 
	 */
	private static ArrayList< AbstractCentralProcessEngineProvider > registeredProviders;

	/**
	 * The publicly visible and readonly name of the resource bundle used
	 * for configuring this.  
	 */
	private String resourceBundleName;

	/**
	 * This class may not be instantiated.
	 */
	protected AbstractCentralProcessEngineProvider()
	{
	}

	/**
	 * Gets the name of the resource bundle. This can either be a fully qualified path
	 * or simply a name.
	 *  
	 * @return String the resource bundle name
	 */
	public final String getResourceBundleName()
	{
		return this.resourceBundleName;
	}

	/**
	 * Initializes this and registers itself with the list of registered process engine
	 * providers.
	 *  
	 * @param instance
	 * @param resourceBundleName
	 */
	protected void initialize( AbstractCentralProcessEngineProvider instance, String resourceBundleName )
	{
		this.resourceBundleName = resourceBundleName;

		AbstractCentralProcessEngineProvider.registerProvider( instance );
	}

	/**
	 * Override this method for your custom initialization code.
	 * You must call super.initialize( resourceBundleName ) after
	 * having finished your custom initialization.
	 *  
	 * @param resourceBundleName
	 */
	public void initialize( String resourceBundleName )
	{
		this.initialize( this, resourceBundleName );
	}

	/**
	 * Gets a list of the available process engines provided by this.
	 * 
	 * @return List< ProcessEngine > a list of available process engine instances or the empty list
	 */
	public abstract List< ProcessEngine > getProcessEngineList();

	/**
	 * Returns whether the specified engineUrl is a valid engine url that can
	 * be provided by any of the available instances of engine providers.
	 * 
	 * @param engineUrl
	 * @return boolean true whether engineUrl is a valid url
	 */
	public final static boolean isValidEngineUrl( String engineUrl )
	{
		boolean result = false;

		if ( AbstractCentralProcessEngineProvider.registeredProviders == null )
		{
			Aggregator.getInstance().updateEngineList();
		}
		for( AbstractCentralProcessEngineProvider provider : AbstractCentralProcessEngineProvider.registeredProviders )
		{
			List< ProcessEngine > engineList = provider.getProcessEngineList();
			for( ProcessEngine engine : engineList )
			{
				if ( engine.getIdentifier().equals( engineUrl ) )
				{
					result = true;
					break;
				}
			}
		}

		return result;
	}

	/**
	 * Gets a process engine instance for the specified engineUrl.
	 * 
	 * @param engineUrl
	 * @return ProcessEngine the instance of the engine or null
	 */
	public final static ProcessEngine getEngineForUrl( String engineUrl )
	{
		ProcessEngine result = null;

		if ( AbstractCentralProcessEngineProvider.registeredProviders == null )
		{
			Aggregator.getInstance().updateEngineList();
		}
		if ( isValidEngineUrl( engineUrl ) )
		{
			for( AbstractCentralProcessEngineProvider provider : AbstractCentralProcessEngineProvider.registeredProviders )
			{
				for( ProcessEngine engine : provider.getProcessEngineList() )
				{
					if ( engine.getIdentifier().equals( engineUrl ) )
					{
						result = engine;
						break;
					}
				}
			}
		}

		return result;
	}
	
	/**
	 * Use this method to register your provider instance with this.
	 *  
	 * @param provider
	 */
	protected static void registerProvider( AbstractCentralProcessEngineProvider provider )
	{
		if ( AbstractCentralProcessEngineProvider.registeredProviders == null )
		{
			AbstractCentralProcessEngineProvider.registeredProviders = new ArrayList< AbstractCentralProcessEngineProvider >();
		}

		AbstractCentralProcessEngineProvider.registeredProviders.add( provider );
	}

	/**
	 * Unregisters the specified provider instance with this.
	 * 
	 * @param provider
	 */
	protected static void unregisterProvider( AbstractCentralProcessEngineProvider provider )
	{
		AbstractCentralProcessEngineProvider.registeredProviders.remove( provider );
	}

	/**
	 * This method is used by the CentralProcessingEngineProviderFactory for finding
	 * any previously created instances.
	 *  
	 * @param classname
	 * @param resourceBundleName
	 * @return AbstractCentralProcessEngineProvider the instance or null
	 */
	static AbstractCentralProcessEngineProvider findProviderInstance( String classname, String resourceBundleName )
	{
		AbstractCentralProcessEngineProvider result = null;

		if ( AbstractCentralProcessEngineProvider.registeredProviders != null )
		{
			for( AbstractCentralProcessEngineProvider provider : AbstractCentralProcessEngineProvider.registeredProviders )
			{
				if ( provider.getClass().getCanonicalName() == classname )
				{
					if ( provider.getResourceBundleName() == resourceBundleName )
					{
						result = provider;
						break;
					}
				}
			}
		}

		return result;
	}
}
