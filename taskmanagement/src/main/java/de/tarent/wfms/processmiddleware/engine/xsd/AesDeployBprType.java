/**
 * AesDeployBprType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package de.tarent.wfms.processmiddleware.engine.xsd;

public class AesDeployBprType  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private java.lang.String bprFilename;
    private java.lang.String base64File;

    public AesDeployBprType() {
    }

    public AesDeployBprType(
           java.lang.String bprFilename,
           java.lang.String base64File) {
           this.bprFilename = bprFilename;
           this.base64File = base64File;
    }


    /**
     * Gets the bprFilename value for this AesDeployBprType.
     * 
     * @return bprFilename
     */
    public java.lang.String getBprFilename() {
        return bprFilename;
    }


    /**
     * Sets the bprFilename value for this AesDeployBprType.
     * 
     * @param bprFilename
     */
    public void setBprFilename(java.lang.String bprFilename) {
        this.bprFilename = bprFilename;
    }


    /**
     * Gets the base64File value for this AesDeployBprType.
     * 
     * @return base64File
     */
    public java.lang.String getBase64File() {
        return base64File;
    }


    /**
     * Sets the base64File value for this AesDeployBprType.
     * 
     * @param base64File
     */
    public void setBase64File(java.lang.String base64File) {
        this.base64File = base64File;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AesDeployBprType)) return false;
        AesDeployBprType other = (AesDeployBprType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bprFilename==null && other.getBprFilename()==null) || 
             (this.bprFilename!=null &&
              this.bprFilename.equals(other.getBprFilename()))) &&
            ((this.base64File==null && other.getBase64File()==null) || 
             (this.base64File!=null &&
              this.base64File.equals(other.getBase64File())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBprFilename() != null) {
            _hashCode += getBprFilename().hashCode();
        }
        if (getBase64File() != null) {
            _hashCode += getBase64File().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AesDeployBprType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "AesDeployBprType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bprFilename");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "bprFilename"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("base64File");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "base64File"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
