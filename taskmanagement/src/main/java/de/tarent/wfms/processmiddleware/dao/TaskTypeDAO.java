/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import java.util.List;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.data.TaskType;

/**
 * @author Hendrik Helwich
 *
 */
public class TaskTypeDAO {

	private static TaskTypeDAO instance = null;

	
    public static synchronized TaskTypeDAO getInstance() {
        if (instance == null)
            instance = new TaskTypeDAO();
        return instance;
    }

    
    @SuppressWarnings("unchecked")
	public TaskType getTypeByUri(String uri, EntityManager em) {
    	List list = em.createNamedQuery(TaskType.STMT_SELECT_BY_URI).setParameter("uri", uri).getResultList();
    	
    	if (list == null || list.size() == 0)
    		return null;
    	else if (list.size() != 1)
    		throw new RuntimeException("uri is not unique"); // db constraint! => should not happen
    	else
    		return (TaskType) list.get(0);
    }
    
    @SuppressWarnings("unchecked")
    public TaskType getTypeById(long id, EntityManager em) {
    	List list = em.createNamedQuery(TaskType.STMT_SELECT_ONE).setParameter("id", id).getResultList();
    	
    	if (list == null || list.size() == 0)
    		return null;
    	else if (list.size() != 1)
    		throw new RuntimeException("id is not unique"); // db constraint! => should not happen
    	else
    		return (TaskType) list.get(0);
    }
    
}
