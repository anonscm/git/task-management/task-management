/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.pdms;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.config.Configuration;
import de.tarent.wfms.processmiddleware.dao.AttachmentDAO;
import de.tarent.wfms.processmiddleware.dao.AttachmentFilter;
import de.tarent.wfms.processmiddleware.data.Attachment;
import de.tarent.wfms.processmiddleware.db.DBAccess;

public class RemoteApi extends TimerTask
{    
	private static Logger log = Logger.getLogger(RemoteApi.class.getName());
	
	private static RemoteApi instance = null;
	

	//TODO: function pdmsStore and pdmsExpire do not adapt the attachment table when called
	//      directly by soap. The canges will not be visible in the staffer intranet view.
    private static final long EXPIRE_PERIOD = 60000L;

    private static String FILENAME_PATTERN = ".+--.+\\.\\d+";


    private static String documentPath;
    private Timer timer = null;
    
    
    private RemoteApi(){
    	
    }
    
    
    public static RemoteApi getInstance() {
    	if (instance == null) {
    		instance = new RemoteApi();
    		instance.initialize();
    	}
    	return instance;
    }
    
    
    // Service Methods
    
    private String cleanupString(String input)
    {
        return input.replace(':', '_').replace('.', '_').replace('/', '_');
    }
    
    private String createCookie()
    {
        return cleanupString(new java.rmi.server.UID().toString());
    }
    
    private File createFile(String processGuid, String docId, int priority, long ttl)
    {
        return new File(documentPath  + "/" + cleanupString(docId) + "--" + cleanupString(processGuid) + "--[" + priority + "]." + ttl);
    }
    
    // FIXME writeMethod
    private void moveFileToArchiv(File f){
    	f.delete();
    }
    
    private String getDocumentIDByFile(String filename){
    	return filename.split("--")[0];
    }
    
    private void deletFileFromDatabase(String filename, EntityManager em){
   
    	AttachmentDAO attachmentDao = AttachmentDAO.getInstance();
    	String docId = getDocumentIDByFile(filename);
    	Attachment attachment = null;
		attachment = (Attachment) (attachmentDao.getAll(new AttachmentFilter().setPdmsId(docId), em).get(0));
		attachment.setDeleted(true);
		// attachmentDao.update(attachment); updates are no longer needed
    }

    /**
     * Tests, if the supplied filename conforms to the creation pattern.
     */
    private boolean isValidFileName(String filename) {
        return filename.matches(FILENAME_PATTERN);
    }

    private File[] listFilesByDocId(final String docId)
    {
        File dir = new File(documentPath);

        return dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name)
            {
                if (!isValidFileName(name))
                    return false;

                if (name.startsWith(cleanupString(docId)))
                    return true;
                else
                    return false;
            }
        });        
    }
    
    private String[] listFileNamesByProcessGuid(final String processGuid)
    {
        File dir = new File(documentPath);

        return dir.list(new FilenameFilter() {
            public boolean accept(File dir, String name)
            {
                if (!isValidFileName(name))
                    return false;

                if (name.contains("--" + cleanupString(processGuid) + "--"))
                    return true;
                else
                    return false;
            }
        });        
    }

    private File[] listFilesByProcessGuid(final String processGuid)
    {
        File dir = new File(documentPath);

        return dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name)
            {
                if (!isValidFileName(name))
                    return false;

                if (name.contains("--" + cleanupString(processGuid) + "--"))
                    return true;
                else
                    return false;
            }
        });        
    }

    private File searchFile(String docId) throws IOException
    {
        File[] files = listFilesByDocId(docId);
        
        if (files.length>1)
            throw new IOException("Duplicate docId detected: " + docId);
        else if (files.length==0)
            return null;
        else
            return files[0];
    }

    private long getTtl(File file)
    {
        String ttlValue = file.getName().replaceAll("^[^.]+", "").replace(".", "");
        return Long.parseLong(ttlValue);
    }
    
    private void runExpireJob()
    {
    	
    	// test if documentPath is configured
    	if (documentPath == null) {
    		log.log(Level.SEVERE, "Error while cleaning expired documents: document path is unconfigured. Work timer will be canceled...");
    		// canceling timer
//    		try {
				cleanup();
//			} catch (OctopusSecurityException e) {
//				log.log(Level.SEVERE, "Error while canceling work timer...", e);
//			}
    	}
    		
    	try {
            File dir = new File(documentPath);
            
            File[] expiredFiles = dir.listFiles(new FilenameFilter() {
                    public boolean accept(File dir, String name)
                    {
                        
                        if (!isValidFileName(name))
                            return false;
                        
                        File thisFile = new File(dir, name);
                        long ttl = getTtl(thisFile);
                        long createDate = thisFile.lastModified();
                        long currentDate = System.currentTimeMillis();
                        
                        if (currentDate<(createDate + ttl))
                            return false;
                        else
                            return true;
                    }
                    
                });                
            
            for (int i=0; i<expiredFiles.length; i++){
                File expiredFile = expiredFiles[i];
                deletFileFromDatabase(expiredFile.getName(), DBAccess.getEntityManager());
                if(expiredFile.getName().contains("[0]")){
                	expiredFile.delete();
                }else{
                	moveFileToArchiv(expiredFile);
                }
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "error while cleaning expired documents", e);            
        }
    }

    // timer Task Methods
    
    @Override
    public void run()
    {
        runExpireJob();   
    }
    
    // Worker Methods

    public String pdmsStore(String processGuid, long ttl,
            int priority,byte[] document) throws IOException
    {
        String docId = createCookie();
        File outputFile = createFile(processGuid, docId, priority, ttl);
        
        BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(outputFile));
        os.write(document);
        os.flush();
        os.close();
        
        return docId;
    }

    public byte[] pdmsRetrieve(String docId) throws IOException
    {
    	System.out.println("HIER "+docId);
        File document = searchFile(docId);
        
        if (document==null)
            throw new IOException("Document not found.");
        
        InputStream is = new FileInputStream(document);
        long length = document.length();
        byte[] bytes = new byte[(int)length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) 
        {
            offset += numRead;
        }
    
        if (offset < bytes.length) 
            throw new IOException("Could not completely read file " + document.getName());

        is.close();

        return bytes;
    }

    public List pdmsList(String processGuid) 
    {
        return Arrays.asList(listFileNamesByProcessGuid(processGuid));
    }


    public void pdmsExpire(String processGuid, EntityManager em) 
    {
        File[] files = listFilesByProcessGuid(processGuid);
      
        for (int i=0; i<files.length; i++){
        	File file = files[i];
            deletFileFromDatabase(file.getName(), em);
            if(file.getName().contains("[0]")){
            	file.delete();
            }else{
            	moveFileToArchiv(file);
            }
        }
    }


    public void cleanup() 
    {
        // Stopping timer
        timer.cancel();
    }


    public void initialize() 
    {
    	documentPath = Configuration.getInstance().getParameter("pdmsPath");
    	
        // Starting timer
        timer = new Timer();
        timer.scheduleAtFixedRate(this, new Date(), EXPIRE_PERIOD);
    }
}
