/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import de.tarent.wfms.processmiddleware.data.CommentType;
import java.util.List;

import javax.persistence.EntityManager;

/**
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
public class CommentTypeDAO {

	private static CommentTypeDAO instance = null;

	
    public static synchronized CommentTypeDAO getInstance() {
        if (instance == null)
            instance = new CommentTypeDAO();
        return instance;
    }
    
    
    /**
     * Retrieves the comment type by the supplied name.
     */     
    public CommentType getTypeByName(String name, EntityManager em) {
    	CommentType commentType = (CommentType) em.createNamedQuery(CommentType.STMT_SELECT_ONE_BY_NAME).setParameter("name", name).getSingleResult();

    	return commentType;        
    }
    
    /**
     * Retrieves the comment type by the supplied pk.
     */     
    public CommentType getTypeByPk(Long pk, EntityManager em) {
    	CommentType commentType = (CommentType) em.createNamedQuery(CommentType.STMT_SELECT_ONE_BY_PK).setParameter("id", pk).getSingleResult();
        
    	return commentType;        
    }


	@SuppressWarnings("unchecked")
	public List<CommentType> getAll(EntityManager em) {
		return (List<CommentType>) em.createNamedQuery(CommentType.STMT_SELECT_ALL).getResultList();
	}
    
}
