package de.tarent.wfms.processmiddleware.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.dao.AttachmentTypeDAO;
import de.tarent.wfms.processmiddleware.data.AttachmentType;
import de.tarent.wfms.processmiddleware.data.ProcessNode;
import de.tarent.wfms.processmiddleware.db.DBAccess;
import de.tarent.wfms.processmiddleware.pdms.RemoteApi;

public class AttachmentApi {

	public String storeFile(ProcessNode process, int priority, byte[] document)
	throws IOException {
		//store attachment
		String pdmsId;
		pdmsId = RemoteApi.getInstance().pdmsStore(process.getGuid(), 10000000000L, priority,
				document);
		return pdmsId;
	}
	
	public Map<Long, AttachmentType> getAttachmentTypeMap() {
		return this.getAttachmentTypeMap(DBAccess.getEntityManager());
	}
	
	public Map<Long, AttachmentType> getAttachmentTypeMap(EntityManager em) {
		List<AttachmentType> list = null;
		Map<Long, AttachmentType> map = null;

		list = AttachmentTypeDAO.getInstance().getAll(em);

		map = new HashMap<Long, AttachmentType>();
		for (AttachmentType type : list)
			map.put(type.getPk(), type);
		return map;
	}
	
	
	public long getRealMimeTyp(String mimetype) {
		return this.getRealMimeTyp(mimetype, DBAccess.getEntityManager());
	}
	
	/**
	 * TODO Set default MimeType
	 * 
	 * @param oc
	 * @param mimetype
	 * @return
	 */
	public long getRealMimeTyp(String mimetype, EntityManager em) {
		long id = 0;
		Map<Long, AttachmentType> mimeTypes = getAttachmentTypeMap(em);
		Iterator<AttachmentType> iter = mimeTypes.values().iterator();
		while (iter.hasNext()) {
			AttachmentType tmp = iter.next();
			if (tmp.getName().equalsIgnoreCase(mimetype))
				id = tmp.getPk();
		}
		return id;
	}
	
	// TODO add this method
//	public Attachment getAttachmentByPk(Long attachmentPK) {
//		Attachment attachment = null;
//		if (attachmentPK != null) {
//			AttachmentDAO adao = AttachmentDAO.getInstance();
//			attachment = adao.getAttachmentByPk(new Long(attachmentPK));
//		}
//		return attachment;
//	}
}
