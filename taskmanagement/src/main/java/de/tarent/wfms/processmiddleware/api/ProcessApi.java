package de.tarent.wfms.processmiddleware.api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.xml.namespace.QName;

import de.tarent.wfms.processmiddleware.aggregator.Aggregator;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngine;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngineException;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngineFactory;
import de.tarent.wfms.processmiddleware.aggregator.ProcessList;
import de.tarent.wfms.processmiddleware.aggregator.ProcessStates;
import de.tarent.wfms.processmiddleware.aggregator.ProcessTreeAccess;
import de.tarent.wfms.processmiddleware.aggregator.WorkerProcessFilter;
import de.tarent.wfms.processmiddleware.dao.AttachmentDAO;
import de.tarent.wfms.processmiddleware.dao.AttachmentFilter;
import de.tarent.wfms.processmiddleware.dao.AttachmentTypeDAO;
import de.tarent.wfms.processmiddleware.dao.CommentDAO;
import de.tarent.wfms.processmiddleware.dao.CommentFilter;
import de.tarent.wfms.processmiddleware.dao.ProcessEventDAO;
import de.tarent.wfms.processmiddleware.dao.ProcessNodeDAO;
import de.tarent.wfms.processmiddleware.dao.TaskDAO;
import de.tarent.wfms.processmiddleware.data.Attachment;
import de.tarent.wfms.processmiddleware.data.AttachmentType;
import de.tarent.wfms.processmiddleware.data.Comment;
import de.tarent.wfms.processmiddleware.data.CommentType;
import de.tarent.wfms.processmiddleware.data.ProcessEvent;
import de.tarent.wfms.processmiddleware.data.ProcessNode;
import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.db.DBAccess;

/**
 * This class provides methods for dealing with processes.
 * 
 * @author Christian Preilowski, tarent GmbH
 * @author Martin Pelzer, tarent GmbH
 * 
 */
public class ProcessApi {

	private static Logger log = Logger.getLogger(TaskApi.class.getName());

	public static final String getVariableXpath(String variableName) {
		return "/process/variables/variable[@name='" + variableName + "']";
	}

	// ###############PROCES RELATED METHODS ##########
	
	public ProcessNode getProcessByPk(Long processPk) {
		return this.getProcessByPk(processPk, DBAccess.getEntityManager());
	}
	
	/**
	 * returns a process object belonging to the given primary key
	 * 
	 * @param processPk
	 *            primary key of the process to return
	 * @return
	 * @throws ProcessEngineException 
	 * @throws SQLException 
	 * @throws MalformedURLException 
	 */
	public ProcessNode getProcessByPk(Long processPk, EntityManager em) {
		return ProcessTreeAccess.getInstance().getProcess(processPk, em);
	}

	
	public ProcessNode getProcessByExternalID(String engineURL, Long externalProcessId) {
		return this.getProcessByExternalID(engineURL, externalProcessId, DBAccess.getEntityManager());
	}
	
	/**
	 * returns a process identified by its external id
	 * 
	 * @param engineURL
	 * @param externalProcessId
	 * @return
	 */
	public ProcessNode getProcessByExternalID(String engineURL, Long externalProcessId, EntityManager em) {
		ProcessNode processNode = null;
		ProcessNodeDAO pndao = ProcessNodeDAO.getInstance();
		processNode = pndao.getProcessNodeByExternalProcessId(externalProcessId, engineURL, em);

		// if process node == null, this process node has not be added to the
		// process middleware by now
		// -> create it
		if (processNode == null)
			processNode = pndao.insertProcessNode(externalProcessId, engineURL, null, em);

		return processNode;
	}

	
	public void forwardProcess(Long processPk, String stafferId) {
		this.forwardProcess(processPk, stafferId, DBAccess.getEntityManager());
	}
	
	/**
	 * forwards a process to another staffers
	 * 
	 * @param processPk
	 *            the process to forward
	 * @param stafferId
	 *            the staffer to forward the process to
	 */
	public void forwardProcess(Long processPk, String stafferId, EntityManager em) {
		ProcessNode process = ProcessTreeAccess.getInstance().getProcess(processPk, em);
		this.forwardProcess(process, stafferId, em);
	}
	
	
	public void forwardProcess(Long processId, String engineUrl, String stafferId) {
		this.forwardProcess(processId, engineUrl, stafferId, DBAccess.getEntityManager());
	}
	
	/**
	 * forwards a process to another staffers
	 * 
	 */
	public void forwardProcess(Long processId, String engineUrl, String stafferId, EntityManager em) {
		ProcessNode process = ProcessNodeDAO.getInstance().getProcessNodeByExternalProcessId(processId, engineUrl, em);
		this.forwardProcess(process, stafferId, em);
	}
	
	
	private void forwardProcess(ProcessNode process, String stafferId, EntityManager em) {
		try {
			ProcessEngine engine = ProcessEngineFactory.getProcessEngine(process.getEngineUrl());
			engine.setVariable(process.getDetail().getProcessId(), getVariableXpath(ServiceConstants.PROCESS_OWNER_VARIABLE), stafferId);
	
			// log
			ProcessEventDAO.getInstance().log(ProcessEvent.TYPE_PROCESS_FORWARDED, process.getProcessId(),
					"Prozess \"" + process.getProcessId() + "\" wurde weitergeleitet an \"" + stafferId + "\".", null, em);
		} catch (ProcessEngineException e) {
			log.severe(ServiceConstants.ERROR_IN_PROCESS_ENGINE);
			log.log(Level.SEVERE, ServiceConstants.ERROR_IN_PROCESS_ENGINE, e);
		}
	}

	
	public void closeProcess(Long processPk) {
		this.closeProcess(processPk, DBAccess.getEntityManager());
	}
	
	/**
	 * closes a process
	 * 
	 * @param processPk
	 *            primary key of the process to close
	 */
	public void closeProcess(Long processPk, EntityManager em) {
		ProcessNode process = ProcessTreeAccess.getInstance().getProcess(processPk, em);
		this.closeProcess(process, em);
	}
	
	
	public void closeProcess(Long processId, String engineUrl) {
		this.closeProcess(processId, engineUrl, DBAccess.getEntityManager());
	}
	
	/**
	 * closes a process
	 * 
	 */
	public void closeProcess(Long processId, String engineUrl, EntityManager em) {
		// TODO process has to be fetched from jbpm!
		ProcessNode process = ProcessNodeDAO.getInstance().getProcessNodeByExternalProcessId(processId, engineUrl, em);
		this.closeProcess(process, em);
	}
		
	
	private void closeProcess(ProcessNode process, EntityManager em) {
		try {
			ProcessEngine engine = ProcessEngineFactory.getProcessEngine(process.getEngineUrl());
			engine.terminateProcess(process.getDetail().getProcessId());

			// log
			ProcessEventDAO.getInstance().log(ProcessEvent.TYPE_PROCESS_CLOSED, process.getProcessId(),
					"Prozess \"" + process.getProcessId() + "\" wurde beendet.", null, em);
		} catch (ProcessEngineException e) {
			log.severe(ServiceConstants.ERROR_IN_PROCESS_ENGINE);
			log.log(Level.SEVERE, ServiceConstants.ERROR_IN_PROCESS_ENGINE, e);
		}
	}
	
	
	public void markProcessAsDeleted(Long processId, String engineUrl) {
		this.markProcessAsDeleted(processId, engineUrl, DBAccess.getEntityManager());
	}
	
	/**
	 * marks a process as deleted
	 * 
	 */
	public void markProcessAsDeleted(Long processId, String engineUrl, EntityManager em) {
		ProcessNode process = ProcessNodeDAO.getInstance().getProcessNodeByExternalProcessId(processId, engineUrl, em);
		process.setDeleteDate(new Date());
	}

	
	/**
	 * returns the number of processes of the given processTypeId
	 * 
	 * @param processTypeId
	 * @return
	 */
	public long getProcessCount(String processTypeId) {
		WorkerProcessFilter filter = new WorkerProcessFilter();
		if (processTypeId != null)
			filter.setProcessName(new QName(processTypeId));
		filter.setProcessState(ProcessStates.PROCESS_REASON_NONE);
		return Aggregator.getInstance().getProcessList(filter).size();
	}
	
	
	public long getProcessCount(WorkerProcessFilter filter) {
		return this.getProcessCount(filter, DBAccess.getEntityManager());
	}
	
	/**
	 * returns the number of processes defined by the given filter
	 * 
	 * @param processTypeId
	 * @return
	 */
	public long getProcessCount(WorkerProcessFilter filter, EntityManager em) {
		//TODO <ckuest> Why is this line here? I commented it out because
		//I'm getting wrong infos about the procces count for a given filter!
		//filter.setProcessState(ProcessStates.PROCESS_REASON_NONE);
		
		// If we filter for deleted processes the filtering has to be done here because the processes
		// are marked as deleted in the taskmanagement.
		/*if (filter.getProcessState() != ProcessStates.PROCESS_DELETED) {
			return Aggregator.getInstance().getProcessList(filter).size();
			
		} else {*/

			Aggregator aggregator = Aggregator.getInstance();
			ProcessList list;
			try {
				list = aggregator.getProcessList(filter);
			} catch (RuntimeException e) {
				log.severe(ServiceConstants.ERROR_LOAD_DATA);
				log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
				return 0;
			}

			List<ProcessNode> pnodes = ProcessTreeAccess.getInstance().addAllProcesses(list, em);
			
			// filter deleted processes if request
			/*if (filter.getProcessState() == ProcessStates.PROCESS_DELETED) {
				for (int i = pnodes.size() - 1; i >= 0; i--) {
					if (pnodes.get(i).getDeleteDate() == null)
						pnodes.remove(i);
				}
			} else {
				for (int i = pnodes.size() - 1; i >= 0; i--) {
					if (pnodes.get(i).getDeleteDate() != null)
						pnodes.remove(i);
				}
			}*/
			
			return pnodes.size();	
		//}
	}

	
	public List<ProcessNode> getProcessList(WorkerProcessFilter filter) {
		return this.getProcessList(filter, DBAccess.getEntityManager());
	}
	
	/**
	 * returns a list of processes. The content of the list is defined by the
	 * given filter.
	 * 
	 */
	public List<ProcessNode> getProcessList(WorkerProcessFilter filter, EntityManager em) {
		
		// Workaround Problem that we cant filter for process deleted. 
		// So we ask for *all* in the list and removing non deleted processes
		// from the results.
		if (filter.getProcessState() == ProcessStates.PROCESS_DELETED)
		{
			filter.setMaxReturn(0);
		}
		Aggregator aggregator = Aggregator.getInstance();
		ProcessList list;
		try {
			list = aggregator.getProcessList(filter);
		} catch (RuntimeException e) {
			log.severe(ServiceConstants.ERROR_LOAD_DATA);
			log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
			return new LinkedList<ProcessNode>();
		}

		List<ProcessNode> pnodes = ProcessTreeAccess.getInstance().addAllProcesses(list, em);
		
		// filter deleted processes if request
		// We can not filter deleted processes here because the filter options start and count would not work anymore.
		/*if (filter.getProcessState() == ProcessStates.PROCESS_DELETED) {
			for (int i = pnodes.size() - 1; i >= 0; i--) {
				if (pnodes.get(i).getDeleteDate() == null)
					pnodes.remove(i);
			}
		} else {
			for (int i = pnodes.size() - 1; i >= 0; i--) {
				if (pnodes.get(i).getDeleteDate() != null)
					pnodes.remove(i);
			}
		}*/
		
		return pnodes;

		// retrieve data for process instances
		/*
		 * try { List<ProcessNode> pnodes =
		 * ProcessTreeAccess.getInstance().addAllProcesses(list);
		 *  // load responsible staffer Map<Long, String> responsibleStaffer =
		 * new HashMap<Long, String>(); if (filter.isResponsible()) { //
		 * responsible staffer is the same // as actual staffer => no need to //
		 * ask engines for (ProcessNode pnode : pnodes)
		 * responsibleStaffer.put(pnode.getPk(), stafferId); } else try { for
		 * (ProcessNode pnode : pnodes) responsibleStaffer.put(pnode.getPk(),
		 * getResponsibleStaffer(pnode)); } catch (MalformedURLException e) {
		 * log.severe(ServiceConstants.ERROR_LOAD_DATA); log.log(Level.SEVERE,
		 * ServiceConstants.ERROR_LOAD_DATA, e); } catch (ProcessEngineException
		 * e) { log.severe(ServiceConstants.ERROR_LOAD_DATA);
		 * log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e); } return
		 * pnodes; } catch (SQLException e) {
		 * log.severe(ServiceConstants.ERROR_LOAD_DATA); log.log(Level.SEVERE,
		 * ServiceConstants.ERROR_LOAD_DATA, e); return new LinkedList<ProcessNode>(); }
		 */
	}

	
	public List<ProcessEvent> getLogForProcess(Long processId) {
		return this.getLogForProcess(processId, DBAccess.getEntityManager());
	}
	
	/**
	 * returns a list of all logged events for a process identified by its id
	 * 
	 * @param processId
	 * @return
	 */
	public List<ProcessEvent> getLogForProcess(Long processId, EntityManager em) {
		return ProcessEventDAO.getInstance().getAllForProcess(processId, em);
	}

	// ##############COMMENT RELATED METHODS################

	public List<Comment> getCommentListByProcess(long processPk) {
		return this.getCommentListByProcess(processPk, DBAccess.getEntityManager());
	}
	
	/**
	 * Returns all comments of the process subtree identified by the supplied
	 * process node.
	 */
	public List<Comment> getCommentListByProcess(long processPk, EntityManager em) {
		ProcessNode process = ProcessNodeDAO.getInstance().getProcessNodeByPk(processPk, em);
		if (process == null)
			return null;
		else
			return process.getCommentList();
	}

	public List<Comment> getCommentListByProcessWholeProcess(long processPk) {
		return this.getCommentListByProcessWholeProcess(processPk, DBAccess.getEntityManager());
	}
	
	/**
	 * Returns all comments of the process subtree identified by the supplied
	 * process node.
	 */
	public List<Comment> getCommentListByProcessWholeProcess(long processPk, EntityManager em) {
		ProcessNode process = ProcessNodeDAO.getInstance().getProcessNodeByPk(processPk, em);
		if (process == null)
			return null;

		return CommentDAO.getInstance().getAll(new CommentFilter().setRootProcessPk(process.getFkRootProcessNode()), em);
	}

	public List<Comment> getCommentListByTaskWholeProcess(long taskPk) {
		return this.getCommentListByTaskWholeProcess(taskPk, DBAccess.getEntityManager());
	}
	
	/**
	 * Returns all comments of the process subtree identified by the supplied
	 * Task.
	 */
	public List<Comment> getCommentListByTaskWholeProcess(long taskPk, EntityManager em) {
		Task task = TaskDAO.getInstance().getTaskByPk(taskPk, false, em);
		if (task == null)
			return null;

		List<Comment> comments = CommentDAO.getInstance().getAll(new CommentFilter().setRootProcessPk(task.getRootId()), em);
		task.setCommentList(comments);
		return comments;

	}

	
	public void addCommentToProcess(long processPk, String commentString, String stafferId, String commentTypeName) {
		this.addCommentToProcess(processPk, commentString, stafferId, commentTypeName, DBAccess.getEntityManager());
	}
	
	/**
	 * adds a comment to a process
	 * 
	 * @param processPk
	 *            id of the process to attach the comment to
	 * @param comment
	 *            the comment itself
	 * @param stafferId
	 *            if of the staffer who gave this comment
	 */
	public void addCommentToProcess(long processPk, String commentString, String stafferId, String commentTypeName, EntityManager em) {
		boolean transactionStarted = true;
		try {
			em.getTransaction().begin();
		} catch (IllegalStateException e) {
			// If this library is used in an enterprise context, we may not be
			// allowed to access the transaction. If so, we believe that the enterprise
			// transaction will rollback if something happens and don't care about
			// transaction handling.
			transactionStarted = false;
		}
		
		// get ProcessNode
		ProcessNode process = ProcessNodeDAO.getInstance().getProcessNodeByPk(processPk, em);

		// get all commentTypes
		Map<String, CommentType> commentTypeMap = new CommentApi().getCommentTypeMap();
		CommentType commentType = commentTypeMap.get(commentTypeName);
		if (commentType == null) {
			log.info("no comment type found with name " + commentTypeName);
		}

		// create Comment
		Comment comment = new Comment();
		comment.setComment(commentString);
		comment.setStafferId(stafferId);
		comment.setCommentType(commentType);
		comment.setFkParentProcessNode(process.getProcessId());
		comment.setFkRootProcessNode(process.getFkRootProcessNode());

		// add Comment to task
		em.persist(comment);
		process.addComment(comment);
		
		if (transactionStarted)
			em.getTransaction().commit();
	}

	// ######### ATTACHMENT RELATED METHODS ############

	public List<Attachment> getAttachmentListByProcess(long processPk) {
		return this.getAttachmentListByProcess(processPk, DBAccess.getEntityManager());
	}
	
	/**
	 * Returns all stored attachments of the process subtree identified by the
	 * supplied process node.
	 */
	public List<Attachment> getAttachmentListByProcess(long processPk, EntityManager em) {
		ProcessNode process = ProcessNodeDAO.getInstance().getProcessNodeByPk(processPk, em);
		if (process == null)
			return null;

		List<Long> processPKs = ProcessNodeDAO.getInstance().getProcessWholeTreePKs(process, em);
		return AttachmentDAO.getInstance().getAll(new AttachmentFilter().setParenProcessPkList(processPKs), em);
	}

	public void addAttachmentToProcess(long processPk, String title, String attachmentMimeType, byte[] document, int priority, String stafferId) {
		this.addAttachmentToProcess(processPk, title, attachmentMimeType, document, priority, stafferId, DBAccess.getEntityManager());
	}
	
	public void addAttachmentToProcess(long processPk, String title, String attachmentMimeType, byte[] document, int priority, String stafferId, EntityManager em) {

		ProcessNode process = ProcessNodeDAO.getInstance().getProcessNodeByPk(processPk, em);

		// select attachment type
		AttachmentType type = AttachmentTypeDAO.getInstance().getOrCreateType(attachmentMimeType, em);

		try {
			// store file
			String pdmsId = new AttachmentApi().storeFile(process, priority, document);

			// create attachment object
			Attachment attachment = new Attachment();
			attachment.setPdmsId(pdmsId);
			attachment.setStafferId(stafferId);
			//attachment.setFkAttachmentType(type.getPk());
			attachment.setAttachmentType(type);
			// attachment.setFkTask(task.getPk());
			attachment.setTitle(title);
			attachment.setFkParentProcessNode(process.getFkParentProcessNode());
			attachment.setFkRootProcessNode(process.getFkRootProcessNode());

			boolean commit = false;
			try {
				if (!em.getTransaction().isActive()) {
					em.getTransaction().begin();
					commit = true;
				}
			} catch (IllegalStateException e) {
				// If this library is used in an enterprise context, we may not be
				// allowed to access the transaction. If so, we believe that the enterprise
				// transaction will rollback if something happens and don't care about
				// transaction handling.
				commit = false;
			}
			em.persist(attachment);
			// add Attachment to task
			process.addAttachment(attachment);
			if (commit)
				em.getTransaction().commit();
		} catch (IOException e) {
			log.severe("attachment could not be saved: " + e.getLocalizedMessage());
			throw new RuntimeException(e);
		}

	}
	
	
	/** inserts a new process node into the database
	 *  
	 * @param processId
	 * @param engineUrl
	 */
	public void insertProcessNode(long processId, String engineUrl) {
		this.insertProcessNode(processId, engineUrl, DBAccess.getEntityManager());
	}
	
	
	public void insertProcessNode(long processId, String engineUrl, EntityManager manager) {
		ProcessNodeDAO.getInstance().insertProcessNode(processId, engineUrl, null, manager);
	}

	// /**
	// * returns a process object belonging to the given primary key
	// *
	// * @param processPk
	// * primary key of the process to return
	// * @return
	// */
	// public ProcessNode getProcess(Long processPk) {
	// ProcessNode processNode = null;
	// try {
	// processNode = ProcessTreeAccess.getInstance().getProcess(processPk);
	//
	// ProcessNode parentProcessNode = null;
	// if (processNode != null && processNode.getFkParentProcessNode() != null)
	// {
	// parentProcessNode =
	// ProcessTreeAccess.getInstance().getProcess(processNode.getFkParentProcessNode());
	// // TODO: also return parentProcessNode
	// // oc.setContent("parentProcess", parentProcessNode);
	// }
	// // TODO: Beschreibung bzw den Namen von Prozess mit pk node.getPk()
	// // ermitteln und zurückgeben
	//
	// } catch (SQLException e) {
	// log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// } catch (MalformedURLException e) {
	// log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// } catch (ProcessEngineException e) {
	// log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// }
	// return processNode;
	// }

	// /** returns the root process of a given process
	// *
	// * @param process the process for which the root process shall be returned
	// * @return the root process of the given process
	// */
	// public ProcessNode getRootProcessByProcess(ProcessNode process) {
	// ProcessNode node = null;
	// try {
	// node =
	// ProcessTreeAccess.getInstance().getProcess(process.getFkRootProcessNode());
	// // TODO: Beschreibung bzw den Namen von Prozess mit pk node.getPk()
	// // ermitteln und zurückgeben
	// } catch (SQLException e) {
	// log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// } catch (NumberFormatException e) {
	// log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// } catch (MalformedURLException e) {
	// log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// } catch (ProcessEngineException e) {
	// log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// }
	//
	// return node;
	// }

	// /** returns the process the given task is associated to
	// *
	// * @param task the task to return the process for
	// * @return
	// */
	// public ProcessNode getParentProcessByTask(Task task) {
	// if (task == null)
	// return null;
	// // try {
	// // ProcessNode taskParent =
	// ProcessNodeDAO.getInstance().getProcessNodeByTask(task);
	// // return ProcessTreeAccess.getInstance().getProcess(taskParent.getPk());
	// // } catch (SQLException e) {
	// // log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// // log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// // } catch (NumberFormatException e) {
	// // log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// // log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// // } catch (MalformedURLException e) {
	// // log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// // log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// // } catch (ProcessEngineException e) {
	// // log.severe(ServiceConstants.ERROR_LOAD_DATA);
	// // log.log(Level.SEVERE, ServiceConstants.ERROR_LOAD_DATA, e);
	// // }
	// // return null;
	// return task.getParent();
	// }
}
