/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 *
 * IAeActiveBpelAdmin.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package de.tarent.wfms.processmiddleware.engine.wsdl;

import java.util.List;

public interface IAeActiveBpelAdmin extends java.rmi.Remote {
    public de.tarent.wfms.processmiddleware.engine.xsd.AesConfigurationType getConfiguration(de.tarent.wfms.processmiddleware.engine.xsd.AesVoidType getConfigurationInput) throws java.rmi.RemoteException, de.tarent.wfms.processmiddleware.engine.xsd.AdminFault;
    public void setConfiguration(de.tarent.wfms.processmiddleware.engine.xsd.AesConfigurationType setConfigurationInput) throws java.rmi.RemoteException, de.tarent.wfms.processmiddleware.engine.xsd.AdminFault;
    public void suspendProcess(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessType suspendProcessInput) throws java.rmi.RemoteException, de.tarent.wfms.processmiddleware.engine.xsd.AdminFault;
    public void resumeProcess(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessType resumeProcessInput) throws java.rmi.RemoteException, de.tarent.wfms.processmiddleware.engine.xsd.AdminFault;
    public void resumeProcessObject(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessObjectType resumeProcessObjectInput) throws java.rmi.RemoteException, de.tarent.wfms.processmiddleware.engine.xsd.AdminFault;
    public void terminateProcess(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessType terminateProcessInput) throws java.rmi.RemoteException, de.tarent.wfms.processmiddleware.engine.xsd.AdminFault;
    public void addEngineListener(de.tarent.wfms.processmiddleware.engine.xsd.AesEngineRequestType addEngineListenerInput) throws java.rmi.RemoteException;
    public void addBreakpointListener(de.tarent.wfms.processmiddleware.engine.xsd.AesBreakpointRequestType addBreakpointListenerInput) throws java.rmi.RemoteException;
    public void updateBreakpointList(de.tarent.wfms.processmiddleware.engine.xsd.AesBreakpointRequestType updateBreakpointListenerInput) throws java.rmi.RemoteException;
    public void removeEngineListener(de.tarent.wfms.processmiddleware.engine.xsd.AesEngineRequestType removeEngineListenerInput) throws java.rmi.RemoteException;
    public void removeBreakpointListener(de.tarent.wfms.processmiddleware.engine.xsd.AesRemoveBreakpointRequestType removeBreakpointListenerInput) throws java.rmi.RemoteException;
    public void addProcessListener(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessRequestType addProcessListenerInput) throws java.rmi.RemoteException;
    public void removeProcessListener(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessRequestType removeProcessListenerInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesStringResponseType getVariable(de.tarent.wfms.processmiddleware.engine.xsd.AesGetVariableDataType getVariableDataInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesStringResponseType setVariable(de.tarent.wfms.processmiddleware.engine.xsd.AesSetVariableDataType setVariableDataInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesProcessListType getProcessList(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessFilterType getProcessListInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesProcessDetailType getProcessDetail(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessType getProcessDetailInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesStringResponseType getProcessState(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessType getProcessStateInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesDigestType getProcessDigest(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessType getProcessDigestInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesStringResponseType getProcessDef(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessType getProcessDefInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesStringResponseType getProcessLog(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessType getProcessLogInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesStringResponseType getAPIVersion(de.tarent.wfms.processmiddleware.engine.xsd.AesVoidType getAPIVersionInput) throws java.rmi.RemoteException;
    public de.tarent.wfms.processmiddleware.engine.xsd.AesStringResponseType deployBpr(de.tarent.wfms.processmiddleware.engine.xsd.AesDeployBprType deployBprInput) throws java.rmi.RemoteException;
    public void setPartnerLinkData(de.tarent.wfms.processmiddleware.engine.xsd.AesSetPartnerLinkType setPartnerLinkDataInput) throws java.rmi.RemoteException;
    public void setCorrelationSetData(de.tarent.wfms.processmiddleware.engine.xsd.AesSetCorrelationType setCorrelationDataInput) throws java.rmi.RemoteException;
    public void retryActivity(de.tarent.wfms.processmiddleware.engine.xsd.AesRetryActivityType retryActivityInput) throws java.rmi.RemoteException;
    public void completeActivity(de.tarent.wfms.processmiddleware.engine.xsd.AesCompleteActivityType completeActivityInput) throws java.rmi.RemoteException;
    public int getProcessCount(de.tarent.wfms.processmiddleware.engine.xsd.AesProcessFilterType input) throws java.rmi.RemoteException;
    public List<Long> getParentList(long aProcessId) throws java.rmi.RemoteException;
}
