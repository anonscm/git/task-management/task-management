/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on Jul 18, 2007
 */

package de.tarent.wfms.processmiddleware.task.type;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.tarent.wfms.processmiddleware.data.Mask;
import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.data.TaskType;
import de.tarent.wfms.processmiddleware.task.MaskConnector;

/**
 * Default implementation of a TaskTypeDescriptor, reading the associated
 * MaskConnector classes from database. The database has a relation containing
 * one or more associations taskType -> MaskConnector class name. For each 
 * association, the corresponding class is instantiated and added to the 
 * returned list.
 * <p>
 * Please note that customer specific MaskConnector implementations <b>should not</b>
 * be added in this project. Use an external library instead!
 * 
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class DatabaseTaskTypeDescriptor extends TaskTypeDescriptor
{    
    @Override
    public List<MaskConnector> getMaskConnectors(TaskType taskType, Task task, Map<String,Object> taskData)
    {
        List<MaskConnector> out = new ArrayList<MaskConnector>();
        
        List<Mask> taskTypeMaskList = taskType.getMasks();
        if (taskTypeMaskList!=null)
            for (int i=0; i<taskTypeMaskList.size(); i++)
                out.add(createMaskConnector(taskTypeMaskList.get(i).getClassname(), taskType, task, taskData));
        
        return out;
    }
}
