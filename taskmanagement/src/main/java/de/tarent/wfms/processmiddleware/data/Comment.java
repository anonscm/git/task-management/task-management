/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * The Class Comment.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 * @author Martin Pelzer, tarent GmbH
 */
@NamedQueries({
	@NamedQuery(name = Comment.STMT_SELECT_ONE_BY_ID, query = "select x from Comment as x where pk = :id"),
	@NamedQuery(name = Comment.STMT_SELECT_ALL, query = "select x from Comment as x")
})
@Entity
@Table(name = "comment")
public class Comment {
	
	public static final String STMT_SELECT_ONE_BY_ID = "stmtSelectCommentById";
	public static final String STMT_SELECT_ALL = "stmtSelectAllComments";
	

	private long pk;
	private Date creationDate;
	private Date updateDate;
	
	/** The fk parent process node. */
	private Long fkParentProcessNode;
	
	/** The fk root process node. */
	private Long fkRootProcessNode;
	
	/** The fk task. */
	private Long fkTask;
	
	/** The staffer id. */
	private String stafferId; // not null
	
	/** The comment. */
	private String comment; // not null
	
	/** The fk comment type. */
	//private Long fkCommentType;
	
	/** The comment type. */
	private CommentType commentType;
	
	public Comment(){
		
	}
	
	
	/**
	 * Gets the comment.
	 * 
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	
	/**
	 * Sets the comment.
	 * 
	 * @param comment the new comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	/**
	 * Gets the fk task.
	 * 
	 * @return the fk task
	 */
	@Column(name = "fk_task")
	public Long getFkTask() {
		return fkTask;
	}
	
	/**
	 * Sets the fk task.
	 * 
	 * @param fkTask the new fk task
	 */
	public void setFkTask(Long fkTask) {
		this.fkTask = fkTask;
	}
	
	/**
	 * Gets the staffer id.
	 * 
	 * @return the staffer id
	 */
	@Column(name = "staffer_id")
	public String getStafferId() {
		return stafferId;
	}
	
	/**
	 * Sets the staffer id.
	 * 
	 * @param stafferId the new staffer id
	 */
	public void setStafferId(String stafferId) {
		this.stafferId = stafferId;
	}

	/**
	 * Gets the fk parent process node.
	 * 
	 * @return the fk parent process node
	 */
	@Column(name = "fk_parent_process_node")
	public Long getFkParentProcessNode() {
		return fkParentProcessNode;
	}

	/**
	 * Sets the fk parent process node.
	 * 
	 * @param fkParentProcessNode the new fk parent process node
	 */
	public void setFkParentProcessNode(Long fkParentProcessNode) {
		this.fkParentProcessNode = fkParentProcessNode;
	}

	/**
	 * Gets the fk root process node.
	 * 
	 * @return the fk root process node
	 */
	@Column(name = "fk_root_process_node")
	public Long getFkRootProcessNode() {
		return fkRootProcessNode;
	}

	/**
	 * Sets the fk root process node.
	 * 
	 * @param fkRootProcessNode the new fk root process node
	 */
	public void setFkRootProcessNode(Long fkRootProcessNode) {
		this.fkRootProcessNode = fkRootProcessNode;
	}

	/**
	 * Gets the comment type.
	 * 
	 * @return the comment type
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_comment_type")
	public CommentType getCommentType() {
		return commentType;
	}
	
	/**
	 * Gets the name of the commenttype.
	 * 
	 * @return the comment type name
	 */
	@Transient
	public String getCommentTypeName(){
		return this.getCommentType().getName();
	}

	/**
	 * Gets the shortcut of the commenttype .
	 * 
	 * @return the commenttype shortcut
	 */
	@Transient
	public String getCommentTypeShortcut(){
		return this.getCommentType().getShortcut();
	}

	public void setCommentType(CommentType commentType) {
		this.commentType = commentType;
	}


	@Id
	@GeneratedValue
	@Column(name = "pk_comment", nullable = false)
	public long getPk() {
		return pk;
	}
	
	/**
	 * Sets the primary key of this object.
	 * 
	 * @param   pk
	 *          The primary key
	 */
	public void setPk(long pk) {
		this.pk = pk;
	}
	
	/**
	 * Returns the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  creationDate
	 *          The creation time
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   creationDate
	 *          The creation time
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Returns the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  updateDate
	 *          The update time
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	
	/**
	 * Sets the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   updateDate
	 *          The update time
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
