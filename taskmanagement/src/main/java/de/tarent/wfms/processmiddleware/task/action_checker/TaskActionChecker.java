package de.tarent.wfms.processmiddleware.task.action_checker;


/**
 * The Interface TaskActionChecker. Defines methods to check if a task is 
 * forwardable, reopenable and closeable as well as methods to get the 
 * corresponding message.  
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 */
public interface TaskActionChecker {
	

	/**
	 * Checks if task is forwardable.
	 * 
	 * @return true, if is forwardable
	 */
	public boolean isForwardable();
	
	/**
	 * Checks if task is reopenable.
	 * 
	 * @return true, if is reopenable
	 */
	public boolean isReopenable();
	
	/**
	 * Checks if task is closeable.
	 * 
	 * @return true, if is closeable
	 */
	public boolean isCloseable();
	
	/**
	 * Checks if task is resubmitable.
	 * 
	 * @return true, if is resubmitable
	 */
	public boolean isResubmitable();
	
	/**
	 * Gets the forward message.
	 * 
	 * @return the forward message
	 */
	public String getForwardMessage();
	
	/**
	 * Gets the reopen message.
	 * 
	 * @return the reopen message
	 */
	public String getReopenMessage();
	
	/**
	 * Gets the close message.
	 * 
	 * @return the close message
	 */
	public String getCloseMessage();
	
	/**
	 * Gets the resubmit message.
	 * 
	 * @return the resubmit message
	 */
	public String getResubmitMessage();
}
