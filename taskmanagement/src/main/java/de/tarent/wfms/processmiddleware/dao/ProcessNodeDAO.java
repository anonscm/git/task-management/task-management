/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;

import de.tarent.wfms.processmiddleware.aggregator.ProcessEngine;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngineException;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngineFactory;
import de.tarent.wfms.processmiddleware.data.ProcessNode;
import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.db.DBAccess;
import java.util.LinkedList;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class ProcessNodeDAO {

	private static ProcessNodeDAO instance = null;

	public static synchronized ProcessNodeDAO getInstance() {
		if (instance == null)
			instance = new ProcessNodeDAO();
		return instance;
	}

	public ProcessNode getProcessNodeByPk(long pk, EntityManager em) {
		return (ProcessNode) em.createNamedQuery(ProcessNode.STMT_SELECT_ONE_BY_ID).setParameter("id", pk).getSingleResult();
	}

	/**
	 * returns the parent ProcessNode for the supplied task
	 */
	public ProcessNode getProcessNodeByTask(Task task, EntityManager em) {
		return getProcessNodeByPk(task.getParent().getProcessId(), em);
	}
	
	public ProcessNode getProcessNodeByExternalProcessId(Long processId, String engineURL, EntityManager em) {
		//EntityManager em = DBAccess.getEntityManager();
		
		Query query = em.createNamedQuery(ProcessNode.STMT_SELECT_ONE_BY_EXTERNAL_ID).setParameter("processId", processId)
				.setParameter("engineUrl", engineURL);

		boolean commit = false;
		try {
			if (!em.getTransaction().isActive()) {
				em.getTransaction().begin();
				commit = true;
			}
		} catch (IllegalStateException e) {
			// If this library is used in an enterprise context, we may not be
			// allowed to access the transaction. If so, we believe that the enterprise
			// transaction will rollback if something happens and don't care about
			// transaction handling.
			commit = false;
		}
		
		ProcessNode pn = null;
		
		try {
			pn = (ProcessNode) query.getSingleResult();
		} catch (NoResultException e) {
			// not in database -> return null
			return null;
		}
		
		if (commit)
			em.getTransaction().commit();
		
		return pn;
	}

	public ProcessNode getProcessNodeByExternalProcessIdWithActualTask(Long processId, String engineURL, EntityManager em) {
		return (ProcessNode) em.createNamedQuery(ProcessNode.STMT_SELECT_ONE_BY_EXTERNAL_ID_WITH_ACTUAL_TASK).setParameter(
				"processId", processId).setParameter("engineUrl", engineURL).getSingleResult();
	}

	public ProcessNode getParentProcessNodeByTaskPk(long taskPk, EntityManager em) {
		return (ProcessNode) em.createNamedQuery(ProcessNode.STMT_SELECT_PARENT_PROCESS_BY_TASK).setParameter("taskPk", taskPk)
				.getSingleResult();
	}

	/**
	 * Returns the list of primary keys of all process nodes in the subtree
	 * identified by the supplied <code>subtreeRootNode<code>.
	 * The subtreeRootNode itself is included at position 0.
	 */
	@SuppressWarnings( { "deprecation", "unchecked" })
	public List<Long> getProcessSubtreePKs(ProcessNode subtreeRootNode, EntityManager em) {
		final List<Long> pklist = new ArrayList<Long>();

		final LinkedList<Long> stack = new LinkedList<Long>();
		stack.add(subtreeRootNode.getProcessId());

		while (stack.size() > 0) {
			Long currentParent = stack.removeLast();
			if (pklist.contains(currentParent))
				continue; // don't fall into loops!
			pklist.add(currentParent);
			Query query = em.createNamedQuery(ProcessNode.STMT_SELECT_BY_FK_PARENT_PROCESS_NODE);
			query.setParameter("fk", currentParent);
			List<Long> results = query.getResultList();
			Iterator<Long> iter = results.iterator();
			while (iter.hasNext()) {
				stack.add(iter.next());
			}
		}
		return pklist;
	}

	/**
	 * Returns the list of primary keys of all process nodes in the subtree
	 * identified by the supplied <code>subtreeRootNode<code>
	 *  and it's parent-nodes.
	 */
	@SuppressWarnings( { "deprecation", "unchecked" })
	public List<Long> getProcessTreePKs(ProcessNode subtreeRootNode, EntityManager em) {
		final List<Long> pklist = new ArrayList<Long>();

		final LinkedList<Long> stack = new LinkedList<Long>();
		stack.add(subtreeRootNode.getProcessId());

		ProcessNode parent = subtreeRootNode.getParentProcessNode();
		while (parent != null) {
			pklist.add(parent.getProcessId());
			parent = parent.getParentProcessNode();
		}

		while (stack.size() > 0) {
			Long currentParent = stack.removeLast();
			if (pklist.contains(currentParent))
				continue; // don't fall into loops!
			pklist.add(currentParent);

			Query query = em.createNamedQuery(ProcessNode.STMT_SELECT_BY_FK_PARENT_PROCESS_NODE);
			query.setParameter("fk", currentParent);
			List<Long> results = query.getResultList();
			Iterator<Long> iter = results.iterator();
			while (iter.hasNext()) {
				stack.add(iter.next());
			}
		}

		return pklist;
	}

	/**
	 * Returns the list of primary keys of all process nodes of the ProcessTree
	 */
	@SuppressWarnings( { "deprecation", "unchecked" })
	public List<Long> getProcessWholeTreePKs(ProcessNode subtreeRootNode, EntityManager em) {
		final List<Long> pklist = new ArrayList<Long>();

		final LinkedList<Long> stack = new LinkedList<Long>();

		stack.add(subtreeRootNode.getFkRootProcessNode());

		while (stack.size() > 0) {
			Long currentParent = stack.removeLast();
			if (pklist.contains(currentParent))
				continue; // don't fall into loops!
			pklist.add(currentParent);
			Query query = em.createNamedQuery(ProcessNode.STMT_SELECT_BY_FK_PARENT_PROCESS_NODE);
			query.setParameter("fk", currentParent);
			List<Long> results = query.getResultList();
			Iterator<Long> iter = results.iterator();
			while (iter.hasNext()) {
				stack.add(iter.next());
			}
		}
		return pklist;
	}
	
	
	/**
	 * counts the number of processes of the given type
	 * 
	 * @param processTypeId
	 * @return
	 */
	// TODO that has to be queried from the process engine because
	// the process type id is not stored in the process middleware
	/*
	 * public long countProcesses(long processTypeId) { Query query =
	 * DBAccess.getEntityManager().createNamedQuery(ProcessNode.STMT_COUNT);
	 * query.setParameter("processTypeId", processTypeId); return (Long)
	 * query.getSingleResult(); }
	 */

	// service methods
	/**
	 * Stores a new process into the process database, creating all necessary
	 * root and parent processes recursively. The nesseccary information will be
	 * retrieved from the engine behind the supplied engine url.
	 * 
	 * For the root node, we insert the node is as root node id, too so
	 * isRootNode(node) is equivalent to (node.pk == node.fk_root_process_node)
	 * 
	 * @param mdbc
	 *            Database context.
	 * @param processId
	 *            External id the the new processs node withing the engine (not
	 *            general unique).
	 * @param engineUrl
	 *            engine url of the new process node.
	 * @param ancestors
	 *            ancestor list, direct parent first. If <code>null</code>,
	 *            method will get ancestor list from engine. Non-null for
	 *            recursive access.
	 */
	public ProcessNode insertProcessNode(Long processId, String engineUrl, List<Long> ancestors, EntityManager em) {
		// retrieve acestor process node ids if given ancestors is null
		if (ancestors == null)
			try {
				ProcessEngine engine = ProcessEngineFactory.getProcessEngine(engineUrl);
				ancestors = fixToBeLongList(engine.getParentList(processId));
			} catch (ProcessEngineException e) {
				throw new RuntimeException("error retrieving ancestor processes for process id " + processId + " of engine at " + engineUrl, e);
			}

		// create root node if not already present
		// rootPId == external process id of the root process in the engine
		// scope
		Long rootPId = ancestors.isEmpty() ? processId : ancestors.get(ancestors.size() - 1);
		ProcessNode rootNode = null;

		//EntityManager em = DBAccess.getEntityManager();
		
		synchronized(this)
		{
			rootNode = em.find(ProcessNode.class, rootPId);
			if (rootNode == null) {
				rootNode = new ProcessNode();
				rootNode.setProcessId(rootPId);
				rootNode.setEngineUrl(engineUrl);
				rootNode.setFkRootProcessNode(null);
				rootNode.setFkParentProcessNode(null);

				System.out.println("inserting new process node " + rootNode.getProcessId());
				boolean commit = false;
				try {
					if (!em.getTransaction().isActive()) {
						em.getTransaction().begin();
						commit = true;
					}
				} catch (IllegalStateException e) {
					// If this library is used in an enterprise context, we may not be
					// allowed to access the transaction. If so, we believe that the enterprise
					// transaction will rollback if something happens and don't care about
					// transaction handling.
					commit = false;
				}

				// for the root node, we insert the node is as root node id, too
				// so isRootNode(node) is equivalent to (node.pk ==
				// node.fk_root_process_node)
				rootNode.setFkRootProcessNode(rootNode.getProcessId());

				em.merge(rootNode);
				//em.flush();

				if (commit)
					em.getTransaction().commit();
			}
		} // synchronized
		ProcessNode parentNode = null;
		if (ancestors.size() > 1) {
			// create parent nodes to complete process chain in database - all
			// hands warning, recursive access!
			Long parentPId = (ancestors == null || ancestors.isEmpty()) ? null : ancestors.get(0);
			if (parentPId != null)
				// call method recursivly, removing first entry from ancestors
				parentNode = insertProcessNode(parentPId, engineUrl, ancestors.subList(1, ancestors.size()), em);
		} else {
			// in this case, we have already created the parent node, because it
			// is the same as the root node.
			parentNode = rootNode;
		}

		// create this process node, if not already present
		// 
		ProcessNode node = null;

		node = this.getProcessNodeByExternalProcessId(processId, engineUrl, em);
		if (node == null) { // create process
			node = new ProcessNode();
			node.setProcessId(processId);
			node.setEngineUrl(engineUrl);
			node.setFkRootProcessNode(rootNode.getProcessId());
			node.setFkParentProcessNode(parentNode == null ? null : parentNode.getProcessId());
			
			boolean commit = false;
			try {
				if (!em.getTransaction().isActive()) {
					em.getTransaction().begin();
					commit = true;
				}
			} catch (IllegalStateException e) {
				// If this library is used in an enterprise context, we may not be
				// allowed to access the transaction. If so, we believe that the enterprise
				// transaction will rollback if something happens and don't care about
				// transaction handling.
				commit = false;
			}
			em.merge(node);
			if (commit)
				em.getTransaction().commit();			
		}

		// returning newly created process node
		return node;
	}

	/**
	 * axis seem to have an inconsistence bug with java1.5 varargs. on this
	 * call, it returns an List containing one Object array with the long value,
	 * insted of an List, containing the Long directly. this workaround should
	 * work with both cases.
	 */
	private List<Long> fixToBeLongList(Object unknown) {
		if (unknown == null || (unknown instanceof List && ((List) unknown).isEmpty()))
			return Collections.EMPTY_LIST;

		if (!(unknown instanceof List))
			throw new RuntimeException("axis result is not a list, but has type " + unknown.getClass());

		Object firstElement = ((List) unknown).get(0);
		if (firstElement instanceof Long)
			return (List<Long>) unknown;

		if (firstElement instanceof Object[]) {
			if (((Object[]) firstElement).length == 0)
				return Collections.EMPTY_LIST;
			ArrayList<Long> longList = new ArrayList<Long>();
			Object[] objectList = (Object[]) firstElement;
			for (int i = 0; i < objectList.length; i++) {
				longList.add((Long) objectList[i]);
			}
			return longList;
		}

		throw new RuntimeException("axis result has wrong type first element of ancestors has type " + firstElement.getClass());
	}
}
