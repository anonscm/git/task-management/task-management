/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import java.util.List;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.data.TaskStaffer;

/**
 * @author Hendrik Helwich
 *
 */
public class TaskStafferDAO {

	private static TaskStafferDAO instance = null;

    
    public static synchronized TaskStafferDAO getInstance() {
        if (instance == null)
            instance = new TaskStafferDAO();
        return instance;
    }

    
    @SuppressWarnings("unchecked")
	public List<TaskStaffer> getStafferByTaskPk(long l, EntityManager em) {
    	return (List<TaskStaffer>) em.createNamedQuery(TaskStaffer.STMT_SELECT_BY_TASK_FK).setParameter("taskFk", l).getResultList();
    }
    
}
