package de.tarent.wfms.processmiddleware.aggregator;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.jbpm.JbpmConfiguration;
import org.jbpm.JbpmContext;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.taskmgmt.exe.TaskInstance;

import de.tarent.wfms.processmiddleware.data.Task;

/** process enginge implementation for accessing a JBpm process engine
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class JbpmProcessEngine implements ProcessEngine {

	static JbpmConfiguration jbpmConfiguration = JbpmConfiguration.getInstance();
	
	
	private String identifier;	// to identify the JBPM process engine. Can be anything because we access JBPM locally.
	
	
	
	public JbpmProcessEngine(String identifier) {
		this.identifier = identifier;
	}

	

	/** returns a list of IDs of process instances that are parents and other predecessors
	 * of the current process. The first entry is the parent's id, the second one the parent's
	 * predecessor and so on... 
	 * @throws RemoteException 
	 * 
	 */
	public List<Long> getParentList(long processId) throws ProcessEngineException { 
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		try {
			ProcessInstance instance = jbpmContext.getProcessInstance(processId);

			List<Long> list = new LinkedList<Long>();
			
			// if the given process is not in the process engine db then there are no ancestors
			if (instance == null)
				return list;
			
			while (instance.getSuperProcessToken() != null) {
				list.add(instance.getSuperProcessToken().getProcessInstance().getId());
				instance = instance.getSuperProcessToken().getProcessInstance();
			}
			
			return list;
		} catch (Exception e) {
			// problems accessing the process engine
			throw new ProcessEngineException(e);
		} finally {
			jbpmContext.close();
		}
	}
	
	
	/** returns a list of ProcessDetail objects that contain information
	 * about all process instances in the JBPM instance that match the
	 * given ProcessFilter filter instance 
	 * @throws ProcessEngineException 
	 * 
	 */
	public List<ProcessDetail> getProcessList(ProcessFilter filter) throws ProcessEngineException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		// In case "advancedQuery" is set we build a native sql query because this is the easiest
		// way to join on process variables. The whole query string constructed so far is lost.
		if (filter.getAdvancedQuery() != null) {
			String column = "";
			String columnValue = "";
			String operation = "";
			boolean isDate = false;
			
			// TODO For now we only support one advanced query item.
			
			// iterate over the map and add where clauses to the query
			Iterator<AdvancedQueryItem> items = filter.getAdvancedQuery().iterator();
			while (items.hasNext()) {
				//String key = (String) keys.next();
				AdvancedQueryItem item = items.next();
				operation = item.getOperation();
				
				// add value part
				Object value = item.getValue();
				if (value instanceof String) {
					column = item.getColumnName();
					columnValue = (String) value;
					
				} else if (value instanceof Date) {
					column = item.getColumnName();
					columnValue = format.format((Date) value);
					isDate = true;
				}			
			}
			
			JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
			List<ProcessDetail> details = null;
			try {
				Session session = jbpmContext.getSession();
				
				// TODO This native query does not support all filter possibilities. Only process type, process owner and one advanced query item
				// are supported by now. Should be extended in future realeses.
				
				String sqlQuery = "select distinct on (p.id_) p.id_, p.key_, p.start_, p.end_, def.name_ as defname, startstate.name_ as sname, node.name_ as nodename, p.issuspended_ from jbpm_processinstance as p";
				sqlQuery = sqlQuery += " left outer join jbpm_processdefinition as def on p.processdefinition_ = def.id_";
				sqlQuery = sqlQuery += " left outer join jbpm_node as startstate on def.startstate_ = startstate.id_";
				sqlQuery = sqlQuery += " left outer join jbpm_token as roottoken on p.roottoken_ = roottoken.id_";
				sqlQuery = sqlQuery += " left outer join jbpm_node as node on roottoken.node_ = node.id_";
				sqlQuery = sqlQuery += " left outer join jbpm_variableinstance as variables on variables.processinstance_ = p.id_";

				if ( filter.getProcessName() != null )
				{
					if ( filter.getProcessName().getLocalPart().contains( "%" ) )
					{
						sqlQuery += " where def.name_ LIKE '" + filter.getProcessName().getLocalPart() + "'";
					}
					else
					{
						sqlQuery = sqlQuery + " where def.name_ = '" + filter.getProcessName().getLocalPart() + "'";
					}
				}
				else
				{
					sqlQuery = sqlQuery += " where def.name_ != 'nothing'";
				}
				
				boolean whereAlreadyAdded = true;
				if (filter.getProcessOwner() != null) {
					sqlQuery += getOperator(whereAlreadyAdded) + " p.key_ IN (";
					for (int i = 0; i < filter.getProcessOwner().size(); i++) {
						sqlQuery += "'" + filter.getProcessOwner().get(i) + "'";
						if (i < filter.getProcessOwner().size() - 1)
							sqlQuery += ", ";
					}
					sqlQuery += ") ";
				}
				
				// process state
				switch (filter.getProcessState()) {
					case ProcessStates.PROCESS_COMPLETE:
						sqlQuery += getOperator(whereAlreadyAdded) + " p.end_ is not null";
						if ( filter.definesNotInDefinedEndStateConstraint() )
						{
							sqlQuery += " and node.name_ != '" + filter.getNotInDefinedEndState() + "'";
						}
						whereAlreadyAdded = true;
						break;
					case ProcessStates.PROCESS_RUNNING:
						sqlQuery += getOperator(whereAlreadyAdded) + " node.name_ != startstate.name_ and p.end_ is null null";
						if ( filter.definesNotInDefinedEndStateConstraint() )
						{
							sqlQuery += " and node.name_ != '" + filter.getNotInDefinedEndState() + "'";
						}
						whereAlreadyAdded = true;
						break;
					case ProcessStates.PROCESS_LOADED:
						sqlQuery += getOperator(whereAlreadyAdded) + " node.name_ = startstate.name_ and p.end_ is null";
						whereAlreadyAdded = true;
						break;
					case ProcessStates.PROCESS_SUSPENDED:
						sqlQuery += getOperator(whereAlreadyAdded) + " p.isSuspended_ = 'true'";
						whereAlreadyAdded = true;
						break;
					case ProcessStates.PROCESS_RUNNING_OR_LOADED:
						sqlQuery += getOperator(whereAlreadyAdded) + " p.end_ is null";
						if ( filter.definesNotInDefinedEndStateConstraint() )
						{
							sqlQuery += " and node.name_ != '" + filter.getNotInDefinedEndState() + "'";
						}
						whereAlreadyAdded = true;
						break;
					case ProcessStates.PROCESS_RUNNING_OR_COMPLETED_BUT_NOT_LOADED:
						sqlQuery += getOperator(whereAlreadyAdded) + " node.name_ != startstate.name_";
						if ( filter.definesNotInDefinedEndStateConstraint() )
						{
							sqlQuery += " and node.name_ != '" + filter.getNotInDefinedEndState() + "'";
						}
						whereAlreadyAdded = true;
						break;
					case ProcessStates.PROCESS_DELETED:
						sqlQuery += getOperator(whereAlreadyAdded) + " node.name_ = 'ProcessDeleted'";
						whereAlreadyAdded = true;
						break;
						
					// PROCESS_FAULTED AND PROCESS_COMPENSATABLE have no equivalent in JBPM
				}
				
				if (filter.getProcessState() != ProcessStates.PROCESS_DELETED) {
					sqlQuery += getOperator(whereAlreadyAdded) + " node.name_ != 'ProcessDeleted'";
					whereAlreadyAdded = true;
				}

				sqlQuery = sqlQuery += " and variables.name_ = '" + column + "'";
				
				if (isDate)
					sqlQuery = sqlQuery += " and variables.datevalue_ " + operation + " '" + columnValue + "'";
				else
					sqlQuery = sqlQuery += " and variables.stringvalue_ " + operation + " '" + columnValue + "'";
				
				if (filter.getSortColumn() != null) {
					String sortColumn = filter.getSortColumn();
					if (sortColumn.startsWith("definition")) {
						sortColumn = "def." + sortColumn.substring("definition".length());
		 			} else if (!sortColumn.contains(".")) {
		 				sortColumn = "p." + sortColumn;
		 			}
					sortColumn += "_";
					
					sqlQuery += " order by " + sortColumn;
					sqlQuery += " " + (filter.getSortDir() != null ? filter.getSortDir() : "ASC"); 
				}
				
				SQLQuery query = session.createSQLQuery(sqlQuery);
				
				query.setFirstResult(filter.getListStart());
			
				// set limit
				if (filter.getMaxReturn() != 0)
					query.setMaxResults(filter.getMaxReturn());
				
				// fire query and create ProcessDetails from result
				List list = query.list();
				
				details = new LinkedList<ProcessDetail>();
				for (Object result : list) {
					Object [] array = (Object []) result;
					
					// create ProcessDetail and fill with data from processInstace
					ProcessDetail detail = new ProcessDetail();
					
					BigInteger processId = (BigInteger) array[0];
					detail.setProcessId(processId.longValue());
					
					detail.setResponsibleStafferId((String) array[1]);
					detail.setName(new QName((String) array[4]));
					
					if (array[3] != null) {
						Calendar ended = Calendar.getInstance();
						Timestamp end = (Timestamp) array[3];
						ended.setTime(new Date(end.getTime()));
						detail.setEnded(ended);
					}
					
					if (array[2] != null) {
						Calendar started = Calendar.getInstance();
						Timestamp start = (Timestamp) array[2];
						started.setTime(new Date(start.getTime()));
						detail.setStarted(started);
					}
					
					// state
					if ((Boolean) array[7])	// is suspended column of table processinstance
						detail.setState(ProcessStates.PROCESS_SUSPENDED);
					else if (array[3] != null)	// if end date in process instance != null
						detail.setState(ProcessStates.PROCESS_COMPLETE);
					else {
						// If not terminated and root node != current node then process is running.
						// If not terminated and root node = current node then process is loaded
						
						// index 5 is start node name and index 5 is current node name
						// index 3 is the end date of the process instance
						if (array[6] == null || array[5] == null)
							// some node name is not given -> set status PROCESS_REASON_NONE
							detail.setState(ProcessStates.PROCESS_REASON_NONE);
						
						else {
							if (!((String) array[6]).equals((String) array[5]) && array[3] != null)
								detail.setState(ProcessStates.PROCESS_RUNNING);
							
							else if (((String) array[6]).equals((String) array[5]) && array[3] != null)
								detail.setState(ProcessStates.PROCESS_LOADED);
							
							else
								detail.setState(ProcessStates.PROCESS_REASON_NONE);
						}
					}
					
					details.add(detail);
				}
			} finally {
				jbpmContext.close();
			}
			
			return details;
			
			
			
		} else {
			
			String queryString = "select p from org.jbpm.graph.exe.ProcessInstance as p";
			// TODO These joins should be made context dependend. They are only needed for some filter options. 
			queryString += " join p.processDefinition as definition";
			queryString += " join definition.startState as start";
			queryString += " join p.rootToken as token";
			queryString += " join token.node as node";
			
			boolean whereAlreadyAdded = false;
			
			// process name (which is the name of the process definition in JBPM)
			if (filter.getProcessName() != null) {
				if ( filter.getProcessName() != null && filter.getProcessName().getLocalPart().contains( "%" ) )
				{
					queryString += getOperator(whereAlreadyAdded) + " definition.name LIKE '" + filter.getProcessName().getLocalPart() + "'";
				}
				else
				{
					queryString += getOperator(whereAlreadyAdded) + " definition.name = '" + filter.getProcessName().getLocalPart() + "'";
				}
				whereAlreadyAdded = true;
			}
			
			// process state
			switch (filter.getProcessState()) {
				case ProcessStates.PROCESS_COMPLETE:
					queryString += getOperator(whereAlreadyAdded) + " p.end != null";
					if ( filter.definesNotInDefinedEndStateConstraint() )
					{
						queryString += " and node.name != '" + filter.getNotInDefinedEndState() + "'";
					}
					whereAlreadyAdded = true;
					break;
				case ProcessStates.PROCESS_RUNNING:
					queryString += getOperator(whereAlreadyAdded) + " node.name != start.name and p.end = null";
					if ( filter.definesNotInDefinedEndStateConstraint() )
					{
						queryString += " and node.name != '" + filter.getNotInDefinedEndState() + "'";
					}
					whereAlreadyAdded = true;
					break;
				case ProcessStates.PROCESS_LOADED:
					queryString += getOperator(whereAlreadyAdded) + " node.name = start.name and p.end = null";
					whereAlreadyAdded = true;
					break;
				case ProcessStates.PROCESS_SUSPENDED:
					queryString += getOperator(whereAlreadyAdded) + " p.isSuspended = 'true'";
					whereAlreadyAdded = true;
					break;
				case ProcessStates.PROCESS_RUNNING_OR_LOADED:
					queryString += getOperator(whereAlreadyAdded) + " p.end = null";
					if ( filter.definesNotInDefinedEndStateConstraint() )
					{
						queryString += " and node.name != '" + filter.getNotInDefinedEndState() + "'";
					}
					whereAlreadyAdded = true;
					break;
				case ProcessStates.PROCESS_RUNNING_OR_COMPLETED_BUT_NOT_LOADED:
					queryString += getOperator(whereAlreadyAdded) + " node.name != start.name";
					if ( filter.definesNotInDefinedEndStateConstraint() )
					{
						queryString += " and node.name != '" + filter.getNotInDefinedEndState() + "'";
					}
					whereAlreadyAdded = true;
					break;
				case ProcessStates.PROCESS_DELETED:
					queryString += getOperator(whereAlreadyAdded) + " node.name = 'ProcessDeleted'";
					whereAlreadyAdded = true;
					break;
					
				// PROCESS_FAULTED AND PROCESS_COMPENSATABLE have no equivalent in JBPM
			}
			
			if (filter.getProcessState() != ProcessStates.PROCESS_DELETED) {
				queryString += getOperator(whereAlreadyAdded) + " node.name != 'ProcessDeleted'";
				whereAlreadyAdded = true;
			}
			
			// every processes started after x
			if (filter.getProcessCreateStart() != null) {
				queryString += getOperator(whereAlreadyAdded) + " p.start > '" + format.format(filter.getProcessCreateStart().getTime()) + "'";
				whereAlreadyAdded = true;
			}
			
			// every processes started before x
			if (filter.getProcessCreateEnd() != null) {
				queryString += getOperator(whereAlreadyAdded) + " p.start < '" + format.format(filter.getProcessCreateEnd().getTime()) + "'";
				whereAlreadyAdded = true;
			}
			
			// every processes ended after x
			if (filter.getProcessCompleteStart() != null) {
				queryString += getOperator(whereAlreadyAdded) + " p.end != null and p.end > '" + format.format(filter.getProcessCompleteStart().getTime()) + "'";
				whereAlreadyAdded = true;
			}
			
			// every processes ended before x
			if (filter.getProcessCompleteEnd() != null) {
				queryString += getOperator(whereAlreadyAdded) + " p.end != null and p.end < '" + format.format(filter.getProcessCompleteEnd().getTime()) + "'";
				whereAlreadyAdded = true;
			}
			
			// add where clause for process owners if any exist
			if (filter.getProcessOwner() != null) {
				queryString += getOperator(whereAlreadyAdded) + " p.key IN (";
				for (int i = 0; i < filter.getProcessOwner().size(); i++) {
					queryString += "'" + filter.getProcessOwner().get(i) + "'";
					if (i < filter.getProcessOwner().size() - 1)
						queryString += ", ";
				}
				queryString += ") ";
			}
			
			// sorting
			if (filter.getSortColumn() != null) {
				String sortColumn = filter.getSortColumn();
				if (!sortColumn.contains(".")) {
					sortColumn = "p." + sortColumn;
	 			}
				
				queryString += " order by " + sortColumn;
				queryString += " " + (filter.getSortDir() != null ? filter.getSortDir() : "ASC"); 
			}

			JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
			List<ProcessDetail> details = null;
			try {
				Session session = jbpmContext.getSession();
				Query query = session.createQuery(queryString);
				query.setFirstResult(filter.getListStart());
			
				// set limit
				if (filter.getMaxReturn() != 0)
					query.setMaxResults(filter.getMaxReturn());
				
				// fire query and create ProcessDetails from result
				Iterator results = query.iterate();
				details = new LinkedList<ProcessDetail>();
				while (results.hasNext()) {
					// create ProcessDetail and fill with data from processInstace
					ProcessInstance instance = (ProcessInstance) results.next();
					ProcessDetail detail = this.createProcessDetailFromProcessInstance(instance);
					details.add(detail);
				}
			} finally {
				jbpmContext.close();
			}
			
			return details;
		}
	}

	
	/** helper method for building up an HQL query
	 * 
	 * @param whereAlreadyAdded
	 * @return
	 */
	private String getOperator(boolean whereAlreadyAdded) {
		if (whereAlreadyAdded)
			return " and";
		else
			return " where";
	}


	/** returns the number of processes that match the given filter
	 * @throws ProcessEngineException 
	 * 
	 */
	public int getProcessCount(ProcessFilter filter) throws ProcessEngineException {
		return this.getProcessList(filter).size();
	}

	
	/** returns detailed information about a process identified by its process id
	 * @throws RemoteException 
	 * 
	 */
	public ProcessDetail getProcessDetail(long processId) throws ProcessEngineException {
		ProcessInstance instance = getProcessById(processId);
		if (instance != null) {
			ProcessDetail detail = this.createProcessDetailFromProcessInstance(instance);
			return detail;
		}
		
		return null;
	}
	
	
	/** creates an object of ProcessDetail and fills it with data from
	 * the given ProcessInstance object
	 * 
	 * @param instance
	 * @return
	 * @throws ProcessEngineException 
	 */
	private ProcessDetail createProcessDetailFromProcessInstance(ProcessInstance instance) throws ProcessEngineException {
		ProcessDetail detail = new ProcessDetail();
		
		detail.setProcessId(instance.getId());
		
		// the name in process detail is the name of the process type (the process
		// definition in JBPM)
		detail.setName(new QName(instance.getProcessDefinition().getName()));
		
		if (instance.getStart() != null) {
			Calendar start = Calendar.getInstance();
			start.setTime(instance.getStart());
			detail.setStarted(start);
		}
		
		if (instance.getEnd() != null) {
			Calendar end = Calendar.getInstance();
			end.setTime(instance.getEnd());
			detail.setEnded(end);
		}
		
		detail.setEngineIdentifier(this.identifier);
		
		// We do not have state reasons in JBPM
		detail.setStateReason(-1);
		
		//detail.setResponsibleStafferId((String) this.getVariable(instance.getId(), ProcessApi.getVariableXpath(ServiceConstants.PROCESS_OWNER_VARIABLE)));
		detail.setResponsibleStafferId(instance.getKey());
		
		// not used by now
		detail.setType(null);
		
		detail.setState(this.getProcessState(instance));
				
		return detail;
	}


	/** returns the state of a process identified by its process id
	 * @throws RemoteException 
	 * 
	 */
	public int getProcessState(long processId) throws ProcessEngineException {
		ProcessInstance instance = this.getProcessById(processId);
		return this.getProcessState(instance);
	}
	
	
	/** computes the state of a process instance and returns it 
	 * 
	 * @param instance
	 * @return
	 */
	private int getProcessState(ProcessInstance instance) {
		if (instance.isSuspended())
			return ProcessStates.PROCESS_SUSPENDED;
		
		if (instance.hasEnded())
			return ProcessStates.PROCESS_COMPLETE;
		
		// If not terminated and root node != current node then process is running.
		// If not terminated and root node = current node then process is loaded
		String currentNodeName = instance.getRootToken().getNode().getName();
		String startNodeName = instance.getProcessDefinition().getStartState().getName();
		if (!currentNodeName.equals(startNodeName) && !instance.hasEnded())
			return ProcessStates.PROCESS_RUNNING;
		else if (currentNodeName.equals(startNodeName) && !instance.hasEnded())
			return ProcessStates.PROCESS_LOADED;

		// ProcessStates.PROCESS_COMPENSATABLE and ProcessStates.PROCESS_FAULTED are not used by JBPM by now
		
		return ProcessStates.PROCESS_REASON_NONE;
	}
	

	/** returns the value of a variable named xpath associated to the process
	 * with the id processId
	 * 
	 */
	public String getVariable(long processId, String xpath) throws ProcessEngineException {
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		ProcessInstance instance = null;
		
		try {
			instance = jbpmContext.getProcessInstance(processId);
			
			// extract variable name from xpath expression
			String name = this.extractVariableNameFromXPathExpression(xpath);
			
			Object variable = instance.getContextInstance().getVariable(name);
			return (String) variable;
		} catch (Exception e) {
			// problems accessing the process engine
			throw new ProcessEngineException(e);
		} finally {
			jbpmContext.close();
		}
	}
	
	
	/** sets a value for a given or new variable in a process instance
	 * identified by its process id
	 * 
	 */
	public void setVariable(long processId, String xpath, String value) throws ProcessEngineException {
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		ProcessInstance instance = null;
		
		try {
			instance = jbpmContext.getProcessInstance(processId);
			
			// extract variable name from xpath expression
			String name = this.extractVariableNameFromXPathExpression(xpath);
		
			instance.getContextInstance().setVariable(name, value);
			jbpmContext.save(instance);
		} catch (Exception e) {
			// problems accessing the process engine
			throw new ProcessEngineException(e);
		} finally {
			jbpmContext.close();
		}
	}


	/** resumes a process identified by its process id
	 * 
	 */
	public void resumeProcess(long processId) throws ProcessEngineException {
		ProcessInstance instance = getProcessById(processId);
		instance.resume();
		this.saveProcessInstance(instance);
	}


	/** suspends a process identified by its process id
	 * 
	 */
	public void suspendProcess(long processId) throws ProcessEngineException {
		ProcessInstance instance = getProcessById(processId);
		instance.suspend();
		this.saveProcessInstance(instance);
	}

	
	/** marks a process instance as terminated
	 * 
	 */
	public void terminateProcess(long processId) {
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		
		try {
			jbpmContext.getProcessInstance(processId).end();
		} finally {
			jbpmContext.close();
		}
	}
	
	
	/** this method performs a callback for a completed or closed task
	 * 
	 */
	public void doCallback(Task task, Map<String, Object> returnData) throws ProcessEngineException {
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();

		TaskInstance instance = null;
		
		try {
			instance = jbpmContext.getTaskInstance(Long.valueOf(task.getTaskId()));

			if (instance == null)
				// the task ist not available in process engine
				return;
			
			// set return data into task in JBPM
			instance.setVariables(returnData);
			
			// define task as completed in JBPM
			instance.end();
			
			jbpmContext.save(instance);
		} catch (Exception e) {
			// problems accessing the process engine
			throw new ProcessEngineException(e);
		} finally {
			jbpmContext.close();
		}
	}
	
	
	/** returns the identifier of the JBPM process engine instance
	 * 
	 */
	public String getIdentifier() {
		// only for identifying reasons. It is not needed to connect to JBPM.
		return this.identifier;
	}

	
	/** sets the identifier for the JBPM process engine instance
	 *  
	 */
	public void setIdentifier(String identifier) {
		// only for identifying reasons. It is not needed to connect to JBPM.
		this.identifier = identifier;
	}

	
	/** fetches a process instance from JBPM
	 * 
	 * @param processInstance
	 * @return
	 * @throws ProcessEngineException
	 */
	private ProcessInstance getProcessById(long processId) throws ProcessEngineException {
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();

		ProcessInstance instance = null;
		
		try {
			instance = jbpmContext.getProcessInstance(processId);

			// the following line are there because we want also fetch
			// the process definition and the root token
			if (instance != null) {
				instance.getProcessDefinition().getName();
				instance.getRootToken().getNode().getName();
				instance.getProcessDefinition().getStartState().getName();
			}
		} catch (Exception e) {
			// problems accessing the process engine
			throw new ProcessEngineException(e);
		} finally {
			jbpmContext.close();
		}
		
		return instance;
	}
	
	
	/** stores a process instance in the JBPM database
	 * 
	 * @param instance
	 */
	private void saveProcessInstance(ProcessInstance instance) {
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		
		try {
			jbpmContext.save(instance);
		} finally {
			jbpmContext.close();
		}
	}
	

	/** variable names are given as XPath expression. To store a variable we
	 * have to extract the name from the XPath expression
	 * 
	 * @param xpath
	 * @return
	 */
	private String extractVariableNameFromXPathExpression(String xPath) {
		// We want to extract "variableName" from "/process/variables/variable[@name='" + variableName + "']"
		// search for ' and extract everything in between
		return xPath.substring(xPath.indexOf("'") + 1, xPath.indexOf("'", xPath.indexOf("'") + 1));
	}
	
}
