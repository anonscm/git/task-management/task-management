/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query; 

import de.tarent.wfms.processmiddleware.data.Attachment;
import de.tarent.wfms.processmiddleware.util.JPAFilterHelper;

public class AttachmentDAO {

	private static AttachmentDAO instance = null;

    
    public static synchronized AttachmentDAO getInstance() {
        if (instance == null)
            instance = new AttachmentDAO();
        return instance;
    }

    
    @SuppressWarnings("unchecked")
	public List<Attachment> getAllDeepByTaskPk(Integer taskId, EntityManager em) {
    	return (List<Attachment>) em.createNamedQuery(Attachment.STMT_SELECT_DEEP_BY_TASK_FK).setParameter("fk", taskId).getResultList();
    }


	@SuppressWarnings("unchecked")
	public List<Attachment> getAll(AttachmentFilter filter, EntityManager em) {
		Query query = JPAFilterHelper.getQueryWithFilter(Attachment.class, Attachment.STMT_SELECT_ALL, filter, em);
		
		query.setMaxResults(filter.getLimit());
		query.setFirstResult(filter.getStart());
		
		return (List<Attachment>) query.getResultList();
	}
	
	public static void main(String[] args, EntityManager em) {
		Query query = em.createNamedQuery(Attachment.STMT_SELECT_ALL);
		System.out.println(query.toString());
	}
    
}
