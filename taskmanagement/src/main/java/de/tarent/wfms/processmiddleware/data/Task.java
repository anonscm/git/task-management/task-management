/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.data;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.FetchType;

import de.tarent.commons.utils.converter.XMLDecoderConverter;
import de.tarent.commons.utils.converter.XMLEncoderConverter;
import de.tarent.wfms.processmiddleware.dao.TaskStafferDAO;
import de.tarent.wfms.processmiddleware.task.action_checker.TaskActionChecker;

/**
 * This class represents a task which has to be handled by a staffer.
 * A task is considered as an atomic part of a business process.
 * It will be created by a process which is executed by a process engine.
 * 
 * @author Hendrik Helwich
 * @author Christian Preilowski (c.preilowski@tarent.de)
 * @author Martin Pelzer, tarent GmbH
 *
 */
@NamedQueries({
	@NamedQuery(name = Task.STMT_SELECT_ONE_BY_ID, query = "select x from Task as x where x.taskId = :id"),
	@NamedQuery(name = Task.STMT_SELECT_ONE_BY_PK, query = "select x from Task as x where x.pk = :pk"),
	@NamedQuery(name = Task.STMT_SELECT_FAILED, query = "select x from Task as x where escalationDate2 < :escalationDate and endDate = null and failed = false"),
	@NamedQuery(name = Task.STMT_SELECT_WARNING, query = "select x from Task as x where escalationDate1 < :escalationDate and endDate = null and sendWarning = false and failed = false"),
	@NamedQuery(name = Task.STMT_SELECT_ALL_ORDER_BY_START_DATE, query = "select x from Task as x order by startDate"),
	@NamedQuery(name = Task.STMT_SELECT_ALL, query = "select x from Task as x"),
	@NamedQuery(name = Task.STMT_SELECT_ALL_JOIN_STAFFER, query = "select distinct x from Task as x left outer join x.stafferList as staffer left outer join x.parent as parentProcessNode"),
	@NamedQuery(name = Task.STMT_COUNT_ALL_JOIN_STAFFER, query = "select count(distinct x) from Task as x left outer join x.stafferList as staffer left outer join x.parent as parentProcessNode"),
	@NamedQuery(name = Task.STMT_COUNT_BY_TASK_TYPE, query = "select count(x) from Task as x join x.type as type where type.pk = :taskTypeId"),
	@NamedQuery(name = Task.STMT_COUNT, query = "select count(x) from Task as x"),
	@NamedQuery(name = Task.STMT_SELECT_TASKS_BY_PARENT_PROCESS, query = "select t from Task as t join t.parent as p where p.processId = :processId and p.engineUrl = :engineUrl order by t.intendedEndDate asc")
})
@Entity
@Table(name = "task")
public class Task {
	
	public static final String STMT_SELECT_ONE_DETAIL = "stmtSelectTaskDetail";
	public static final String STMT_SELECT_ONE_BY_ID = "stmtSelectTaskById";
	public static final String STMT_SELECT_ONE_BY_PK = "stmtSelectTaskByPk";
	public static final String STMT_SELECT_FAILED = "stmtSelectFailedTask";
	public static final String STMT_SELECT_WARNING = "stmtSelectWarningTask";
	public static final String STMT_SELECT_ALL = "stmtSelectAllTasks";
	public static final String STMT_SELECT_ALL_JOIN_STAFFER = "stmtSelectAllTasksJoinStaffer";
	public static final String STMT_COUNT_ALL_JOIN_STAFFER = "stmtCountAllTasksJoinStaffer";
	public static final String STMT_COUNT_BY_TASK_TYPE = "stmtCountTasksByTaskType";
	public static final String STMT_COUNT = "stmtCountTasks";
	public static final String STMT_SELECT_ALL_ORDER_BY_START_DATE = "stmtSelectAllTasksOrderByStartDate";
	public static final String STMT_SELECT_TASKS_BY_PARENT_PROCESS = "stmtSelectTasksByParentProcess";
	

	/**
	 * Specifies at which time the staffer will get a notification email.
	 * This variable holds the difference of the intended end date and the
	 * notification date in seconds.  
	 */
	private static final int NOTIFICATION_DIFFERNCE = 172800; // 172800 seconds = 60*60*24*2  seconds = 2 days
	
	
	private long pk;
	private Date creationDate;
	private Date updateDate;
	private String taskId; // not null
	private Long fkParentProcessNode; // not null
	private Long fkTaskType; // not null
	private String description; // not null
	private Date startDate;
	private String processName;
	private String processCreator;
	private Date endDate;
	private Date intendedEndDate;
	private Date escalationDate1; // not null
	private Date escalationDate2; // not null
	private Map<String,Object> data;
	private String replyToUrl;
	private Boolean failed; // not null
	private Boolean sendWarning; // not null
	private Date deleteDate;
	private List<TaskStaffer> stafferList = new LinkedList<TaskStaffer>();
	private List<Comment> commentList = new LinkedList<Comment>();
	private List<Attachment> attachmentList = new LinkedList<Attachment>();
	private TaskType type;
	private ProcessNode parent;

	private TaskActionChecker actionChecker; 

	
	// @Column(name = "delete_date")
	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	/*
	 * must order by here, otherwise the order of the staffers in the list
	 * is not in descending order of staffer participation. 
	 */
	@OneToMany(mappedBy = "task")
	@OrderBy( "startDate DESC" )
	public List<TaskStaffer> getStafferList() {
		return stafferList;
	}

	public void setStafferList(List<TaskStaffer> stafferList) {
		this.stafferList = stafferList;
	}

	public void addStaffer(TaskStaffer staffer) {
		if (! stafferList.contains(staffer))
			stafferList.add(staffer);
	}

	@Transient
	public TaskStaffer getActualStaffer() {
		return stafferList == null || stafferList.size() == 0 ? null : stafferList.get(0);
	} 

	/**
     * Returns the data structure which is attached to the task.
     * The type of this data-structure is task dependant.
     * 
     * @return  The data structure which is attached to the task.
     */
	@Transient
	public Map<String,Object> getData() {
    	return data;
	}
	
    /**
     * Attaches a data structure which to the task.
     * The type of this data-structure is task dependant.
     * 
	 * @param   data
	 *          The data structure which is attached to the task.
	 */
	public void setData(Map<String,Object> data) {
		this.data = data;
	}
	
	
	/** This method only exists for converting the data map to a XML string
	 * when inserting in the database and converting back when retrieving from
	 * the database.
	 * 
	 */
	@Column(name = "data", length = 20000)
	public String getDataAsString() {
		XMLEncoderConverter converter = new XMLEncoderConverter(Map.class);
		return (String) converter.doConversion(this.data);
	}
	
	/** This method only exists for converting the data map to a XML string
	 * when inserting in the database and converting back when retrieving from
	 * the database.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void setDataAsString(String data) {
		XMLDecoderConverter decoder = new XMLDecoderConverter(Map.class);
		this.data = (Map) decoder.doConversion(data);
	}

	/**
	 * Returns a description of the task.
	 * This value is required by a valid task.
	 * 
	 * @return  A description of the task
	 */
	public String getDescription() {
         return description;
	}

	/**
	 * Sets a description of the task.
	 * This value is required by a valid task.
	 * 
	 * @param   description
	 *          A description of the task
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the end date of the task.
	 * This value will be set by the processmiddleware if a staffer has
	 * finished this task.
	 * 
	 * @return  The end date of the task.
	 */
	//@Column(name = "end_date")
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date of the task.
	 * This value will be set by the processmiddleware if a staffer has
	 * finished this task.
	 * 
	 * @param   endDate
	 *          The end date of the task.
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Returns the escalation date of this task.
	 * The task can only be changed or finished before that date.
	 * If the current time is equal or later than the escalation date
	 * and the task has no end date, then the task has caused a fault.
	 * This value will be set by the process engine.
	 * 
	 * @return  The escalation date of this task
	 */
	//@Column(name = "escalation_date1")
	public Date getEscalationDate1() {
		return escalationDate1;
	}

	/**
	 * Sets the escalation date of this task.
	 * The task can only be changed or finished before that date.
	 * If the current time is equal or later than the escalation date
	 * and the task has no end date, then the task has caused a fault.
	 * This value will be set by the process engine.
	 * 
	 * @param   escalationDate
	 *          The escalation date of this task
	 */
	public void setEscalationDate1(Date escalationDate) {
		escalationDate1 = escalationDate;
	}

	//@Column(name = "escalation_date2")
	public Date getEscalationDate2() {
		return escalationDate2;
	}

	public void setEscalationDate2(Date escalationDate) {
		escalationDate2 = escalationDate;
	}

	/**
	 * Returns the unique external task id.
	 * This id will be set by the process which creates the task in an
	 * asynchronous way.
	 * It can be used to refer to this task while it is still active.
	 * This value is required by a valid task.
	 * 
	 * @return  The unique external task id
	 */
	public String getTaskId() {
		return taskId;
	}

	/**
	 * Sets the unique external task id.
	 * This id will be set by the process which creates the task in an
	 * asynchronous way.
	 * It can be used to refer to this task while it is still active.
	 * This value is required by a valid task.
	 * 
	 * @param   id
	 *          The unique external task id
	 */
	public void setTaskId(String id) {
		this.taskId = id;
	}

	/**
	 * Returns the intended end date of the task.
	 * It hast to be before or equal to the escalation date and it also has
	 * to be later than the start date.
	 * 
	 * @return  The intended end date of the task
	 */
	//@Column(name = "intended_end_date")
	public Date getIntendedEndDate() {
		return intendedEndDate;
	}

	/**
	 * Sets the intended end date of the task.
	 * It hast to be before or equal to the escalation date and it also has
	 * to be later than the start date.
	 * 
	 * @param   intendedEndDate
	 *          The intended end date of the task
	 */
	public void setIntendedEndDate(Date intendedEndDate) {
		this.intendedEndDate = intendedEndDate;
	}
	

	/**
	 * Returns the start date of the task. This value will be automatically
	 * be set by a database system trigger.
	 * 
	 * @return  The start date of the task
	 */
	//@Column(name = "start_date")
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date of the task. This value will be automatically
	 * be set by a database system trigger.
	 * 
	 * @param   startDate
	 *          The start date of the task
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Returns the primary key of the related task type.
	 * 
	 * @return  The primary key of the related task type
	 */
	//@Column(name = "fk_task_type")
	@Transient
	public Long getFkTaskType() {
		return fkTaskType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_task_type")
	public TaskType getType() {
		return type;
	}

	public void setType(TaskType type) {
		this.type = type;
	}

	/**
	 * Sets the primary key of the related task type.
	 * 
	 * @param   fkTaskType
	 *          The primary key of the related task type
	 */
	public void setFkTaskType(Long fkTaskType) {
		this.fkTaskType = fkTaskType;
	}

	public Boolean getFailed() {
		return failed;
	}

	public void setFailed(Boolean failed) {
		this.failed = failed;
	}

	@Column(name = "send_warning")
    public Boolean getSendWarning() {
		return sendWarning;
	}

	public void setSendWarning(Boolean sendWarning) {
		this.sendWarning = sendWarning;
	}
	
	/**
	 * Returns <code>true</code> if the task is finished.
	 * This value is <code>true</code> if the <code>endDate</code> property
	 * is not <code>null</code>.
	 * 
	 * @return  <code>true</code> if the task is finished
	 */
	@Transient
	public boolean isClosed() {
		return endDate != null;
	}

	/**
	 * Returns <code>true</code> if the task is not closed and it should be
	 * processed immediatly.
	 * 
	 * @see #NOTIFICATION_DIFFERNCE
	 * 
	 * @return  <code>true</code> if the task should be processed immediatly.
	 */
	@Transient
	public boolean isImmediate() {
		return ! isClosed() && getEscalationDate1().getTime() - System.currentTimeMillis() < NOTIFICATION_DIFFERNCE * 1000;
	}

	/**
	 * Returns <code>true</code> if the task is not closed and the current
	 * time has exceeded the intended end date.
	 * If this method returns <code>true</code>, then the return value of
	 * method {@link #isImmediate()} returns also <code>true</code>.
	 *  
	 * @see #isClosed()
	 * 
	 * @return  <code>true</code> if the task is not closed and the current
	 *          time has exceeded the intended end date.
	 */
	@Transient
	public boolean isEscalated() {
		return ! isClosed() && getEscalationDate1().getTime() < System.currentTimeMillis();
	}
	
	/**
	 * Returns the task properties as a string.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return 
		    "pk                 : " + getPk() + "\n" +
			"id                 : " + taskId + "\n" +
			"fkParentProcessNode: " + fkParentProcessNode + "\n" +
			"description        : " + description + "\n" +
			"type               : " + type + "\n" +
			"startDate          : " + startDate + "\n" +
			"endDate            : " + endDate + "\n" +
			"intendedEndDate    : " + intendedEndDate + "\n" +
			"escalationDate1    : " + escalationDate1 + "\n" +
			"escalationDate2    : " + escalationDate2 + "\n" +
			"data               : " + data + "\n" +
			"replyToUrl         : " + replyToUrl + "\n" +
	    	"creationdate       : " + getCreationDate() + "\n" +
	    	"updatedate         : " + getUpdateDate() + "\n";
	}

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_task", updatable = false)
	public List<Attachment> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<Attachment> attachmentList) {
		this.attachmentList = attachmentList;
	}

	public void addAttachment(Attachment attachment) {
		if (! attachmentList.contains(attachment) && !attachment.isDeleted())
			attachmentList.add(attachment);
	}

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_task", updatable = false)
	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public void addComment(Comment comment) {
		if (! commentList.contains(comment))
			commentList.add(comment);
	}

	@Column(name = "reply_to_url")
	public String getReplyToUrl() {
		return replyToUrl;
	}

	public void setReplyToUrl(String replyToUrl) {
		this.replyToUrl = replyToUrl;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkparentprocessnode")
	public ProcessNode getParent(){
		return this.parent;
	}
	
	public void setParent(ProcessNode parent){
		this.parent = parent;
	}
	
	@Transient
	public long getRootId(){
		return this.getParent().getFkRootProcessNode();
	}
	
	@Transient
	public boolean isForwardable(){
		return this.getActionChecker().isForwardable();
	}
	
	@Transient
	public boolean isCloseable(){
		return this.getActionChecker().isCloseable();
	}
	
	@Transient
	public boolean isReopenable(){
		return this.getActionChecker().isReopenable();
	}
	
	@Transient
	public boolean isResubmitable(){
		return getActionChecker().isResubmitable();
	}
	
	@Transient
	public String getForwardMessage(){
		return this.getActionChecker().getForwardMessage();
	}
	
	@Transient
	public String getReopenMessage(){
		return this.getActionChecker().getReopenMessage();
	}

	@Transient
	public String getCloseMessage(){
		return this.getActionChecker().getCloseMessage();
	}
	
	@Transient
	public String getResubmitMessage(){
		return this.getActionChecker().getResubmitMessage();
	}

	@Transient
	public TaskActionChecker getActionChecker() {
		if (actionChecker==null)
			setActionChecker(type.getTaskActionChecker(this));
		return actionChecker;
	}

	private void setActionChecker(TaskActionChecker actionChecker) {
		this.actionChecker = actionChecker;
	}

	@Id
	@GeneratedValue()
	@Column(name = "pk_task", nullable = false)
	public long getPk() {
		return pk;
	}

	/**
	 * Sets the primary key of this object.
	 * 
	 * @param   pk
	 *          The primary key
	 */
	public void setPk(long pk) {
		this.pk = pk;
	}
	
	/**
	 * Returns the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  creationDate
	 *          The creation time
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   creationDate
	 *          The creation time
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Returns the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  updateDate
	 *          The update time
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	
	/**
	 * Sets the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   updateDate
	 *          The update time
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public void setProcessName(String processName)
	{
		this.processName = processName;
	}
	
	public String getProcessName()
	{
		return this.processName;
	}

	public String getProcessCreator() {
		return processCreator;
	}

	public void setProcessCreator(String processCreator) {
		this.processCreator = processCreator;
	}

	/*
	 * introduced as part of fix for issue 500
	 */
	@Transient
	public boolean isDeleted()
	{
		return ( this.getDeleteDate() != null );
	}
}
