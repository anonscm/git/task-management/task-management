/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on Jul 24, 2007
 */

package de.tarent.wfms.processmiddleware.task.notifier;

import de.tarent.wfms.processmiddleware.data.Task;

public interface EscalationNotifier
{
    public void notify(Task task);
}
