/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * AesProcessFilter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package de.tarent.wfms.processmiddleware.engine.xsd;

public class AesProcessFilter  extends AesListingFilter  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

    private java.util.Calendar processCompleteEnd;

    private java.util.Calendar processCompleteStart;

    private java.util.Calendar processCreateEnd;

    private java.util.Calendar processCreateStart;

    private javax.xml.namespace.QName processName;

    private int processState;

    private java.util.Map advancedQuery;

    public AesProcessFilter() {
    }

    public AesProcessFilter(
           java.util.Calendar processCompleteEnd,
           java.util.Calendar processCompleteStart,
           java.util.Calendar processCreateEnd,
           java.util.Calendar processCreateStart,
           javax.xml.namespace.QName processName,
           int processState,
           java.util.Map advancedQuery) {
        this.processCompleteEnd = processCompleteEnd;
        this.processCompleteStart = processCompleteStart;
        this.processCreateEnd = processCreateEnd;
        this.processCreateStart = processCreateStart;
        this.processName = processName;
        this.processState = processState;
        this.advancedQuery = advancedQuery;
    }


    /**
     * Gets the processCompleteEnd value for this AesProcessFilter.
     * 
     * @return processCompleteEnd
     */
    public java.util.Calendar getProcessCompleteEnd() {
        return processCompleteEnd;
    }


    /**
     * Sets the processCompleteEnd value for this AesProcessFilter.
     * 
     * @param processCompleteEnd
     */
    public void setProcessCompleteEnd(java.util.Calendar processCompleteEnd) {
        this.processCompleteEnd = processCompleteEnd;
    }


    /**
     * Gets the processCompleteStart value for this AesProcessFilter.
     * 
     * @return processCompleteStart
     */
    public java.util.Calendar getProcessCompleteStart() {
        return processCompleteStart;
    }


    /**
     * Sets the processCompleteStart value for this AesProcessFilter.
     * 
     * @param processCompleteStart
     */
    public void setProcessCompleteStart(java.util.Calendar processCompleteStart) {
        this.processCompleteStart = processCompleteStart;
    }


    /**
     * Gets the processCreateEnd value for this AesProcessFilter.
     * 
     * @return processCreateEnd
     */
    public java.util.Calendar getProcessCreateEnd() {
        return processCreateEnd;
    }


    /**
     * Sets the processCreateEnd value for this AesProcessFilter.
     * 
     * @param processCreateEnd
     */
    public void setProcessCreateEnd(java.util.Calendar processCreateEnd) {
        this.processCreateEnd = processCreateEnd;
    }


    /**
     * Gets the processCreateStart value for this AesProcessFilter.
     * 
     * @return processCreateStart
     */
    public java.util.Calendar getProcessCreateStart() {
        return processCreateStart;
    }


    /**
     * Sets the processCreateStart value for this AesProcessFilter.
     * 
     * @param processCreateStart
     */
    public void setProcessCreateStart(java.util.Calendar processCreateStart) {
        this.processCreateStart = processCreateStart;
    }


    /**
     * Gets the processName value for this AesProcessFilter.
     * 
     * @return processName
     */
    public javax.xml.namespace.QName getProcessName() {
        return processName;
    }


    /**
     * Sets the processName value for this AesProcessFilter.
     * 
     * @param processName
     */
    public void setProcessName(javax.xml.namespace.QName processName) {
        this.processName = processName;
    }


    /**
     * Gets the processState value for this AesProcessFilter.
     * 
     * @return processState
     */
    public int getProcessState() {
        return processState;
    }


    /**
     * Sets the processState value for this AesProcessFilter.
     * 
     * @param processState
     */
    public void setProcessState(int processState) {
        this.processState = processState;
    }


    /**
     * Gets the advancedQuery value for this AesProcessFilter.
     * 
     * @return advancedQuery
     */
    public java.util.Map getAdvancedQuery() {
        return advancedQuery;
    }


    /**
     * Sets the advancedQuery value for this AesProcessFilter.
     * 
     * @param advancedQuery
     */
    public void setAdvancedQuery(java.util.Map advancedQuery) {
        this.advancedQuery = advancedQuery;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AesProcessFilter)) return false;
        AesProcessFilter other = (AesProcessFilter) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.processCompleteEnd==null && other.getProcessCompleteEnd()==null) || 
             (this.processCompleteEnd!=null &&
              this.processCompleteEnd.equals(other.getProcessCompleteEnd()))) &&
            ((this.processCompleteStart==null && other.getProcessCompleteStart()==null) || 
             (this.processCompleteStart!=null &&
              this.processCompleteStart.equals(other.getProcessCompleteStart()))) &&
            ((this.processCreateEnd==null && other.getProcessCreateEnd()==null) || 
             (this.processCreateEnd!=null &&
              this.processCreateEnd.equals(other.getProcessCreateEnd()))) &&
            ((this.processCreateStart==null && other.getProcessCreateStart()==null) || 
             (this.processCreateStart!=null &&
              this.processCreateStart.equals(other.getProcessCreateStart()))) &&
            ((this.processName==null && other.getProcessName()==null) || 
             (this.processName!=null &&
              this.processName.equals(other.getProcessName()))) &&
            this.processState == other.getProcessState() &&
            ((this.advancedQuery==null && other.getAdvancedQuery()==null) || 
             (this.advancedQuery!=null &&
              this.advancedQuery.equals(other.getAdvancedQuery())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getProcessCompleteEnd() != null) {
            _hashCode += getProcessCompleteEnd().hashCode();
        }
        if (getProcessCompleteStart() != null) {
            _hashCode += getProcessCompleteStart().hashCode();
        }
        if (getProcessCreateEnd() != null) {
            _hashCode += getProcessCreateEnd().hashCode();
        }
        if (getProcessCreateStart() != null) {
            _hashCode += getProcessCreateStart().hashCode();
        }
        if (getProcessName() != null) {
            _hashCode += getProcessName().hashCode();
        }
        _hashCode += getProcessState();
        if (getAdvancedQuery() != null) {
            _hashCode += getAdvancedQuery().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AesProcessFilter.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "AesProcessFilter"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processCompleteEnd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "processCompleteEnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processCompleteStart");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "processCompleteStart"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processCreateEnd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "processCreateEnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processCreateStart");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "processCreateStart"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "processName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "QName"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "processState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("advancedQuery");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.active-endpoints.com/activebpeladmin/2007/01/activebpeladmin.xsd", "advancedQuery"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
