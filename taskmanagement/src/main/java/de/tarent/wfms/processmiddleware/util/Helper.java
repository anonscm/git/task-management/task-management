package de.tarent.wfms.processmiddleware.util;

import java.io.File;
import java.net.URISyntaxException;

public class Helper {

	private static final String AXIS_CONFIG_FILENAME = "/axis-config.wsdd";

	private Helper() {}

    private static String axisConfigFileString;

	public static final String getAxisConfigFileString() {
		if (axisConfigFileString == null)
			try {
				//System.out.println("HELPER: "+Helper.class.getResource(AXIS_CONFIG_FILENAME));
				axisConfigFileString = new File(Helper.class.getResource(AXIS_CONFIG_FILENAME).toURI()).toString();
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
		return axisConfigFileString;
	}
}
