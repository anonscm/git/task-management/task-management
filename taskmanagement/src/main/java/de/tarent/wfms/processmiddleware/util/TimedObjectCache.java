package de.tarent.wfms.processmiddleware.util;

import java.util.HashMap;
import java.util.Map;

// FIXME: remove this and replace using SoftReference!!

/**
 * This is a simple cache implementation, which stores objects together with an 
 * expiration date.
 *
 * <p>You can use this cache like a singleton, altrough it is also possible to
 *    create private instances, which may be not target of side effects.</p>
 */
public class TimedObjectCache {

    HashMap<String, StorageObject> map = new HashMap<String, StorageObject>();

    /**
     * Default lifetime of 5 minutes.
     */
    protected static long DEFAULT_LIFETIME = 1000 * 60 * 5;

    /**
     * The defafault clean size
     */
    protected static int CLEAN_SIZE = 500;

    static TimedObjectCache theGlobalInstance = null;

    long defaultLifetime = DEFAULT_LIFETIME;
    int cleanSize = CLEAN_SIZE;

    /**
     * Constructs a new TimedObjectCache. For general purpose it may be better to use the 
     * getGlobalInstance(), shared in the complete VM (classloader).
     */
    public TimedObjectCache() {

    }

    /**
     * Method to retrieve a global instance of this cache.
     * This instance should be used in normal cases.
     */
    public static TimedObjectCache getGlobalInstance() {
        if (theGlobalInstance == null) 
            theGlobalInstance = new TimedObjectCache();
        return theGlobalInstance;
    }


    /**
     * Stores the cacheSubject in this cache. With a default lifetime of 5 minutes.
     *
     * @param uniqueKey A unique key to identify this instance. Since the cache may be used for the whole process, 
     *                  this has to be a key prefixed by e.g. the classname.
     * @param cacheSubject the Object to cache
     */
    public void store(String uniqueKey, Object cacheSubject) {
        store(uniqueKey, cacheSubject, DEFAULT_LIFETIME);
    }

    /**
     * Stores the cacheSubject in this cache. With the maximum lifetime of validityms milliseconds.
     *
     * @param uniqueKey A unique key to identify this instance. Since the cache may be used for the whole process, 
     *                  this has to be a key prefixed by e.g. the classname.
     * @param cacheSubject the Object to cache
     * @param validityms maximum lifetime in cache.
     */
    public void store(String uniqueKey, Object cacheSubject, long validityms) {
        map.put(uniqueKey, new StorageObject(cacheSubject, validityms));
        if (map.size() > cleanSize)
            cleanup();
    }
    
    @SuppressWarnings("unchecked")
	protected synchronized void cleanup() {
    	HashMap<String, StorageObject> localMap = (HashMap<String, StorageObject>)map.clone();
    	
    	for(Map.Entry<String,StorageObject> entry: localMap.entrySet()) {
            if (!entry.getValue().isValid()) {
                map.remove(entry.getKey());
            }
        }
    }
   
    /**
     * Method to retrieve a cached object. If the validity lifetime is expired, null will be returned. 
     * @param uniqueKey A unique key to identify this instance. Since the cache may be used for the whole process, 
     *                  this has to be a key prefixed by e.g. the classname.
     */
    public Object retrieve(String uniqueKey) {
        StorageObject storageObject = map.get(uniqueKey);
        if (storageObject == null)
            return null;

        if (!storageObject.isValid()) {        
            // if we know, its dirty, we clean it
            map.remove(uniqueKey);
            return null;        
        }
        
        return storageObject.getCacheSubject();
    }

    /**
     * Method to retrieve a cached object. If the validity lifetime is expired, null will be returned. It Returns
     * the StorageObject, not only the Object 
     * @param uniqueKey A unique Key, which specifies the Object, which should be returned
     * @return StorageObject which contains the specified object.
     */
    public StorageObject retrieveStorageObject(String uniqueKey){
    	StorageObject storageObject = map.get(uniqueKey);
        if (storageObject == null)
            return null;

        if (!storageObject.isValid()) {        
            // if we know, its dirty, we clean it
            map.remove(uniqueKey);
            return null;        
        }
        
        return storageObject;
    }    
    
    
    public final int getCleanSize() {
        return cleanSize;
    }

    /**
     * Sets the size, 
     * after which invalid item will be removed from the cache.
     */
    public final void setCleanSize(final int newCleanSize) {
        this.cleanSize = newCleanSize;
    }
    
    public final long getDeaultLifetime() {
        return defaultLifetime;
    }

    /** 
     * Sets the default lifetime, which is used, if no special lifetime is specified.
     */
    public final void setDeaultLifetime(final long newDeaultLifetime) {
        this.defaultLifetime = newDeaultLifetime;
    }  
}