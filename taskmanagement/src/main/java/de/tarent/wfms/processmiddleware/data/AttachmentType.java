/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
		@NamedQuery(name = AttachmentType.STMT_SELECT_ONE_BY_ID, query = "select x from AttachmentType as x where x.pk = :id"),
		@NamedQuery(name = AttachmentType.STMT_SELECT_ALL, query = "select x from AttachmentType as x"),
		@NamedQuery(name = AttachmentType.STMT_SELECT_ONE_BY_NAME, query = "select x from AttachmentType as x where name  = :name")
})
@Entity
@Table(name = "attachment_type")
public class AttachmentType {
	
	public static final String STMT_SELECT_ONE_BY_ID = "stmtSelectAttachmentTypeById";
	public static final String STMT_SELECT_ONE_BY_NAME = "stmtSelectAttachmentTypeByName";
	public static final String STMT_SELECT_ALL = "stmtSelectAllAttachmentTypes";
	
	private long pk;
	private Date creationDate;
	private Date updateDate;
	private String name; // not null

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Id
	@GeneratedValue
	@Column(name = "pk_attachment_type", nullable = false)
	public long getPk() {
		return pk;
	}
	
	/**
	 * Sets the primary key of this object.
	 * 
	 * @param   pk
	 *          The primary key
	 */
	public void setPk(long pk) {
		this.pk = pk;
	}
	
	/**
	 * Returns the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  creationDate
	 *          The creation time
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   creationDate
	 *          The creation time
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Returns the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @return  updateDate
	 *          The update time
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	
	/**
	 * Sets the update time of this object.
	 * This value will be set automatically by a database trigger.
	 * 
	 * @param   updateDate
	 *          The update time
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
