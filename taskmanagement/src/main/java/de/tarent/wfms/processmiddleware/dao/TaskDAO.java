/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.util.JPAFilterHelper;


public class TaskDAO {

	private static TaskDAO instance = null;

	
    public static synchronized TaskDAO getInstance() {
        if (instance == null)
            instance = new TaskDAO();
        return instance;
    }
    
    public Task getTaskByPk(Long pk, boolean withType, EntityManager em) {   
        return (Task) em.createNamedQuery(Task.STMT_SELECT_ONE_BY_PK).setParameter("pk", pk).getSingleResult();
    }
    
    /**
     * Retrurns one task by id.
     *
     * @param id The external (general unique) id of the task, which was also used on inserting the task. This is not the pk.
     */
    public Task getTaskById(String id, EntityManager em) {
    	Query query = em.createNamedQuery(Task.STMT_SELECT_ONE_BY_ID).setParameter("id", id);
    	
    	try {
    		return (Task) query.getSingleResult();
    	} catch (NoResultException e) {
    		// no such task -> return null
    		return null;
    	}
    }

    @SuppressWarnings("unchecked")
	public List<Task> getFailedTaskList(EntityManager em) {
    	return (List<Task>) em.createNamedQuery(Task.STMT_SELECT_FAILED).setParameter("escalationDate", new Date()).getResultList();
    }

    @SuppressWarnings("unchecked")
	public List<Task> getWarningTaskList(int hourShift, EntityManager em) {
    	GregorianCalendar cal = new GregorianCalendar();
    	cal.add(Calendar.HOUR, hourShift);
    	
    	return (List<Task>) em.createNamedQuery(Task.STMT_SELECT_WARNING).setParameter("escalationDate", cal.getTime()).getResultList();
    }


    /**
     * Returs all task belonging to one single process node,
     * identified by the id and engine url of the process node.
     */
    @SuppressWarnings("unchecked")
	public List<Task> getTaskListByExternalProcessId(long processId, String engineURL, EntityManager em) { 	
     	return (List<Task>) em.createNamedQuery(Task.STMT_SELECT_TASKS_BY_PARENT_PROCESS)
     				.setParameter("processId", processId)
     				.setParameter("engineUrl", engineURL)
     				.getResultList();
    }
    

    /** 
     * Mark one tasks for the supplied process as deleted
     *
     * @param id The external (general unique) id of the task, which was also used on inserting the task. This is not the pk.
     */
    public void deleteTaskByExternalId(String id, EntityManager em) {
    	Task task = getTaskById(id, em);
    	if (task != null && task.getDeleteDate() == null) {
    		task.setDeleteDate(new Date());
    	}
    }
    

    /** 
     * Mark all tasks for the supplied process as deleted
     *
     * @param engineURL The url to identify the engine, and thereby full qualify the processId.
     * @param processId The process id, the task belongs to. This id is only unique in conjunction with the engineUrl and should not be confused with the pk_process_node.
     */
    public void deleteTasksByProcess(String engineURL, Long processId, EntityManager em) {
        List<Task> taskList = getTaskListByExternalProcessId(processId, engineURL, em);
     	Date deleteDate = new Date();
     	for (Task task : taskList) {
     		if (task.getDeleteDate() == null) {
     			task.setDeleteDate(deleteDate);
            }
        }
    }	
    
    
    public List<Task> getAll(TaskFilter filter, EntityManager em) {
    	return JPAFilterHelper.getQueryWithFilter(Task.class, Task.STMT_SELECT_ALL, filter, em).getResultList();
    }
    
    public List<Task> getAllJoinStaffer(TaskFilter filter, EntityManager em) {
    	filter.setPrefix("x");
    	return JPAFilterHelper.getQueryWithFilter(Task.class, Task.STMT_SELECT_ALL_JOIN_STAFFER, filter, em).getResultList();
    }
    
    
    public long countAllJoinStaffer(TaskFilter filter, EntityManager em) {
    	filter.setPrefix("x");
    	filter.setSortField(null);
    	return (Long) JPAFilterHelper.getQueryWithFilter(Task.class, Task.STMT_COUNT_ALL_JOIN_STAFFER, filter, em).getSingleResult();
    }
    
    
    public long countTasks(long taskTypeId, EntityManager em) {
    	Query query = em.createNamedQuery(Task.STMT_COUNT_BY_TASK_TYPE);
    	query.setParameter("taskTypeId", taskTypeId);
    	return (Long) query.getSingleResult();
    }

	public long countTasks(EntityManager em) {
		Query query = em.createNamedQuery(Task.STMT_COUNT);
		return (Long) query.getSingleResult();
	}
	
}
