/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.task;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.config.Configuration;
import de.tarent.wfms.processmiddleware.dao.TaskDAO;
import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.data.TaskType;
import de.tarent.wfms.processmiddleware.db.DBAccess;

public class TaskObserver implements Runnable {
	
	private static Thread thread;
	private static boolean interrupted;
	
	//public static final String TASK_OBSERVER_INTERVAL_CONFIG_PARAM = "TaskObserverInterval";
	
	public static final int HOUR_SHIFT_WARNING = 48;

	private static Logger log = Logger.getLogger(TaskObserver.class.getName());
	
	//private static OctopusContext octopusContext;
	
	private long sleepTime = 60000;
	
	public static void start() {
		if (thread == null) {
			interrupted = false;
			thread = new Thread(new TaskObserver());
			thread.start();
		}
	}
	
	public TaskObserver(){
		//TaskObserver.octopusContext = octopusContext;
		//this.sleepTime = Long.valueOf(octopusContext.getConfigObject().getModuleConfig().getParam(TASK_OBSERVER_INTERVAL_CONFIG_PARAM));
		this.sleepTime = Long.valueOf(Configuration.getInstance().getParameter("taskObserverInterval"));
	}
	
	public static void stop() {
		interrupted = true;
	}

	public void run() {
		log.info("started task observing thread");
		while (! interrupted) {
			try {
				observe(DBAccess.getEntityManager());
			} catch (Exception e) {
				log.log(Level.SEVERE, "error while observing", e);
			}
			try {
				Thread.sleep(this.sleepTime);
			} catch (InterruptedException e) {
				log.log(Level.SEVERE, "error while pausing thread", e);
			}
		}
		thread = null;
	}
	
	private void observe(EntityManager em) throws SQLException {
			TaskDAO dao = TaskDAO.getInstance();
			List<Task> taskList = null;
			
			taskList = dao.getFailedTaskList(em);

			for (Task task : taskList) {
				notifyTaskEscalation(task);
				task.setFailed(true);
				// dao.update(udbc, task); updates are no longer needed
			}

			taskList = dao.getWarningTaskList(HOUR_SHIFT_WARNING, em);

			for (Task task : taskList) {
				notifyTaskWarning(task);
				task.setSendWarning(true);
				// dao.update(udbc, task); updates are no longer needed
			}
			
	}
	
	private void notifyTaskWarning(Task task) {
		log.info("escalation notify of task pk " + task.getPk() + ".");
		TaskType type = task.getType();
		if(type == null){
			log.log(Level.SEVERE, "TaskType is null");
		}else{
			type.getNotifier().notify(task);
		}
    }
	
	private void notifyTaskEscalation(Task task) {
        log.info("task pk " + task.getPk() + " has failed.");
	}

//	public static OctopusContext getOctopusContext() {
//		return octopusContext;
//	}

    public static Thread getThread()
    {
        return thread;
    }
}
