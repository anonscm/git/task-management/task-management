/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on Jul 18, 2007
 */

package de.tarent.wfms.processmiddleware.task.type;

import java.util.List;
import java.util.Map;

import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.data.TaskType;
import de.tarent.wfms.processmiddleware.task.MaskConnector;
import de.tarent.wfms.processmiddleware.util.GroovyScriptHelper;

/**
 * Default implementation of a TaskTypeDescriptor, executing a Groovy script
 * (selected based on taskType) to get a list of MaskConnector instances.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class GroovyTaskTypeDescriptor extends TaskTypeDescriptor
{
    @Override
    public List<MaskConnector> getMaskConnectors(TaskType taskType, Task task, Map taskData)
    {
        try
        {
            // FIXME: migrate to Groovy helper class.
            return GroovyScriptHelper.getInstance().newGroovyMaskConnectorInstance(taskType.getUri().replaceAll("\\.", "_")).getMaskConnectors(taskType, task, taskData);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Error creating groovy script instance.", e);
        }        
    }
}
