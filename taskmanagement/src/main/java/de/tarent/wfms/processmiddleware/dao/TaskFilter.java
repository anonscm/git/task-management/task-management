/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import de.tarent.commons.datahandling.ListFilterOperator;
import de.tarent.wfms.processmiddleware.util.JPAListFilter;

public class TaskFilter extends JPAListFilter {

	private static final int MAX_ENTRIES_PER_PAGE = 10;
	private static Logger log = Logger.getLogger(TaskFilter.class.getName());

	private Date startDateFrom;
	private Date startDateTo;
	private Date endDateFrom;
	private Date endDateTo;
	private Date intendedEndDateFrom;
	private Date intendedEndDateTo;
	private boolean statusOpen;
	private boolean statusClosed;
	private boolean statusDeleted;
	private String processName;
    private List<String> stafferIds;
    private List<Long> parentProcessPks;
    private List<Map<String, String>> subordinateStafferIds;
    
	private final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private String prefix;
	
	public TaskFilter(Date startDateFrom, 
                      Date startDateTo, 
                      Date endDateFrom, 
                      Date endDateTo,
                      Date intendedEndDateFrom, 
                      Date intendedEndDateTo,
                      String stafferId, 
                      boolean statusOpen, 
                      boolean statusClosed, 
                      String processName,
                      List<Long> parentProcessPks,
                      List<Map<String, String>> subordinateStafferIds) {

		this.startDateFrom = startDateFrom;
		this.startDateTo = startDateTo;
		this.endDateFrom = endDateFrom;
		this.endDateTo = endDateTo;
		this.intendedEndDateFrom = intendedEndDateFrom;
		this.intendedEndDateTo = intendedEndDateTo;
		this.statusOpen = statusOpen;
		this.statusClosed = statusClosed;
		this.statusDeleted = false;
		this.processName = processName;
        this.stafferIds = new LinkedList<String>();
        if (stafferId != null)
        	this.stafferIds.add(stafferId);
        this.parentProcessPks = parentProcessPks;
        this.subordinateStafferIds = subordinateStafferIds;
		setLimit(MAX_ENTRIES_PER_PAGE);
		setUseLimit(MAX_ENTRIES_PER_PAGE > 0);
		setSortField("intendedEndDate");
		setSortDirection(DIRECTION_ASC);
		//System.out.println(this.toString());
	}

	public TaskFilter() {
		this(null, null, null, null,null, null, null, true, false, null, null,null);
	}

	public TaskFilter(String stafferId, 
			List<Map<String, String>> subordinateStafferIds) {
		this(null, null, null, null,null, null, stafferId, true, false, null, null, subordinateStafferIds);
	}
	
	
	/** set a prefix that is added to each column name. For example if you set prefix "x" your
	 * statement will loke like "where x.column1 = 1 and x.column2 = 2"
	 * 
	 * @param prefix
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	@Override
	public List<Object> getFilterList() {  	
		String startDateFrom = this.startDateFrom == null ? null : DEFAULT_DATE_FORMAT.format(this.startDateFrom);
		String startDateTo   = this.startDateTo == null ? null : DEFAULT_DATE_FORMAT.format(alternateDay(this.startDateTo, 1));
		String endDateFrom   = this.endDateFrom == null ? null : DEFAULT_DATE_FORMAT.format(this.endDateFrom);
		String endDateTo     = this.endDateTo == null ? null : DEFAULT_DATE_FORMAT.format(alternateDay(this.endDateTo, 1));
		String intendedEndDateFrom = this.intendedEndDateFrom == null ? null : DEFAULT_DATE_FORMAT.format(alternateDay(this.intendedEndDateFrom, 1));
		String intendedEndDateTo = this.intendedEndDateTo == null ? null : DEFAULT_DATE_FORMAT.format(alternateDay(this.intendedEndDateTo, 1));
		
		List<Object> filterList = new LinkedList<Object>();
        int clauseCount = 0;
        
        // if statusDeleted is set, only deleted tasks will be shown
        if (statusDeleted) {
        	filterList.add(prefix + "." + "deleteDate");
			filterList.add(null);
			filterList.add(ListFilterOperator.NE);
            clauseCount++;
            
        }
        else if (statusOpen != statusClosed)
			if (statusOpen) { // only open tasks <=> endDate == null
				filterList.add(prefix + "." + "endDate");
				filterList.add(ListFilterOperator.IS_NULL);
				clauseCount++;
                if (endDateFrom != null || endDateTo != null) {
					log.warning("end date filter will be ignored because only open tasks are requested");
					endDateFrom = null;
					endDateTo = null;
				}
			} else { // only closed tasks <=> endDate != null
				filterList.add(prefix + "." + "endDate");
				filterList.add(null);
				filterList.add(ListFilterOperator.NE);
                clauseCount++;
			}
		else
			if (statusOpen) { // all tasks
				
			} else { // no tasks (empty set)
				filterList.add(prefix + "." + "endDate");
				filterList.add(ListFilterOperator.IS_NULL);
                clauseCount++;
				filterList.add(prefix + "." + "endDate");
				filterList.add(null);
				filterList.add(ListFilterOperator.NE);
                clauseCount++;
			}
		
		if(startDateFrom != null){
			filterList.add(prefix + "." + "startDate");
			filterList.add(startDateFrom);
			filterList.add(ListFilterOperator.GT);
            clauseCount++;
		}
		if (startDateTo != null ) {
			filterList.add(prefix + "." + "startDate");
			filterList.add(startDateTo);
			filterList.add(ListFilterOperator.LT);
            clauseCount++;
		}
		if (endDateFrom != null) {
			filterList.add(prefix + "." + "endDate");
			filterList.add(endDateFrom);
			filterList.add(ListFilterOperator.GT);
            clauseCount++;
		}
		if (endDateTo != null) {
			filterList.add(prefix + "." + "endDate");
			filterList.add(endDateTo);
			filterList.add(ListFilterOperator.LT);
            clauseCount++;
		}
		if (intendedEndDateFrom != null) {
			filterList.add(prefix + "." + "intendedEndDate");
			filterList.add(intendedEndDateFrom);
			filterList.add(ListFilterOperator.GT);
            clauseCount++;
		}
		if (intendedEndDateTo != null) {
			filterList.add(prefix + "." + "intendedEndDate");
			filterList.add(intendedEndDateTo);
			filterList.add(ListFilterOperator.LT);
            clauseCount++;
		}
		if (processName != null) 
		{
			if (!processName.equals("")) 
			{
				filterList.add(prefix + "." + "processName");
				filterList.add(processName);
				filterList.add(ListFilterOperator.LIKE);
				clauseCount++;
			}
		}
		if (stafferIds != null && stafferIds.size() > 0) {
	    	// return all tasks of the selected staffers
	    	filterList.add("staffer" +"."+ "stafferId");
	    	
	    	String idGroup = "(";
	    	for (int i = 0; i < stafferIds.size(); i++) {
	    		idGroup += "'" + stafferIds.get(i) + "'";
				if (i < stafferIds.size() - 1)
					idGroup += ", ";
			}
	    	idGroup += ") ";
	    	filterList.add(idGroup);
		    	
	    	filterList.add(ListFilterOperator.IN);
	        clauseCount++;
	    	//also show all escalated tasks of all subordinate staffers
	    	if (this.subordinateStafferIds!=null){
	        	for (Map<String, String> staffer : this.subordinateStafferIds){
	        		//for each subordinate staffer
	        		filterList.add("staffer" +"."+ "stafferId");
	            	filterList.add(staffer.get("login"));
	            	filterList.add(ListFilterOperator.EQ);
	            	//and his escalated tasks
	    			filterList.add("intendedEndDate");
	    			filterList.add(new Date());
	    			filterList.add(ListFilterOperator.LT);
	    			
	            	filterList.add(ListFilterOperator.AND);
	            	
	            	//or between all (staffer AND escalated)-clauses
	            	filterList.add(ListFilterOperator.OR);
	        	}
	    	}
            filterList.add("staffer" +"."+ "endDate");
            filterList.add(ListFilterOperator.IS_NULL);
            clauseCount++;   
	    }

        if (parentProcessPks != null) {
            filterList.add(prefix + "." + "parentProcessNode.pk");
            filterList.add(parentProcessPks);
            filterList.add(ListFilterOperator.IN);
            clauseCount++;
        }

        if (!statusDeleted) {
	        filterList.add(prefix + "." + "deleteDate");
			filterList.add(ListFilterOperator.IS_NULL);
	        clauseCount++;
        }
        
		filterList.add(prefix + "." + "failed");
		filterList.add(false);
		filterList.add(ListFilterOperator.EQ);
        clauseCount++;
        
        List superList = super.getFilterList();
        filterList.addAll(superList);
        if (superList != null && superList.size() > 0)
        	clauseCount++;
        
        // add AND operator for all above clauses
        for (int i = 0; i < clauseCount-1; i++) {
        	filterList.add(ListFilterOperator.AND);
        }
        return filterList;
	}
	
	@Override
	public String getSortField() {
		String sortField = super.getSortField();
		if (sortField == null)
			return null;
				
		if (this.prefix == null || super.getSortField().contains("."))
			return super.getSortField();
		else
			return this.prefix + "." + super.getSortField();
    }
	
	
	/** adds the given number of days to the given date
	 * 
	 * @param date
	 * @param days
	 * @return
	 */
	private Date alternateDay(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR, days);
		return cal.getTime();
	}
	

	public Date getEndDateFrom() {
		return endDateFrom;
	}

	public void setEndDateFrom(Date endDateFrom) {
		this.endDateFrom = endDateFrom;
	}

	public Date getEndDateTo() {
		return endDateTo;
	}

	public void setEndDateTo(Date endDateTo) {
		this.endDateTo = endDateTo;
	}

	public Date getStartDateFrom() {
		return startDateFrom;
	}

	public void setStartDateFrom(Date startDateFrom) {
		this.startDateFrom = startDateFrom;
	}

	public Date getStartDateTo() {
		return startDateTo;
	}

	public void setStartDateTo(Date startDateTo) {
		this.startDateTo = startDateTo;
	}

	public boolean isStatusClosed() {
		return statusClosed;
	}

	public void setStatusClosed(boolean statusClosed) {
		this.statusClosed = statusClosed;
	}
	
	public boolean isStatusDeleted() {
		return statusDeleted;
	}

	public void setStatusDeleted(boolean statusDeleted) {
		this.statusDeleted = statusDeleted;
	}

	public boolean isStatusOpen() {
		return statusOpen;
	}

	public void setStatusOpen(boolean statusOpen) {
		this.statusOpen = statusOpen;
	}
	
	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

    public List<String> getStafferIds() {
        return stafferIds;
    }

    public void setStafferIds(List<String> stafferIds) {
        this.stafferIds = stafferIds;
    }
    
    public void addStafferId(String stafferId) {
    	if (this.stafferIds == null)
    		this.stafferIds = new LinkedList<String>();
        this.stafferIds.add(stafferId);
    }

	public Date getIntendedEndDateFrom() {
		return intendedEndDateFrom;
	}

	public void setIntendedEndDateFrom(Date intendedEndDateFrom) {
		this.intendedEndDateFrom = intendedEndDateFrom;
	}

	public Date getIntendedEndDateTo() {
		return intendedEndDateTo;
	}

	public void setIntendedEndDateTo(Date intendedEndDateTo) {
		this.intendedEndDateTo = intendedEndDateTo;
	}
	
	public String toString(){
		return "Filter:" +
				"startDateFrom: "+this.getStartDateFrom()+"\n"+
				"startDateTo: "+this.getStartDateTo()+"\n"+
				"intendedEndDateFrom: "+this.getIntendedEndDateFrom()+"\n"+
				"intendedEndDateTo: "+this.getIntendedEndDateTo()+"\n"+
				"endDateFrom: "+this.getEndDateFrom()+"\n"+
				"endDateTo: "+this.getEndDateTo()+"\n"+
				"stafferId: "+this.getStafferIds()+"\n"+
				"processName: "+this.getProcessName()+"\n"+
				"statusClosed: "+this.isStatusClosed()+"\n"+
				"statusOpen: "+this.isStatusOpen();
		
	}
}
