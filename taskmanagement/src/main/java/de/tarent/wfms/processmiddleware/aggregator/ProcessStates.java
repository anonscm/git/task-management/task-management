/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

public interface ProcessStates {
	
	/*
	 * The following input states can be used in the methods
	 * {@link ProcessDetail#getState()} and 
	 * {@link ProcessDetail#setState(int)}.
	 * This values are copied from the class
	 * {@link org.activebpel.rt.bpel#IAeBusinessProcess}
	 */ 
	
	/** State to indicate that the process has been created and ready for execution. */
	public static final int PROCESS_LOADED = 0;
    /** Indicates that the process is currently executing. */
    public static final int PROCESS_RUNNING = 1;
    /** Indicates that the process is currently suspended. */
    public static final int PROCESS_SUSPENDED = 2;
    /** Process has finished executing. */
    public static final int PROCESS_COMPLETE = 3;
    /** State to indicate that the process has faulted. */
    public static final int PROCESS_FAULTED = 4;
    /** The process has completed normally and is compensatable (process instance compensation). */
    public static final int PROCESS_COMPENSATABLE = 5;
    /** Reason code for processes without a specific state reason */
    public static final int PROCESS_REASON_NONE = -1;
    /** Process is running or is loaded */
    public static final int PROCESS_RUNNING_OR_LOADED = 6;
    /** Process is completed and deleted */
    public static final int PROCESS_DELETED = 7;
    /** Pseudo State Used For JBPM Process Filtering: Process is completed or running but not loaded
     *  Not Supported By The ActiveEndpointsProcessEngine Implementation */
    public static final int PROCESS_RUNNING_OR_COMPLETED_BUT_NOT_LOADED = 8;

	/*
	 * The following input states can be used in the methods
	 * {@link ProcessFilter#setProcessState(int)} and 
	 * {@link ProcessFilter#getProcessState()}.
	 * This values are copied from the class
	 * {@link org.activebpel.rt.bpel.impl.list#AeProcessFilter}
	 */
    
    /** Used to specify filter for process state to all processes, running and completed. */
    public final static int STATE_ANY = 0;
    /** Used to specify filter for process state to only running processes. */
    public final static int STATE_RUNNING = 1;
    /** Used to specify filter for process state to only completed processes. */
    public final static int STATE_COMPLETED = 2;
    /** Used to specify filter for process state to only faulted processes. */
    public final static int STATE_FAULTED = 3;
    /** Used to specify filter for process state to completed or faulted processes. */
    public final static int STATE_COMPLETED_OR_FAULTED = 4;
    /** Used to specify filter for suspended process state. */
    public static final int STATE_SUSPENDED = 5;
    /** Used to specify filter for processes that have been suspended due to uncaught fault. */
    public static final int STATE_SUSPENDED_FAULTING = 6;
    /** Used to specify filter for processes that have been suspended due to suspend activity. */
    public static final int STATE_SUSPENDED_PROGRAMMATIC = 7;
    /** Used to specify filter for processes that have been suspended by user manually. */
    public static final int STATE_SUSPENDED_MANUAL = 8;
    /** Used to specify filter for processes that have completed their normal execution and are eligible for compensation */
    public static final int STATE_COMPENSATABLE = 9;
}
