package de.tarent.wfms.processmiddleware.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import de.tarent.wfms.processmiddleware.dao.CommentDAO;
import de.tarent.wfms.processmiddleware.dao.CommentTypeDAO;
import de.tarent.wfms.processmiddleware.data.Comment;
import de.tarent.wfms.processmiddleware.data.CommentType;
import de.tarent.wfms.processmiddleware.db.DBAccess;

public class CommentApi {

	public Comment getCommentByPk(Long commentPk) {
		return this.getCommentByPk(commentPk, DBAccess.getEntityManager());
	}
	
	/** returns a comment fetched from the database identified
	 * by its primary key
	 *  
	 * @param commentPK primary key of the comment
	 * @return the comment object belonging to the given primary key
	 */
	public Comment getCommentByPk(Long commentPk, EntityManager em) {
		
		Comment comment = null;
		if (commentPk != null) {
			CommentDAO cdao = CommentDAO.getInstance();
			comment = cdao.getCommentByPk(new Long(commentPk), em);
		}
		return comment;
	}
	
	public Comment updateCommentByPk(Long commentPk, String commentString, String stafferId){
		return this.updateCommentByPk(commentPk, commentString, stafferId, DBAccess.getEntityManager());
	}
	
	public Comment updateCommentByPk(Long commentPk, String commentString, String stafferId, EntityManager em){
		Comment comment = this.getCommentByPk(commentPk, em);
		comment.setStafferId(stafferId);
		comment.setComment(commentString);
		return comment;
	}
	
	public void deleteCommentByPk(Long commentPk){
		this.deleteCommentByPk(commentPk, DBAccess.getEntityManager());
	}
	
	public void deleteCommentByPk(Long commentPk, EntityManager em){
		
		Comment comment = this.getCommentByPk(commentPk, em);
		em.remove(comment);
	}
	
	
	public Map<String, CommentType> getCommentTypeMap() {
		return this.getCommentTypeMap(DBAccess.getEntityManager());
	}
	
	public Map<String, CommentType> getCommentTypeMap(EntityManager em) {
		
		List<CommentType> commentTypeList = CommentTypeDAO.getInstance().getAll(em);
		Map<String, CommentType> map = new HashMap<String, CommentType>();

		for (CommentType commentType : commentTypeList)
			map.put(commentType.getName(), commentType);

		return map;
	}
	
	public Map<Long, CommentType> getCommentTypePkMap() {
		return this.getCommentTypePkMap(DBAccess.getEntityManager());
	}
	
	public Map<Long, CommentType> getCommentTypePkMap(EntityManager em) {
		
		List<CommentType> commentTypeList = CommentTypeDAO.getInstance().getAll(em);
		Map<Long, CommentType> map = new HashMap<Long, CommentType>();

		for (CommentType commentType : commentTypeList)
			map.put(commentType.getPk(), commentType);

		return map;
	}

}
