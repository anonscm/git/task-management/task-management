/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.api;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.xml.rpc.JAXRPCException;

import de.tarent.wfms.processmiddleware.aggregator.AbstractCentralProcessEngineProvider;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngine;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngineException;
import de.tarent.wfms.processmiddleware.aggregator.ProcessEngineFactory;
import de.tarent.wfms.processmiddleware.config.Configuration;
import de.tarent.wfms.processmiddleware.dao.AttachmentTypeDAO;
import de.tarent.wfms.processmiddleware.dao.ProcessEventDAO;
import de.tarent.wfms.processmiddleware.dao.ProcessNodeDAO;
import de.tarent.wfms.processmiddleware.dao.TaskDAO;
import de.tarent.wfms.processmiddleware.dao.TaskFilter;
import de.tarent.wfms.processmiddleware.dao.TaskTypeDAO;
import de.tarent.wfms.processmiddleware.data.Attachment;
import de.tarent.wfms.processmiddleware.data.AttachmentType;
import de.tarent.wfms.processmiddleware.data.Comment;
import de.tarent.wfms.processmiddleware.data.CommentType;
import de.tarent.wfms.processmiddleware.data.ProcessEvent;
import de.tarent.wfms.processmiddleware.data.ProcessNode;
import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.data.TaskStaffer;
import de.tarent.wfms.processmiddleware.data.TaskType;
import de.tarent.wfms.processmiddleware.db.DBAccess;
import de.tarent.wfms.processmiddleware.task.MaskConnector;
import de.tarent.wfms.processmiddleware.task.TaskObserver;

/** This class provides methods for dealing with tasks.
 * 
 * @author Hendrik Helwich, Christian Preilowski, Martin Pelzer
 */
public class TaskApi implements ServiceConstants {

	public static SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat(
			"dd.MM.yyyy");

	private static Logger log = Logger.getLogger(TaskApi.class.getName());

	//####### TASK RELATED METHODS #######
	
	public void addTask(String engineUrl, String processName, String processCreator, Long processId, String taskId, String staffer, Map<String, Object> taskData, String typeURI, String description,
			Calendar intendedEndDate, Calendar escalationDate1, Calendar escalationDate2, String intendedEndDateInterval, String escalationDate1Interval,
			String escalationDate2Interval, String replyToUrl) {	
		Calendar cal = Calendar.getInstance();
		this.addTask(engineUrl, processName, processCreator, processId, taskId, staffer, taskData, typeURI, description, cal, intendedEndDate, escalationDate1, escalationDate2, intendedEndDateInterval, escalationDate1Interval, escalationDate2Interval, replyToUrl, DBAccess.getEntityManager());
	}
	
	/** adds a task starting now
	 * 
	 */
	public void addTask(String engineUrl, String processName, String processCreator, Long processId, String taskId, String staffer, Map<String, Object> taskData, String typeURI, String description,
			Calendar intendedEndDate, Calendar escalationDate1, Calendar escalationDate2, String intendedEndDateInterval, String escalationDate1Interval,
			String escalationDate2Interval, String replyToUrl, EntityManager em) {	
		Calendar cal = Calendar.getInstance();
		this.addTask(engineUrl, processName, processCreator, processId, taskId, staffer, taskData, typeURI, description, cal, intendedEndDate, escalationDate1, escalationDate2, intendedEndDateInterval, escalationDate1Interval, escalationDate2Interval, replyToUrl, em);
	}
	

	public void addTask(String engineUrl, String processName, String processCreator, Long processId, String taskId, String staffer, Map<String, Object> taskData, String typeURI, String description,
			Calendar startDate, Calendar intendedEndDate, String intendedEndDateInterval, String replyToUrl) {
		this.addTask(engineUrl, processName, processCreator, processId, taskId, staffer, taskData, typeURI, description, startDate, intendedEndDate, intendedEndDate, intendedEndDate, intendedEndDateInterval, intendedEndDateInterval, intendedEndDateInterval, replyToUrl, DBAccess.getEntityManager());		
	}
	
	
	/** adds a task without escalation dates
	 * 
	 */
	public void addTask(String engineUrl, String processName, String processCreator, Long processId, String taskId, String staffer, Map<String, Object> taskData, String typeURI, String description,
			Calendar startDate, Calendar intendedEndDate, String intendedEndDateInterval, String replyToUrl, EntityManager em) {	
		this.addTask(engineUrl, processName, processCreator, processId, taskId, staffer, taskData, typeURI, description, startDate, intendedEndDate, intendedEndDate, intendedEndDate, intendedEndDateInterval, intendedEndDateInterval, intendedEndDateInterval, replyToUrl, em);
	}
	
	
	/**
	 * Adds a new task to the tasklist system.
     * See config.xml#addTask for a detailed description.
	 * @throws SQLException 
	 */
	public void addTask(String engineUrl, String processName, String processCreator, Long processId, String taskId, String staffer, Map<String, Object> taskData, String typeURI, String description,
			Calendar startDate, Calendar intendedEndDate, Calendar escalationDate1, Calendar escalationDate2, String intendedEndDateInterval, String escalationDate1Interval,
			String escalationDate2Interval, String replyToUrl, EntityManager em) {

		if (AbstractCentralProcessEngineProvider.isValidEngineUrl(engineUrl)) {

			// calculate real escalation dates from (date+interval) if set
			if (intendedEndDate != null)
				intendedEndDate = calculateEscalation(intendedEndDate, intendedEndDateInterval);
			if (escalationDate1 != null && escalationDate1Interval != null)
				escalationDate1 = calculateEscalation(escalationDate1, escalationDate1Interval);
			if (escalationDate2 != null && escalationDate2Interval != null)
				escalationDate2 = calculateEscalation(escalationDate2, escalationDate2Interval);

			ProcessNodeDAO pndao = ProcessNodeDAO.getInstance();

			Task task = null;
			//EntityManager em = DBAccess.getEntityManager();
			boolean transactionStarted = true;
			try {
				em.getTransaction().begin();
			} catch (IllegalStateException e) {
				// If this library is used in an enterprise context, we may not be
				// allowed to access the transaction. If so, we believe that the enterprise
				// transaction will rollback if something happens and don't care about
				// transaction handling.
				transactionStarted = false;
			}
			try {
				// store process node and
				// recursive retrieve the complete parent process tree
				ProcessNode node = pndao.getProcessNodeByExternalProcessId(processId, engineUrl, em);
				
				if (node == null)
					node = pndao.insertProcessNode(processId, engineUrl, null, em);

				// load tasktype
				TaskTypeDAO ttdao = TaskTypeDAO.getInstance();
				TaskType type = ttdao.getTypeByUri(typeURI, em);
				if (type == null)
					throw new RuntimeException("unknown tasktype uri: " + typeURI);

				// a task must have a guid
				// check if taskid is unique
				TaskDAO tdao = TaskDAO.getInstance();
				task = tdao.getTaskById(taskId, em);
				if (task != null)
					throw new RuntimeException("existing task id: " + taskId + " (a task must have a guid)");

				// taskid is unique => save task
				task = new Task();
				task.setTaskId(taskId);
				task.setParent(node);
				task.setFkTaskType(type.getPk());
				task.setType(type);
				task.setData(taskData);
				task.setReplyToUrl(replyToUrl);
				task.setDescription(description);
				if (intendedEndDate != null)
					task.setIntendedEndDate(intendedEndDate.getTime());
				if (escalationDate1 != null)
					task.setEscalationDate1(escalationDate1.getTime());
				task.setStartDate(startDate.getTime());
				if (escalationDate2 != null)
					task.setEscalationDate2(escalationDate2.getTime());
				task.setSendWarning(false);
				task.setFailed(false);
				task.setProcessName(processName);
				task.setProcessCreator(processCreator);
				TaskStaffer tStaffer = new TaskStaffer();
				tStaffer.setStafferId(staffer);
				tStaffer.setTask(task);
				tStaffer.setStartDate(new Date());
				em.persist(tStaffer);
				task.addStaffer(tStaffer);
				
				em.persist(task);

				if (transactionStarted)
					em.getTransaction().commit();
			} catch (RuntimeException e) {
				if (transactionStarted)
					em.getTransaction().rollback();
				throw e;
			}
		} else {
			throw new RuntimeException("engineUrl is not valid: " + engineUrl);
		}
	}
	
	
	public Task getTaskByPk(Long taskPk) {
		return this.getTaskByPk(taskPk, DBAccess.getEntityManager());
	}
	
	/**
	 * returns a task fetched from the database identified by its primary key
	 * 
	 * @param taskPK
	 *            primary key of the task to fetch
	 * @return
	 */
	public Task getTaskByPk(Long taskPk, EntityManager em) {
		Task task = null;
		task = TaskDAO.getInstance().getTaskByPk(taskPk, true, em);
		if (task == null)
			log.severe(ERROR_LOAD_DATA);
		return task;
	}
	
	public Task getTaskById(String taskId) {
		return this.getTaskById(taskId, DBAccess.getEntityManager());
	}
	
	public Task getTaskById(String taskId, EntityManager em) {
		Task task = null;
		task = TaskDAO.getInstance().getTaskById(taskId, em);
		if (task == null)
			log.severe(ERROR_LOAD_DATA);
		return task;
	}
	
	
	public List<Task> getTaskListByProcess(Long processId, String engineUrl, String stafferId) {
		return this.getTaskListByProcess(processId, engineUrl, stafferId, DBAccess.getEntityManager());
	}
	
	/**
	 * Returns all tasks of the process subtree identified by the supplied
	 * process node id. It is filtered by the staffer.
	 */
	public List<Task> getTaskListByProcess(Long processId, String engineUrl, String stafferId, EntityManager em) {
		ProcessNode process = ProcessNodeDAO.getInstance().getProcessNodeByExternalProcessId(processId, engineUrl, em);
		
		TaskDAO tdao = TaskDAO.getInstance();

		List<Long> processPKs = ProcessNodeDAO.getInstance()
				.getProcessSubtreePKs(process, em);
		TaskFilter filter = new TaskFilter(null, null, null, null, null, null,
				stafferId, true, true, null, processPKs, null);
		return tdao.getAllJoinStaffer(filter, em);

	}
	
	
	public long getTaskCount(long taskTypeId) {
		return this.getTaskCount(taskTypeId, DBAccess.getEntityManager());
	}
	
	/** returns the number of tasks of a given task type
	 * 
	 * @param taskTypeId
	 * @return
	 */
	public long getTaskCount(long taskTypeId, EntityManager em) {
		if (taskTypeId != -1)
			return TaskDAO.getInstance().countTasks(taskTypeId, em);
		else
			return TaskDAO.getInstance().countTasks(em);
	}
	
	
	public long getTaskCount(TaskFilter filter) {
		return this.getTaskCount(filter, DBAccess.getEntityManager());
	}
	
	/** returns the number of tasks of a given task type and for the given filter
	 * 
	 * @param taskTypeId
	 * @return
	 */
	public long getTaskCount(TaskFilter filter, EntityManager em) {
		return TaskDAO.getInstance().countAllJoinStaffer(filter, em);
	}
	
	
	public List<Task> getTaskList(TaskFilter filter) {
		return this.getTaskList(filter, DBAccess.getEntityManager());
	}
	
	/** returns a list of all tasks that match the given filter
	 * 
	 * @param filter a filter which can be used to control the contents of the resulting list
	 */
	public List<Task> getTaskList(TaskFilter filter, EntityManager em) {

		TaskDAO tdao = TaskDAO.getInstance();
		// TODO We join over staffer and parent because the filter COULD contain
		// filter params for these entities. This is not the perfect solution because
		// in many because one or both joins are useless.
		List<Task> taskList = tdao.getAllJoinStaffer(filter, em);
		
		return taskList;
	}
	
	
	public void removeProcessTasks(String engineUrl, Long processId){
		this.removeProcessTasks(engineUrl, processId, DBAccess.getEntityManager());
	}
	
	/**
	 * Removes all tasks which are associated to the specified process.
	 */
	public void removeProcessTasks(String engineUrl, Long processId, EntityManager em){
		TaskDAO.getInstance().deleteTasksByProcess(engineUrl, processId, em);
	}
	
	//############### TASK ACTIONS ######################
	
	public void forwardTask(Long taskPk, String stafferName){
		this.forwardTask(taskPk, stafferName, DBAccess.getEntityManager());
	}
	
	
	public void forwardTask(Long taskPk, String stafferName, EntityManager em) {
		Task task = this.getTaskByPk(taskPk);
		this.forwardTask(task, stafferName, em);
	}
	
	public void forwardTask(String taskId, String stafferName){
		this.forwardTask(taskId, stafferName, DBAccess.getEntityManager());
	}
	
	public void forwardTask(String taskId, String stafferName, EntityManager em){
		Task task = this.getTaskById(taskId, em);
		this.forwardTask(task, stafferName, em);
	}
	
	private void forwardTask(Task task, String stafferName){
		this.forwardTask(task, stafferName, DBAccess.getEntityManager());
	}
	
	private void forwardTask(Task task, String stafferName, EntityManager em){
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			boolean transactionStarted = true;
			try {
				em.getTransaction().begin();
			} catch (IllegalStateException e) {
				// If this library is used in an enterprise context, we may not be
				// allowed to access the transaction. If so, we believe that the enterprise
				// transaction will rollback if something happens and don't care about
				// transaction handling.
				transactionStarted = false;
			}
			
			//create new Staffer
			TaskStaffer staffer = new TaskStaffer();
			staffer.setStafferId(stafferName);
			staffer.setStartDate(new Date());
			staffer.setTask(task);
			
			//set Enddate to actual staffer
			task.getActualStaffer().setEndDate(new Date());
			
			//set Staffer
			em.persist(staffer);
			task.addStaffer(staffer);
			
			if (transactionStarted)
				em.getTransaction().commit();
		}
	}
	
	
	public void resubmitTask(Long taskPk, Date resubmitDate) {
		this.resubmitTask(taskPk, resubmitDate, DBAccess.getEntityManager());
	}
	
	public void resubmitTask(Long taskPk, Date resubmitDate, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		this.resubmitTask(task, resubmitDate);
	}
	
	
	public void resubmitTask(String taskId, Date resubmitDate) {
		this.resubmitTask(taskId, resubmitDate, DBAccess.getEntityManager());
	}
	
	public void resubmitTask(String taskId, Date resubmitDate, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		this.resubmitTask(task, resubmitDate);
	}
	
	/** sets the start date of a task to a later date (has to be before end date!) so that
	 * the task will show up later in the task list (will start to show up at start date) and not now.
	 * 
	 * @param taskPk
	 * @param resubmitDate
	 */
	private void resubmitTask(Task task, Date resubmitDate) {
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			//resubmission only for open tasks
			if (task.getEndDate()!=null){
				log.info("task ("+task.getTaskId()+") already closed - no resubmission");
				return;
			}
			//resubmission only for dates in future
			if (resubmitDate.before(new Date())){
				log.info("resubmit date ("+resubmitDate.getTime()+") must be in future - no resubmission");
				return;
			}
			//resubmit date must be between startDate and intended endDate
			if (resubmitDate.after(task.getStartDate()) && resubmitDate.before(task.getIntendedEndDate())){
				task.setStartDate(resubmitDate);
			}else{
				log.info("resubmit date ("+resubmitDate.getTime()+") must be between start and enddate - no resubmission");
			}
		}
	}

	
	public void cancelTask(Long taskPk, String stafferName){
		this.cancelTask(taskPk, stafferName, DBAccess.getEntityManager());
	}
	
	public void cancelTask(Long taskPk, String stafferName, EntityManager em){
		Task task = this.getTaskByPk(taskPk, em);
		this.cancelTask(task, stafferName, em);
	}
	
	public void cancelTask(String taskId, String stafferName){
		this.cancelTask(taskId, stafferName, DBAccess.getEntityManager());
	}
	
	public void cancelTask(String taskId, String stafferName, EntityManager em){
		Task task = this.getTaskById(taskId, em);
		this.cancelTask(task, stafferName, em);
	}	
	
	private void cancelTask(Task task, String stafferName, EntityManager em){
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			// we set the delete date in the task so 
			// that it will be closed but no callback
			// will be made

			task.setDeleteDate(new Date());

			// log
			ProcessEventDAO.getInstance().log(ProcessEvent.TYPE_TASK_COMPLETED, task.getParent().getProcessId(), "Aufgabe \"" + task.getDescription() + "\" wurde abgebrochen.", stafferName, em);
		}
	}
	
	
	public void closeTask(Long taskPk, Map<String,Object> returnData, String commentText, String staffer) {
		this.closeTask(taskPk, returnData, commentText, staffer, DBAccess.getEntityManager());
	}
	
	public void closeTask(Long taskPk, Map<String,Object> returnData, String commentText, String staffer, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		this.closeTask(task, returnData, commentText, staffer, em);
	}

	public void closeTask(String taskId, Map<String,Object> returnData, String commentText, String staffer) {
		this.closeTask(taskId, returnData, commentText, staffer, DBAccess.getEntityManager());
	}
	
	public void closeTask(String taskId, Map<String,Object> returnData, String commentText, String staffer, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		this.closeTask(task, returnData, commentText, staffer, em);
	}
	
	private void closeTask(Task task, Map<String,Object> returnData, String commentText, String staffer, EntityManager em) {
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			// must not close already closed a/o deleted tasks, cf. issue #500
			if (task.isClosed() || task.isDeleted())
			{
				return;
			}

			boolean transactionStarted = true;
			try {
				em.getTransaction().begin();
			} catch (IllegalStateException e) {
				// If this library is used in an enterprise context, we may not be
				// allowed to access the transaction. If so, we believe that the enterprise
				// transaction will rollback if something happens and don't care about
				// transaction handling.
				transactionStarted = false;
			}
			
			// add closingComment to returnData
			if (returnData == null)
				returnData = new HashMap<String, Object>();
			if (commentText != null)
				returnData.put("closeComment", commentText);
			returnData.put("closeDate", new Date());
			returnData.put("taskId", task.getTaskId());
			returnData.put("staffer", staffer);
	
			// log
			ProcessEventDAO.getInstance().log(ProcessEvent.TYPE_TASK_COMPLETED, task.getParent().getProcessId(), "Aufgabe \"" + task.getDescription() + "\" wurde beendet.", staffer, em);
			em.flush();
			
			try {
				ProcessApi s = new ProcessApi();
				String engineUrl = s.getProcessByPk(task.getParent().getProcessId(), em).getEngineUrl();
				
				task.setEndDate(GregorianCalendar.getInstance(
						Locale.getDefault()).getTime());
				this.doCallback(engineUrl, task, returnData);
			} catch (ProcessEngineException e) {
				log.severe(ERROR_IN_PROCESS_ENGINE);
				log.log(Level.SEVERE, ERROR_IN_PROCESS_ENGINE, e);
			} catch (JAXRPCException e) {
				log.severe(ERROR_IN_PROCESS_ENGINE);
				log.log(Level.SEVERE, ERROR_IN_PROCESS_ENGINE, e);
			}
			
			if (transactionStarted)
				em.getTransaction().commit();

		}
	}

	// #########  COMMENT RELATED METHODS ############ 

	public void addCommentToTask(Long taskPk, String commentString, String stafferId, String commentTypeName) {
		this.addCommentToTask(taskPk, commentString, stafferId, commentTypeName, DBAccess.getEntityManager());
	}
	
	public void addCommentToTask(Long taskPk, String commentString, String stafferId, String commentTypeName, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		this.addCommentToTask(task, commentString, stafferId, commentTypeName);
	}
	
	public void addCommentToTask(String taskId, String commentString, String stafferId, String commentTypeName) {
		this.addCommentToTask(taskId, commentString, stafferId, commentTypeName, DBAccess.getEntityManager());
	}
	
	public void addCommentToTask(String taskId, String commentString, String stafferId, String commentTypeName, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		this.addCommentToTask(task, commentString, stafferId, commentTypeName);
	}
	
	private void addCommentToTask(Task task, String commentString, String stafferId, String commentTypeName) {
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			//get all commentTypes
			Map<String, CommentType> commentTypeMap = new CommentApi().getCommentTypeMap();
			CommentType commentType = commentTypeMap.get(commentTypeName);
			if (commentType==null){
				log.info("no comment type found with name "+commentTypeName);
			}
			
			//create Comment
			Comment comment = new Comment();
			comment.setComment(commentString);
			comment.setStafferId(stafferId);
			//comment.setFkParentProcessNode(task.getParent().getPk());
			//comment.setFkRootProcessNode(task.getParent().getPk());
			comment.setCommentType(commentType);
			
			
			//add Comment to task
			task.addComment(comment);
		}
	}

	
	public List<Comment> getCommentListByTask(Long taskPk) {
		return this.getCommentListByTask(taskPk, DBAccess.getEntityManager());
	}
	
	public List<Comment> getCommentListByTask(Long taskPk, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		return this.getCommentListByTask(task);
	}
	
	public List<Comment> getCommentListByTask(String taskId) {
		return this.getCommentListByTask(taskId, DBAccess.getEntityManager());
	}
	
	public List<Comment> getCommentListByTask(String taskId, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		return this.getCommentListByTask(task);
	}
	
	private List<Comment> getCommentListByTask(Task task) {
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			return task.getCommentList();
		}

		return new ArrayList< Comment >();
	}
	
	// #########  ATTACHMENT RELATED METHODS ############ 
	
	public void addAttachmentToTask(Long taskPk, String title,
			String attachmentMimeType, byte[] document, int priority,
			String stafferId){
		this.addAttachmentToTask(taskPk, title, attachmentMimeType, document, priority, stafferId, DBAccess.getEntityManager());
	}
	
	public void addAttachmentToTask(Long taskPk, String title,
			String attachmentMimeType, byte[] document, int priority,
			String stafferId, EntityManager em){
		Task task = this.getTaskByPk(taskPk, em);
		this.addAttachmentToTask(task, title, attachmentMimeType, document, priority, stafferId, em);
	}
	
	public void addAttachmentToTask(String taskId, String title,
			String attachmentMimeType, byte[] document, int priority,
			String stafferId){
		this.addAttachmentToTask(taskId, title, attachmentMimeType, document, priority, stafferId, DBAccess.getEntityManager());
	}
	
	public void addAttachmentToTask(String taskId, String title,
			String attachmentMimeType, byte[] document, int priority,
			String stafferId, EntityManager em){
		Task task = this.getTaskById(taskId, em);
		this.addAttachmentToTask(task, title, attachmentMimeType, document, priority, stafferId, em);
	}
	
	private void addAttachmentToTask(Task task, String title,
			String attachmentMimeType, byte[] document, int priority,
			String stafferId, EntityManager em){
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			//select attachment type
			AttachmentType type = AttachmentTypeDAO.getInstance().getOrCreateType(
					attachmentMimeType, em);
	
			try {
				//store file
				String pdmsId =	new AttachmentApi().storeFile(task.getParent(), priority, document);
	
				//create attachment object
				Attachment attachment = new Attachment();
				attachment.setPdmsId(pdmsId);
				attachment.setStafferId(stafferId);
				//attachment.setFkAttachmentType(type.getPk());
				attachment.setAttachmentType(type);
				attachment.setFkTask(task.getPk());
				attachment.setTitle(title);
	
				boolean commit = false;
				try {
					if (!em.getTransaction().isActive()) {
						em.getTransaction().begin();
						commit = true;
					}
				} catch (IllegalStateException e) {
					// If this library is used in an enterprise context, we may not be
					// allowed to access the transaction. If so, we believe that the enterprise
					// transaction will rollback if something happens and don't care about
					// transaction handling.
					commit = false;
				}
				em.persist(attachment);
	
				task.addAttachment(attachment);
				if (commit)
					em.getTransaction().commit();
				
			} catch (IOException e) {
				log.severe("attachment could not be saved: "+e.getLocalizedMessage());
				throw new RuntimeException(e);
			}
		}
	}
	
	
	public List<Attachment> getAttachmentListByTask(Long taskPk) {
		return this.getAttachmentListByTask(taskPk, DBAccess.getEntityManager());
	}
	
	public List<Attachment> getAttachmentListByTask(Long taskPk, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		return this.getAttachmentListByTask(task);
	}
	
	public List<Attachment> getAttachmentListByTask(String taskId) {
		return this.getAttachmentListByTask(taskId, DBAccess.getEntityManager());
	}
	
	public List<Attachment> getAttachmentListByTask(String taskId, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		return this.getAttachmentListByTask(task);
	}
	
	private List<Attachment> getAttachmentListByTask(Task task) {		
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			return  task.getAttachmentList();
		}

		return new ArrayList< Attachment >();
	}


	//############### TASKDATA RELATED METHODS #################

	public Object getTaskDataByKey(Long taskPk, String key) {
		return this.getTaskDataByKey(taskPk, key, DBAccess.getEntityManager());
	}
	
	public Object getTaskDataByKey(Long taskPk, String key, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		return this.getTaskDataByKey(task, key);
	}
	
	public Object getTaskDataByKey(String taskId, String key) {
		return this.getTaskDataByKey(taskId, key, DBAccess.getEntityManager());
	}
	
	public Object getTaskDataByKey(String taskId, String key, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		return this.getTaskDataByKey(task, key);
	}
	
	/**
	 * Gets the object with the given key from taskdata.
	 * 
	 * @param oc
	 *            the octopus context
	 * @param taskId
	 *            the task id
	 * @param key
	 *            the key
	 * 
	 * @return the task data
	 */
	private Object getTaskDataByKey(Task task, String key) {
		if (task == null) {
			return null;
		}
		Map<String,Object> taskData = task.getData();

		return taskData.get(key);
	}

	
	public void addToTaskData(Long taskPk, String key, Object value) {
		this.addToTaskData(taskPk, key, value, DBAccess.getEntityManager());
	}
	
	public void addToTaskData(Long taskPk, String key, Object value, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		this.addToTaskData(task, key, value);
	}
	
	public void addToTaskData(String taskId, String key, Object value) {
		this.addToTaskData(taskId, key, value, DBAccess.getEntityManager());
	}
	
	public void addToTaskData(String taskId, String key, Object value, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		this.addToTaskData(task, key, value);
	}
	
	/**
	 * Adds the Object value with the given key to the taskdata map.
	 * 
	 * @param oc
	 *            the octopus context
	 * @param taskId
	 *            the task id
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 * 
	 */
	private void addToTaskData(Task task, String key, Object value) {		
		Map<String,Object> taskData = task.getData();
		if (taskData == null) {
			taskData = new HashMap<String, Object>();
		}
		
		taskData.put(key, value);
		task.setData(taskData);

	}
	
	
	public void addToTaskData(Long taskPk, Map<String,Object> newTaskData) {
		this.addToTaskData(taskPk, newTaskData, DBAccess.getEntityManager());
	}
	
	public void addToTaskData(Long taskPk, Map<String,Object> newTaskData, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		this.addToTaskData(task, newTaskData);
	}
	
	public void addToTaskData(String taskId, Map<String,Object> newTaskData) {
		this.addToTaskData(taskId, newTaskData, DBAccess.getEntityManager());
	}
	
	public void addToTaskData(String taskId, Map<String,Object> newTaskData, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		this.addToTaskData(task, newTaskData);
	}
	
	/**
	 * updates task data. for all entries of the given map it is checked if they
	 * are already there. If so, they are updated. If not, they are inserted.
	 * 
	 * @param taskId
	 *            id of the task to update
	 * @param taskData
	 *            the data to update
	 */
	private void addToTaskData(Task task, Map<String,Object> newTaskData) {
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			Map<String,Object> taskData = task.getData();
	
			// update task data: for each key in new task data
			for (Object key : newTaskData.keySet()) {
				taskData.put((String) key, newTaskData.get(key));
			}
			
			task.setData(taskData);
		}
	}
	
	
	public void deleteTaskDataByKey(Long taskPk, String key) {
		this.deleteTaskDataByKey(taskPk, key, DBAccess.getEntityManager());
	}
	
	public void deleteTaskDataByKey(Long taskPk, String key, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		this.deleteTaskDataByKey(task, key);
	}
	
	public void deleteTaskDataByKey(String taskId, String key) {
		this.deleteTaskDataByKey(taskId, key, DBAccess.getEntityManager());
	}
	
	public void deleteTaskDataByKey(String taskId, String key, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		this.deleteTaskDataByKey(task, key);
	}

	/** deletes the data variable with the given name (key) from
	 * the given task
	 * 
	 * @param taskId
	 * @param key
	 */
	private void deleteTaskDataByKey(Task task, String key) {
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			Map<String,Object> taskData = task.getData();
			taskData.remove(key);
			task.setData(taskData);
		}
	}
	
	// ############# MISC METHODS ############
	
	public List<MaskConnector> getMasks(Long taskPk, Map<Integer, MaskConnector> masks) {
		return this.getMasks(taskPk, masks, DBAccess.getEntityManager());
	}
	
	public List<MaskConnector> getMasks(Long taskPk, Map<Integer, MaskConnector> masks, EntityManager em) {
		Task task = this.getTaskByPk(taskPk, em);
		return this.getMasks(task, masks);
	}
	
	public List<MaskConnector> getMasks(String taskId, Map<Integer, MaskConnector> masks) {
		return this.getMasks(taskId, masks, DBAccess.getEntityManager());
	}
	
	public List<MaskConnector> getMasks(String taskId, Map<Integer, MaskConnector> masks, EntityManager em) {
		Task task = this.getTaskById(taskId, em);
		return this.getMasks(task, masks);
	}

	private List<MaskConnector> getMasks(Task task,
			Map<Integer, MaskConnector> masks) {
		// task must not be null
		// this can happen due to inconsistencies of the data in our task table
		// and the processing engine's own task table, perhaps the user deleted
		// older tupels from our table and left the jbpm table intact
		if ( task != null )
		{
			TaskType taskType = task.getType();
	
			// Map<Integer, MaskConnector> masks = (Map)oc.sessionAsObject("masks");
			if (masks == null) {
				masks = new HashMap<Integer, MaskConnector>();
			}
	
			List<MaskConnector> maskList = taskType.getTaskTypeDescriptor()
					.getMaskConnectors(task.getType(), task, task.getData());
			for (int i = 0; i < maskList.size(); i++)
				masks.put(maskList.get(i).hashCode(), maskList.get(i));
	
			return maskList;
		}

		return new ArrayList< MaskConnector >();
	}
	
	
	public void startTaskObserver() {
		// make sure the TaskObserver shuts down on server shutdown
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				TaskObserver.stop();
				TaskObserver.getThread().interrupt();
			}
		});

		TaskObserver.start();
	}

	public void stopTaskObserver() {
		TaskObserver.stop();
	}
	
	private void doCallback(String engineUrl, Task task,
			Map<String, Object> returnData) throws ProcessEngineException {
		ProcessEngine engine = ProcessEngineFactory.getProcessEngine(engineUrl);
		engine.doCallback(task, returnData);
	}
	
	public TaskType getTaskTypByUri(String uri) {
		return this.getTaskTypByUri(uri, DBAccess.getEntityManager());
	}
	
	public TaskType getTaskTypByUri(String uri, EntityManager em) {
		return TaskTypeDAO.getInstance().getTypeByUri(uri, em);
	}
	
	
	
	private Calendar calculateEscalation(Calendar givenDate, String interval) {

        // This code is duplicated from UtilWorker to keep class distance reasonable!
        if (givenDate.getTimeInMillis()==0)
            return null;

        if (givenDate==null)
            givenDate = new GregorianCalendar();

        if (interval!=null && !interval.equals(""))
        {
            int unit = Calendar.SECOND;
            int value = 0;
    
            if (interval.startsWith("+"))
                interval = interval.substring(1);
            
            if (Character.isLetter(interval.charAt(interval.length()-1)))
            {
                switch (interval.charAt(interval.length()-1))
                {
                    case 'h': unit = Calendar.HOUR_OF_DAY; break;
                    case 'd': unit = Calendar.DAY_OF_MONTH; break;
                    case 'w': unit = Calendar.WEEK_OF_YEAR; break;
                    case 'm': unit = Calendar.MONTH; break;
                    case 'y': unit = Calendar.YEAR; break;
                }
                value = Integer.parseInt(interval.substring(0, interval.length()-1));            
            }
            else
                value = Integer.parseInt(interval); 
            
            // if in debugMode, unit is always minutes
            if (Configuration.getInstance().getParameterAsBoolean("debugMode"))
                unit = Calendar.MINUTE;
            
            if (unit != Calendar.DAY_OF_MONTH)
                givenDate.add(unit, value);
            else
            {
                int signum = Integer.signum(value);
                value = Math.abs(value);
                while (value>0)
                {
                    if (givenDate.get(Calendar.DAY_OF_WEEK)!=Calendar.FRIDAY && givenDate.get(Calendar.DAY_OF_WEEK)!=Calendar.SATURDAY)
                        value--;
    
                    givenDate.add(Calendar.DAY_OF_MONTH, signum);
                }
            }
        }
        
        return givenDate;
    }

}