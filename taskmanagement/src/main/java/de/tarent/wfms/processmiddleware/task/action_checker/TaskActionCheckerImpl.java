/**
 * 
 */
package de.tarent.wfms.processmiddleware.task.action_checker;

import de.tarent.wfms.processmiddleware.data.Task;

/**
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
public abstract class TaskActionCheckerImpl implements TaskActionChecker {
	
	private Task task;
	
	/**
	 * Instantiates a new task action checker impl.
	 * 
	 * @param task the task
	 */
	public TaskActionCheckerImpl(Task task){
		this.setTask(task);
	}


	/**
	 * Gets the task.
	 * 
	 * @return the task
	 */
	public Task getTask() {
		return task;
	}

	/**
	 * Sets the task.
	 * 
	 * @param task the new task
	 */
	private void setTask(Task task) {
		this.task = task;
	}

}
