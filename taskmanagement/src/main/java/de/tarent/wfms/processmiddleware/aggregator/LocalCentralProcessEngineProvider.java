/*
 * tarent task management library,
 * Middleware which handles tasks and processes
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent task management library'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package de.tarent.wfms.processmiddleware.aggregator;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 * The class LocalCentralProcessEngineProvider models a singleton
 * provider that is capable of servicing requests for local processing
 * engine instances.
 * 
 * @author Hendrik Helwich, h.helwich@tarent.de
 * @author Carsten Klein
 */
public class LocalCentralProcessEngineProvider
	extends AbstractCentralProcessEngineProvider
{
	private static final Logger log = Logger.getLogger( LocalCentralProcessEngineProvider.class.getName() );

	public LocalCentralProcessEngineProvider()
	{
		super();
	}

	@Override
	public void initialize( String resourceBundleName )
	{
		super.initialize( this, resourceBundleName );
	}

	/**
	 * 
	 * 
	 * @return
	 */
	@Override
	public List< ProcessEngine > getProcessEngineList()
	{
		List<ProcessEngine> engines = null;

		// get process engines from local configuration file
		engines = new LinkedList< ProcessEngine >();

		try
		{
			JAXBContext context = JAXBContext.newInstance( Engines.class );
			File file = new File("/etc/taskmgmt/engines.xml");
			
			Unmarshaller u = context.createUnmarshaller();
			Engines e = ( Engines ) u.unmarshal( file );

			List< String > identifiers = e.getIdentifiers();
			for( String identifier : identifiers )
			{
				ProcessEngine engine = new JbpmProcessEngine( identifier );
				engines.add( engine );
			}
		}
		catch( Exception e )
		{
			log.log( Level.WARNING, "engines.xml not found or not parseable; no process engines were registered", e);
		}

		return engines;
	}
}
