package org.evolvis.middleware;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.namespace.QName;

import org.jbpm.graph.exe.ProcessInstance;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.tarent.wfms.processmiddleware.aggregator.ProcessStates;
import de.tarent.wfms.processmiddleware.aggregator.WorkerProcessFilter;
import de.tarent.wfms.processmiddleware.api.ProcessApi;
import de.tarent.wfms.processmiddleware.api.TaskApi;
import de.tarent.wfms.processmiddleware.api.UtilApi;
import de.tarent.wfms.processmiddleware.data.Attachment;
import de.tarent.wfms.processmiddleware.data.Comment;
import de.tarent.wfms.processmiddleware.data.CommentType;
import de.tarent.wfms.processmiddleware.data.ProcessEvent;
import de.tarent.wfms.processmiddleware.data.ProcessNode;
import de.tarent.wfms.processmiddleware.data.Task;

/** test class for class ProcessService in process middleware
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class ProcessApiTest {

	private static final String engineUrl = "jbpmPiA";
	
	private static ProcessApi processService;
	private static TaskApi taskService;
	private static UtilApi utilService;
	
	private static long processId;
	private static long processPk;
	
	// ------------------------ set up and tear down ------------------------
	
	@BeforeClass
	public static void setUpTestClass() {
		JbpmHelper.createSchema();
		
		// initialize service classes of process middleware
		taskService = new TaskApi();
		processService = new ProcessApi();
		utilService = new UtilApi();
		
		utilService.addTaskType(Helper.createTaskType());
		
		// deploy a process in JBPM so we can access one
		JbpmHelper.deployProcessDefinition(JbpmHelper.FIRST_DEFINITION);
		
		// start a process instance
		processId = JbpmHelper.startProcessInstance(JbpmHelper.FIRST_DEFINITION);
	}
	
	
	@AfterClass
	public static void tearDownTestClass() {
		JbpmHelper.dropSchema();
	}
	
	
	@Before
	public void setUp() {
		
	}


	@After
	public void tearDown() {
	
	}
	
	
	// ------------------------ tests ------------------------
	
	
	@Test
	public void testGetProcessByExternalId() {
		// get the already started process instance and check if it is the right one
		ProcessNode process = processService.getProcessByExternalID(engineUrl, processId);
		
		assertTrue(process != null);
		assertTrue(process.getProcessId() == processId);
	}
	
	@Test
	public void testGetProcessByPk() {
		// get process by external id, get pk from this object, get process
		// again by pk and compare
		ProcessNode processById = processService.getProcessByExternalID(engineUrl, processId);
		
		processPk = processById.getPk(); // for later use
		
		ProcessNode processByPk = processService.getProcessByPk(processById.getPk());
		
		assertTrue(processByPk.getPk() == processById.getPk());
		assertTrue(processByPk.getProcessId() == processById.getProcessId());
	}
	
	@Test
	public void testForwardProcess() {
		// forward process to "Jeffrey Lebowski", fetch it from process middleware and
		// check if staffer is correct
		processService.forwardProcess(processPk, "Jeffrey Lebowski");
		
		ProcessNode process = processService.getProcessByPk(processPk);
		
		// check that process owner is now "Jeffrey Lebowski"
		assertTrue(process.getDetail().getResponsibleStafferId().equals("Jeffrey Lebowski"));
	}
	
	@Test
	public void testCloseProcess() {
		processService.closeProcess(processPk);
		
		// check that end date is set and that process state is PROCESS_COMPLETE
		ProcessNode process = processService.getProcessByPk(processPk);
		assertTrue(process.getDetail().getEnded() != null);
		assertTrue(process.getDetail().getState() == ProcessStates.PROCESS_COMPLETE);
	}
	
	@Test
	public void testGetProcessCount() {
		// We start a second process instance so that the count method has something
		// more to count than just one instance.
		long myProcessId = JbpmHelper.startProcessInstance(JbpmHelper.FIRST_DEFINITION);
		
		long count = processService.getProcessCount("hello world"); // "hello world" is the name of the process definition
		
		// count has to be two because we started two process
		// instances until now
		assertTrue(count == 2);
	}
	
	@Test
	public void testGetProcessList() {
		// define a few processes with different create and complete dates, process
		// definitions and states
		
		// first we define some timestamps (all one year in the future)
		Calendar now = Calendar.getInstance();
		now.add(Calendar.YEAR, 1);
		Calendar inOneMonth = Calendar.getInstance();
		inOneMonth.add(Calendar.YEAR, 1);
		inOneMonth.add(Calendar.MONTH, 1);
		Calendar inTwoMonth = Calendar.getInstance();
		inTwoMonth.add(Calendar.YEAR, 1);
		inTwoMonth.add(Calendar.MONTH, 2);
		
		// then we deploy a second process definition
		JbpmHelper.deployProcessDefinition(JbpmHelper.SECOND_DEFINITION);
		
		// now let's start some processes
		long one = JbpmHelper.startProcessInstance(JbpmHelper.FIRST_DEFINITION, now, null, false);
		long two = JbpmHelper.startProcessInstance(JbpmHelper.SECOND_DEFINITION, now, inOneMonth, false);
		long three = JbpmHelper.startProcessInstance(JbpmHelper.FIRST_DEFINITION, inOneMonth, null, false);
		long four = JbpmHelper.startProcessInstance(JbpmHelper.FIRST_DEFINITION, now, inTwoMonth, true);
		
		// We fetch the processes from process middleware so that they will be inserted (strange but true).
		ProcessNode nodeOne = processService.getProcessByExternalID(engineUrl, one);
		ProcessNode nodeTwo = processService.getProcessByExternalID(engineUrl, two);
		ProcessNode nodeThree = processService.getProcessByExternalID(engineUrl, three);
		ProcessNode nodeFour = processService.getProcessByExternalID(engineUrl, four);
		
		// All these processes generate a task that is inserted into the process middleware so
		// that these processes are also known there.
		
		
		// get all processes created between now and inOneMonth (should be 3)
		WorkerProcessFilter filter = new WorkerProcessFilter();
		filter.setProcessCreateStart(now);
		filter.setProcessCreateEnd(inOneMonth);
		filter.setProcessState(-1);
		List<ProcessNode> processes = processService.getProcessList(filter);
		assertTrue(processes.size() == 3);
		
		// get all processes that end before inTowMonth (should be 1)
		filter = new WorkerProcessFilter();
		filter.setProcessCreateStart(now); // added so that not other processes of other tests are found
		filter.setProcessCompleteEnd(inTwoMonth);
		filter.setProcessState(-1);
		processes = processService.getProcessList(filter);
		assertTrue(processes.size() == 1);
		
		// get all processes that are suspended (should be 1)
		filter = new WorkerProcessFilter();
		filter.setProcessCreateStart(now); // added so that not other processes of other tests are found
		filter.setProcessState(ProcessStates.PROCESS_SUSPENDED);
		processes = processService.getProcessList(filter);
		assertTrue(processes.size() == 1);
		
		// get all processes in our timeframe that belong to definition 1 (should be 3)
		filter = new WorkerProcessFilter();
		filter.setProcessCreateStart(now); // added so that not other processes of other tests are found
		filter.setProcessName(new QName(JbpmHelper.FIRST_DEFINITION));
		filter.setProcessState(-1);
		processes = processService.getProcessList(filter);
		assertTrue(processes.size() == 3);	
	}
	
	@Test
	public void testGetLogForProcess() {
		// do some actions that will be loggend (starting, forwarding, stopping)
		long myProcessId = JbpmHelper.startProcessInstance(JbpmHelper.FIRST_DEFINITION);
		
		// add log event for starting process to process middleware
		ProcessEvent event = new ProcessEvent(ProcessEvent.TYPE_PROCESS_STARTED);
		event.setDate(new Date());
		event.setDescription("process started");
		event.setProcessId(myProcessId);
		utilService.addExternalLogEvent(event);
		
		ProcessNode process = processService.getProcessByExternalID(engineUrl, myProcessId);
		processService.forwardProcess(process.getPk(), "Hula Kavula");
		processService.closeProcess(process.getPk());
		
		// check if the log is filled with 3 entries
		List<ProcessEvent> log = processService.getLogForProcess(process.getProcessId());
		assertTrue(log.size() == 3);
		assertTrue(log.get(0).getProcessId() == process.getProcessId());
		assertTrue(log.get(1).getProcessId() == process.getProcessId());
		assertTrue(log.get(2).getProcessId() == process.getProcessId());
	}
	
	@Test
	public void testAddCommentToProcess() {
		// first add a comment type to database
		CommentType ct = new CommentType();
		ct.setName("nameOfCommentType");
		ct.setShortcut("shortCutOfCommentType");
		utilService.addCommentType(ct);
		
		// now add a comment to a process
		processService.addCommentToProcess(processPk, "The dude abides.", "Jeffrey Lebowski", ct.getName());
		
		// fetch process from database and check if comment was added
		ProcessNode process = processService.getProcessByPk(processPk);
		assertTrue(process.getCommentList().size() == 1);
		assertTrue(process.getCommentList().get(0).getComment().equals("The dude abides."));
		assertTrue(process.getCommentList().get(0).getStafferId().equals("Jeffrey Lebowski"));
		assertTrue(process.getCommentList().get(0).getCommentTypeName().equals(ct.getName()));
	}
	
	@Test
	public void testGetCommentListByProcess() {
		List<Comment> comments = processService.getCommentListByProcess(processPk);
		
		// check size of comment list and that the added comment is in the list
		assertTrue(comments.size() == 1);
		assertTrue(comments.get(0).getComment().equals("The dude abides."));
		assertTrue(comments.get(0).getStafferId().equals("Jeffrey Lebowski"));
	}
	
	@Test
	public void testGetCommentListByProcessWholeProcess() {
		// deploy complex process definition
		JbpmHelper.deployComplexProcessDefinition(JbpmHelper.COMPLEX_DEFINITION);
		
		// start complex process
		long complexProcessId = JbpmHelper.startProcessInstance(JbpmHelper.COMPLEX_DEFINITION);
		
		// check that sub process has been started.
		// (We know that the sub process will have process id 9 due to the beautiful world of unit testing.)
		ProcessInstance subProcess = JbpmHelper.getProcessInstance(complexProcessId + 1);
		assertTrue(subProcess != null);
		
		// make sure that both processes will be in process middleware
		ProcessNode father = processService.getProcessByExternalID(engineUrl, complexProcessId);
		ProcessNode son = processService.getProcessByExternalID(engineUrl, complexProcessId + 1);
				
		// add comments to process and sub process
		processService.addCommentToProcess(father.getPk(), "Vaterkommentar", "Martin Pelzer", "nameOfCommentType");
		processService.addCommentToProcess(son.getPk(), "SohnKommentar", "mpelze2s", "nameOfCommentType");
		
		// get complete comment list
		List<Comment> comments = processService.getCommentListByProcessWholeProcess(son.getPk());
		
		// check number of comments
		assertTrue(comments.size() == 2);
	}
	
	@Test
	public void testGetCommentListByTaskWholeProcess() throws SQLException {
		// We add a new task
		// start date is now
		Calendar start = Calendar.getInstance();
		
		// we set the intended end date to one month later
		Calendar end = Calendar.getInstance();
		end.add(Calendar.MONTH, 1);
		
		taskService.addTask(engineUrl, processId, "4711", "Martin Pelzer", null, Helper.taskTypeURI, "Aufgabenbeschreibung", start, end, end, end, "2", "2", "2", null);
		Task task = taskService.getTaskById("4711");
		
		// add a comment type to the database so that we can use it
		CommentType ct = new CommentType();
		ct.setName("comment type x");
		ct.setShortcut("shortcut x");
		utilService.addCommentType(ct);
		
		// now add some comments to the process
		ProcessNode process = processService.getProcessByExternalID(engineUrl, processId);
		
		processService.addCommentToProcess(process.getPk(), "Prozesskommentar", "Henry Jones sr.", "comment type x");
		
		List<Comment> comments = processService.getCommentListByTaskWholeProcess(task.getPk());
		
		// 2 because there was already one comment added before
		assertTrue(comments.size() == 2);
	}
	
	@Test
	public void testAddAttachmentToProcess() {
		// add an attachment
		byte [] document = new byte [36];
		processService.addAttachmentToProcess(processPk, "a nice attachment", "application/pacman", document, 1, "Pac Man");
		
		// get process by pk and check if attachment is there
		ProcessNode process = processService.getProcessByPk(processPk);
		assertTrue(process.getAttachmentList().size() == 1);
		assertTrue(process.getAttachmentList().get(0).getTitle().equals("a nice attachment"));
		assertTrue(process.getAttachmentList().get(0).getAttachmentType().getName().equals("application/pacman"));
	}
	
	@Test
	public void testGetAttachmentListByProcess() {
		List<Attachment> attachments = processService.getAttachmentListByProcess(processPk);
		
		// check size of list and that the added attachment is in the list
		assertTrue(attachments.size() == 1);
		assertTrue(attachments.get(0).getTitle().equals("a nice attachment"));
	}
		
}
