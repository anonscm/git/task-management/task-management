package org.evolvis.middleware;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbpm.taskmgmt.exe.TaskInstance;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.tarent.wfms.processmiddleware.api.CommentApi;
import de.tarent.wfms.processmiddleware.api.ProcessApi;
import de.tarent.wfms.processmiddleware.api.TaskApi;
import de.tarent.wfms.processmiddleware.api.UtilApi;
import de.tarent.wfms.processmiddleware.dao.TaskFilter;
import de.tarent.wfms.processmiddleware.data.Attachment;
import de.tarent.wfms.processmiddleware.data.Comment;
import de.tarent.wfms.processmiddleware.data.CommentType;
import de.tarent.wfms.processmiddleware.data.Task;
import de.tarent.wfms.processmiddleware.data.TaskType;

/** test class for the TaskService class in process middleware
 * NOTE: this class tests only using an JBPM instance as process engine
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class TaskApiTest {

	private static final String engineUrl = "jbpmPiA";
	
	
	private static TaskApi taskService;
	private static ProcessApi processService;
	private static CommentApi commentService;
	private static UtilApi utilService;
	
	private static long processId;
	
	
	
	// ------------------------ set up and tear down ------------------------
	
	@BeforeClass
	public static void setUpTestClass() {
		JbpmHelper.createSchema();
		
		// initialize service classes of process middleware
		taskService = new TaskApi();
		processService = new ProcessApi();
		commentService = new CommentApi();
		utilService = new UtilApi();
		
		// insert task type with uri defined above
		utilService.addTaskType(Helper.createTaskType());
		
		// deploy a process in JBPM so we can access one
		JbpmHelper.deployProcessDefinition(JbpmHelper.FIRST_DEFINITION);
		
		// start a process instance
		processId = JbpmHelper.startProcessInstance(JbpmHelper.FIRST_DEFINITION);
	}
	
	
	@AfterClass
	public static void tearDownTestClass() {
		JbpmHelper.dropSchema();
	}
	
	
	@Before
	public void setUp() {
		
	}


	@After
	public void tearDown() {
	
	}
	
	
	// ------------------------ tests ------------------------
	
	
	@Test
	public void testAddTask() throws SQLException {
		// call helper method for adding a complete task
		this.addCompleteTask("1");
		
		// get task by id and check that it is != null
		Task task = taskService.getTaskById("1");
		assertTrue(task != null);
		assertTrue(task.getActualStaffer() != null);
	}
	
	@Test
	public void testForwardTask() {
		// we forward the added task to another staffer
		
		// check that task is assigned to "Martin Pelzer"
		Task task = taskService.getTaskById("1");
		assertTrue(task.getActualStaffer().getStafferId().equals("Martin Pelzer"));
		
		// forward task
		taskService.forwardTask(task.getPk(), "Ford Prefect");
		
		// check that task is assigned to "Ford Prefect"
		task = taskService.getTaskById("1");
		assertTrue(task.getStafferList().size() == 2);
		assertTrue(task.getStafferList().get(1).getStafferId().equals("Ford Prefect"));
	}
	
	@Test
	public void testCancelTask() {
		// cancel the task we added so far
		Task task = taskService.getTaskById("1");
		taskService.cancelTask(task.getPk(), "Trillian");
		
		assertTrue(taskService.getTaskById("1").getDeleteDate() != null);
	}
	
	@Test
	public void testCloseTask() throws SQLException {
		// first we have to add a new process
		long myProcessId = JbpmHelper.startProcessInstance(JbpmHelper.FIRST_DEFINITION);

		// get TaskInstance from JBPM to get id
		TaskInstance instance = JbpmHelper.getTaskInstance(myProcessId);
		long taskId = instance.getId();
		
		// add a task with this id to processMiddleware (if not already there)
		if( taskService.getTaskById("" + taskId) == null)
			this.addTask("" + taskId);
		
		// close the task
		Task task = taskService.getTaskById("" + taskId);
		Map<String, Object> returnData = new HashMap<String, Object>();
		returnData.put("key1", "value1");
		returnData.put("key2", "value2");
		
		taskService.closeTask(task.getPk(), returnData, "I now declare the depressing task 2 for closed!", "Marvin");
		
		// check that task is closed
		task = taskService.getTaskById("" + taskId);
		assertTrue(task.getEndDate() != null);
		
		// check if callback was successful
		instance = JbpmHelper.getTaskInstance(myProcessId);
		assertTrue(instance.hasEnded());
		assertTrue(instance.getEnd() != null);
	}


	@Test
	public void testGetTaskById() {
		// get task 1 by id and check
		assertTrue(taskService.getTaskById("1").getTaskId().equals("1"));
		
		// get task 2 by id and check
		assertTrue(taskService.getTaskById("2").getTaskId().equals("2"));
	}
	
	@Test
	public void testGetTaskByPk() {
		// get task 1 by id, fet pk and fetch again
		Task taskById = taskService.getTaskById("1");
		
		Task taskByPk = taskService.getTaskByPk(taskById.getPk());
		
		// check that pk and id are the same
		assertTrue(taskByPk.getPk() == taskById.getPk());
		assertTrue(taskByPk.getTaskId().equals(taskById.getTaskId()));
	}
	
	@Test
	public void testGetTaskListByProcess() throws SQLException {
		this.addTask("42");
		
		// get all tasks for Ford Prefect. This should only be two, namely the task with task id "42" and task "2"
		List<Task> taskList = taskService.getTaskListByProcess(processId, engineUrl, "Martin Pelzer");
		
		assertTrue(taskList.size() == 2);
		assertTrue(taskList.get(0).getTaskId().equals("2"));
		assertTrue(taskList.get(1).getTaskId().equals("42"));
	}
	
	@Test
	public void testGetTaskList() throws SQLException {
		// we add several tasks for staffer "James Bond" with several dates, staffers and states
		Calendar now = Calendar.getInstance();
		now.add(Calendar.YEAR, 1);
		Calendar inOneMonth = Calendar.getInstance();
		inOneMonth.add(Calendar.MONTH, 1);
		inOneMonth.add(Calendar.YEAR, 1);
		Calendar inTwoMonth = Calendar.getInstance();
		inTwoMonth.add(Calendar.MONTH, 2);
		inTwoMonth.add(Calendar.YEAR, 1);
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DAY_OF_YEAR, -1);
		yesterday.add(Calendar.YEAR, 1);
		
		this.addCompleteTask("1001", now, inOneMonth);
		this.addCompleteTask("1002", now, inTwoMonth);
		this.addCompleteTask("1003", inOneMonth, inTwoMonth);
		this.addCompleteTask("1004", now, inOneMonth);
		
		Task task1001 = taskService.getTaskById("1001");
		Task task1002 = taskService.getTaskById("1002");
		Task task1003 = taskService.getTaskById("1003");
		Task task1004 = taskService.getTaskById("1004");
		
		taskService.forwardTask(task1001.getPk(), "Guybrush Threepwood");		
		taskService.closeTask(task1004.getPk(), null, "close task 1004", "Martin Pelzer");
		
		
		// default is that only open tasks are shown. so we have to set closedTasks
		
		// get list of all tasks between now and in one month (should be 3)
		TaskFilter filter = new TaskFilter();
		filter.setIntendedEndDateFrom(now.getTime());
		filter.setIntendedEndDateTo(inOneMonth.getTime());
		filter.setStatusClosed(true);
		List<Task> tasks = taskService.getTaskList(filter);
		
		assertTrue(tasks.size() == 2);
		
		// get list of all tasks between now and in two month (should be 4)
		filter = new TaskFilter();
		filter.setIntendedEndDateFrom(now.getTime());
		filter.setIntendedEndDateTo(inTwoMonth.getTime());
		filter.setStatusClosed(true);
		tasks = taskService.getTaskList(filter);
		
		assertTrue(tasks.size() == 4);
		
		
		// get list of all tasks of Guybrush Threepwood (should be 1)
		filter = new TaskFilter();
		filter.setStafferId("Guybrush Threepwood");
		filter.setStatusClosed(true);
		// filter.setStafferId("Martin Pelzer"); mit "Martin Pelzer" kommt da korrekterweise 3 raus!
		tasks = taskService.getTaskList(filter);

		assertTrue(tasks.size() == 1);
		
		
		// get all running tasks in our time frame (should be 3)
		filter = new TaskFilter();
		filter.setIntendedEndDateFrom(now.getTime());
		filter.setIntendedEndDateTo(inTwoMonth.getTime());
		tasks = taskService.getTaskList(filter);
		
		assertTrue(tasks.size() == 3);
		
		
		// get all closed tasks (should be 1)
		filter = new TaskFilter();
		filter.setIntendedEndDateFrom(now.getTime());
		filter.setIntendedEndDateTo(inTwoMonth.getTime());
		filter.setStatusOpen(false);
		filter.setStatusClosed(true);
		tasks = taskService.getTaskList(filter);
		
		assertTrue(tasks.size() == 1);
	}
	
	@Test
	public void testResubmitTask() throws SQLException {
		this.addTask("11");
		Task task = taskService.getTaskById("11");
	
		// First we try to resubmit the task by two month which should not work
		// because the task ends in one month.
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 2);
		taskService.resubmitTask(task.getPk(), cal.getTime());
		
		// check
		task = taskService.getTaskById("11");
		assertTrue(task.getStartDate().before(cal.getTime()));
		
		
		// Now we try to resubmit it by 1 day which should work.
		cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, 1);
		taskService.resubmitTask(task.getPk(), cal.getTime());
		
		// check
		task = taskService.getTaskById("11");
		assertTrue(task.getStartDate().equals(cal.getTime()));
	}
	
	@Test
	public void testAddCommentToTask() throws SQLException {
		// add a new task for testing comment related things
		this.addTask("5");
		Task task = taskService.getTaskById("5");
		
		// add a comment type to the database so that we can use it
		CommentType ct = new CommentType();
		ct.setName("comment type 1");
		ct.setShortcut("shortcut 1");
		utilService.addCommentType(ct);
		
		// add comment to task
		taskService.addCommentToTask(task.getPk(), "ein Testkommentar", "Martin Pelzer", "comment type 1");
		
		// get task again and check that comment is there and that comment data was set
		task = taskService.getTaskById("5");
		assertTrue(task.getCommentList().size() == 1);
		assertTrue(task.getCommentList().get(0).getComment().equals("ein Testkommentar"));
		assertTrue(task.getCommentList().get(0).getStafferId().equals("Martin Pelzer"));
		assertTrue(task.getCommentList().get(0).getCommentType().getName().equals("comment type 1"));
	}
	
	@Test
	public void testGetCommentListByTask() {
		Task task = taskService.getTaskById("5");
		
		List<Comment> commentList = taskService.getCommentListByTask(task.getPk());
		
		// check that one comment is there
		assertTrue(commentList.size() == 1);
		assertTrue(commentList.get(0).getComment().equals("ein Testkommentar"));
		assertTrue(commentList.get(0).getStafferId().equals("Martin Pelzer"));
	}
	
	@Test
	public void testAddAttachmentToTask() {
		// we use the task we added the comments to to test attachments
		Task task = taskService.getTaskById("5");
		
		byte [] document = new byte [42];
		taskService.addAttachmentToTask(task.getPk(), "attachmentTitle", "application/tetris", document, 1, "Martin Pelzer");
		
		// asserts
		task = taskService.getTaskById("5");
		assertTrue(task.getAttachmentList().size() == 1);
		assertTrue(task.getAttachmentList().get(0).getTitle().equals("attachmentTitle"));
		assertTrue(task.getAttachmentList().get(0).getStafferId().equals("Martin Pelzer"));
		assertTrue(task.getAttachmentList().get(0).getAttachmentType().getName().equals("application/tetris"));
	}
	
	@Test
	public void testGetAttachmentListByTask() {
		Task task = taskService.getTaskById("5");
		
		List<Attachment> al = taskService.getAttachmentListByTask(task.getPk());
		
		// check that attachment list has size 1 and contains the attachment we attached so far
		assertTrue(al.size() == 1);
		assertTrue(al.get(0).getTitle().equals("attachmentTitle"));
		assertTrue(al.get(0).getStafferId().equals("Martin Pelzer"));
		assertTrue(al.get(0).getAttachmentType().getName().equals("application/tetris"));
	}
	
	@Test
	public void testAddToTaskData() throws SQLException {
		// we create a new task for testing task data related things
		this.addTask("6");
		Task task = taskService.getTaskById("6");
		
		// use method for adding one variable
		taskService.addToTaskData(task.getPk(), "Key West", "Tief im Westen");
		
		// check if that variable has been added
		task = taskService.getTaskById("6");
		assertTrue(task.getData().get("Key West").equals("Tief im Westen"));
		
		// use method for adding several variables
		Map<String, Object> newData = new HashMap<String, Object>();
		newData.put("key2", "value2");
		newData.put("key3", "value3");
		taskService.addToTaskData(task.getPk(), newData);
		
		// check if that variables have been added
		task = taskService.getTaskById("6");
		assertTrue(task.getData().get("key2").equals("value2"));
		assertTrue(task.getData().get("key3").equals("value3"));
	}
	
	@Test
	public void testGetTaskDataByKey() {
		Task task = taskService.getTaskById("6");
		
		String value = (String) taskService.getTaskDataByKey(task.getPk(), "key2");
		assertTrue(value.equals("value2"));
	}
	
	@Test
	public void testDeleteTaskDataByKey() {
		Task task = taskService.getTaskById("6");
		
		taskService.deleteTaskDataByKey(task.getPk(), "key2");

		// get task again and check that variable is gone 
		task = taskService.getTaskById("6");
		assertTrue(task.getData().get("key2") == null);
	}
	
	@Test
	public void testGetMaps() {
		// TODO
		assertTrue(false);
	}
	
	
	@Test
	public void testGetTaskCount() {
		TaskType type = taskService.getTaskTypByUri(Helper.taskTypeURI);
		
		long count = taskService.getTaskCount(type.getPk());
		assertTrue(count == 10);
	}
	
	
	// --------- some helper methods ----------

	private void addTask(String taskId) throws SQLException {
		// start date is now
		Calendar start = Calendar.getInstance();
		
		// we set the intended end date to one month later
		Calendar end = Calendar.getInstance();
		end.add(Calendar.MONTH, 1);
		
		taskService.addTask(engineUrl, processId, taskId, "Martin Pelzer", null, Helper.taskTypeURI, "Aufgabenbeschreibung", start, end, "2", null);
	}
	
	private void addCompleteTask(String taskId) throws SQLException {
		// start date is now
		Calendar start = Calendar.getInstance();
		
		// we set the intended end date to one month later
		Calendar end = Calendar.getInstance();
		end.add(Calendar.MONTH, 1);
		
		taskService.addTask(engineUrl, processId, taskId, "Martin Pelzer", null, Helper.taskTypeURI, "Aufgabenbeschreibung", start, end, end, end, "2", "2", "2", null);
	}
	
	private void addCompleteTask(String taskId, Calendar start, Calendar intendedEndDate) throws SQLException {		
		taskService.addTask(engineUrl, processId, taskId, "Martin Pelzer", null, Helper.taskTypeURI, "Aufgabenbeschreibung", start, intendedEndDate, intendedEndDate, intendedEndDate, "2", "2", "2", null);
	}

}
