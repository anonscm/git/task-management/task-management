package org.evolvis.middleware;

import de.tarent.wfms.processmiddleware.data.TaskType;

/** This class contains some general helper methods
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class Helper {

	public static String taskTypeURI = "task:type:uri";
	
	
	public static TaskType createTaskType() {
		TaskType type = new TaskType();
		type.setClassname("classname");
		type.setNotifierClassname("notifierClassname");
		type.setReturnType("returnType");
		type.setTarget("target");
		type.setTaskActionCheckerClassname("taskActionCheckerClassname");
		type.setUri(taskTypeURI);
		
		return type;
	}
	
}
