package org.evolvis.middleware;

import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

import org.jbpm.JbpmConfiguration;
import org.jbpm.JbpmContext;
import org.jbpm.context.exe.VariableInstance;
import org.jbpm.db.GraphSession;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.graph.exe.Token;
import org.jbpm.taskmgmt.exe.TaskInstance;

/** This class contains some helper methods for accessing JBPM
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class JbpmHelper {

	public static final String FIRST_DEFINITION = "hello world";
	public static final String SECOND_DEFINITION = "second process definition";
	public static final String COMPLEX_DEFINITION = "complex definition";
	
	
	private static JbpmConfiguration jbpmConfiguration = JbpmConfiguration.getInstance();
	
	
	public static void createSchema() {
		jbpmConfiguration.createSchema();
	}
	
	
	public static void dropSchema() {
		jbpmConfiguration.dropSchema();
	}
	
	
	public static void deployProcessDefinition(String name) {
		// This test shows a process definition and one execution
		// of the process definition. The process definition has
		// 3 nodes: an unnamed start-state, a state 's' and an
		// end-state named 'end'.
		ProcessDefinition processDefinition = ProcessDefinition
				.parseXmlString("<process-definition  name='" + name + "'>"
						+	"<start-state name='start-state1'>"
						+		"<transition to='task-node1'></transition>"
						+	"</start-state>"
						+	"<state name='state1'>"
						+		"<transition to='end-state1'></transition>"
						+	"</state>"
						+	"<task-node name='task-node1'>"
						+		"<task name='Task1' blocking='true'>"
						+			"<description>ein Test-Task</description>"
						+			"<assignment actor-id='testActor'></assignment>"
						+			"<controller></controller>"
						+		"</task>"
						+		"<transition to='state1'></transition>"
						+	"</task-node>"
						+	"<end-state name='end-state1'></end-state>"
						+"</process-definition>");

		// Lookup the pojo persistence context-builder that is configured above
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		try {
			// Deploy the process definition in the database
			jbpmContext.deployProcessDefinition(processDefinition);

		} finally {
			// Tear down the pojo persistence context.
			// This includes flush the SQL for inserting the process definition
			// to the database.
			jbpmContext.close();
		}
	}
	
	
	public static void deployComplexProcessDefinition(String name) {
		// This test shows a process definition and one execution
		// of the process definition. The process definition has
		// 3 nodes: an unnamed start-state, a state 's' and an
		// end-state named 'end'.
		ProcessDefinition processDefinition = ProcessDefinition
				.parseXmlString("<process-definition  name='" + name + "'>"
						+	"<start-state name='start-state1'>"
						+		"<transition to='process-state1'></transition>"
						+	"</start-state>"
						+	"<state name='state1'>"
						+		"<transition to='end-state1'></transition>"
						+	"</state>"
						+	"<process-state name='process-state1'>"
						+ 	   "<sub-process name='hello world' binding='late'></sub-process>"
						+ 	   "<transition to='state1'></transition>"
						+   "</process-state>"
						+	"<end-state name='end-state1'></end-state>"
						+"</process-definition>");
		
		// Lookup the pojo persistence context-builder that is configured above
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		try {
			// Deploy the process definition in the database
			jbpmContext.deployProcessDefinition(processDefinition);

		} finally {
			// Tear down the pojo persistence context.
			// This includes flush the SQL for inserting the process definition
			// to the database.
			jbpmContext.close();
		}
	}
	
	
	public static ProcessInstance getProcessInstance(long processId) {
		// Lookup the pojo persistence context-builder that is configured above
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		try {
			ProcessInstance instance = jbpmContext.getProcessInstance(processId);
			return instance;
		} finally {
			// Tear down the pojo persistence context.
			jbpmContext.close();
		}
	}
	
	
	public static ProcessInstance getSubProcessInstance(long processId) {
		// Lookup the pojo persistence context-builder that is configured above
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		try {
			ProcessInstance instance = jbpmContext.getProcessInstance(processId);
			ProcessInstance sub = instance.getRootToken().getSubProcessInstance();
			return sub;
		} finally {
			// Tear down the pojo persistence context.
			jbpmContext.close();
		}
	}
	
	
	public static long startProcessInstance(String name) {
		// Lookup the pojo persistence context-builder that is configured above
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		try {
			GraphSession graphSession = jbpmContext.getGraphSession();
			
			ProcessDefinition processDefinition = graphSession
					.findLatestProcessDefinition(name);

			ProcessInstance processInstance = new ProcessInstance(processDefinition);

			Token token = processInstance.getRootToken();
			// Let's start the process execution
			token.signal();

			jbpmContext.save(processInstance);
			
			return processInstance.getId();
		} finally {
			// Tear down the pojo persistence context.
			jbpmContext.close();
		}
	}
	
	public static long startProcessInstance(String definitionName, Calendar creationDate, Calendar endDate, boolean suspended) {
		// Lookup the pojo persistence context-builder that is configured above
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		try {
			GraphSession graphSession = jbpmContext.getGraphSession();
			
			ProcessDefinition processDefinition = graphSession
					.findLatestProcessDefinition(definitionName);

			ProcessInstance processInstance = new ProcessInstance(processDefinition);

			Token token = processInstance.getRootToken();
			// Let's start the process execution
			token.signal();
			
			if (creationDate != null)
				processInstance.setStart(creationDate.getTime());
			
			if (endDate != null) {
				processInstance.end();
				processInstance.setEnd(endDate.getTime());
			}

			if (suspended)
				processInstance.suspend();

			jbpmContext.save(processInstance);
			
			return processInstance.getId();
		} finally {
			// Tear down the pojo persistence context.
			jbpmContext.close();
		}
	}
	
	
	public static void signalProcessInstance(long processId) {
		// Lookup the pojo persistence context-builder that is configured above
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		try {
			ProcessInstance instance = jbpmContext.getProcessInstance(processId);
			instance.signal();
			jbpmContext.save(instance);
		} finally {
			// Tear down the pojo persistence context.
			jbpmContext.close();
		}
	}
	
	public static TaskInstance getTaskInstance(long processId) {
		// Lookup the pojo persistence context-builder that is configured above
		JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
		try {
			Collection tasks = jbpmContext.getProcessInstance(processId).getTaskMgmtInstance().getTaskInstances();
			if (tasks == null)
				return null;
			Iterator iter = tasks.iterator();
			TaskInstance instance = null;
			while (iter.hasNext()) {
				instance = (TaskInstance) iter.next();
			}
			return instance;
		} finally {
			// Tear down the pojo persistence context.
			jbpmContext.close();
		}
	}
		
}
